# Xeno Game Engine
Xeno Engine is a simple game engine I’m trying to create for learning purposes.
It contains lots of bad code, is unfinished, and I highly doubt it will ever be finished. It might not even get to a usable state.

## What is Xeno Engine?
In the XenoEngine solution, there’s a project called XenoEngine.
This is the engine itself. This is where the magic happens.

## What is Xeno Game?
In the XenoEngine solution, there’s a project called XenoGame.
This project is *not* part of the engine itself. It’s only a test game running on top of the Xeno game engine.