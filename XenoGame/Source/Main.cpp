#include <Xeno/Engine.h>
#include <Xeno/EngineIncludes.h>
#include <Xeno/Math/Math.h>
#include <iostream>
#include <Xeno/Types/String.h>

using Xeno::Engine;

int main(int argc, char* argv[])
{
	Engine& engine = Engine::GetInstance();

	if (!engine.Initialize(argc, argv))
	{
		XENO_DEBUG_ONLY(std::cin.get());
	}

	return 0;
}