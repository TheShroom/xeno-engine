#pragma once
#include <cstdlib>

// If set to true, assertions will be compiled into code. If not, they will be removed from the compiled code.
#define ENABLE_ASSERTIONS _DEBUG
#define XENO_ABORT(x) raise(SIGABRT);

#if ENABLE_ASSERTIONS true

#define XENO_ASSERT(expression, msg) if (expression == false) { XENO_LOG_ERROR("Assertion triggered, expression: " + std::to_string(##expression) + ", message: " + ##msg); Xeno::Abort(); }
#define XENO_STATIC_ASSERT(expression, msg) static_assert(expression, ##msg);

#elif
#define XENO_ASSERT(expression)
#define XENO_STATIC_ASSERT(expression)
#endif

namespace Xeno
{
	static void Abort()
	{
		std::abort();
	}

}