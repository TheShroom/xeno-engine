#pragma once
#include "Assert.h"

#define XENO_DEBUG_ENABLED _DEBUG

#if XENO_DEBUG_ENABLED == 1
#define XENO_DEBUG_ONLY(x) x
#else
#define XENO_DEBUG_ONLY(x)
#endif

#if XENO_DEBUG_ENABLED == 1
// Note: Need to implement Types::String::ToString() before the below can be fully converted from std::string to Types::String.
#define XENO_DEBUG_TIME_FUNC(f) { XENO_LOG(std::string("Timing function: ") +std::string(#f)); float before = Time::GetElapsedTime(); f; float after = Time::GetElapsedTime(); float elapsedTime = after - before; XENO_LOG(std::to_string(elapsedTime)); }
#else
#define XENO_DEBUG_TIME_FUNC(f) f
#endif