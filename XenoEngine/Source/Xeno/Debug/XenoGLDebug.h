#pragma once
#include <Xeno/EngineIncludes.h>
#include <assert.h>

// Checks for OpenGL errors after running the wrapped code.
// Note: Added XenoTypes::String::ParseInt() call
#define XENO_GL_CALL(call) call; XENO_DEBUG_ONLY(if (glGetError() != GL_NO_ERROR) { std::string msg = std::string(); msg += "OpenGL Error: "; msg += std::to_string(unsigned(glGetError())); XENO_LOG_ERROR("XENO_GL_CALL ERROR: " + msg); assert(false); })