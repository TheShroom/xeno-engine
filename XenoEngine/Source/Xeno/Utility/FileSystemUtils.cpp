#include "FileSystemUtils.h"

namespace Xeno
{
	namespace Utility
	{

		// Very simple utility, checks if a path contains a "disk specifier" (the :)
		// So C:/SomeDirectory/file.txt would return true since it has a : while
		// SomeDirectory/file.txt would return false since it doesn't.
		bool FileSystemUtils::IsPathAbsolute(const std::string& path)
		{
			return path.find_first_of(':') != std::string::npos;
		}

	}
}