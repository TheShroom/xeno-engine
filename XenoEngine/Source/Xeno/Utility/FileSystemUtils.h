#pragma once
#include <Xeno/EngineIncludes.h>
#include <string.h>

namespace Xeno
{
	namespace Utility
	{
		ENGINEAPI class FileSystemUtils
		{
		public:
			static bool IsPathAbsolute(const std::string& path);

		};
	}
}