#pragma once

#include <Xeno/EngineDefines.h>
#include <Xeno/Types/Types.h>
#include <Box2D/Box2D.h>

namespace Xeno
{
	namespace Physics
	{

		class ENGINEAPI Physics2D
		{
		private:
			Types::Vector2 m_gravity;
			float m_timeStep;
			int m_velocityIterations;
			int m_positionIterations;

			b2World* m_physicsWorld;

			Physics2D();

		protected:
		public:
			static Physics2D& GetInstance();

			void Initialize(Types::Vector2 gravity, float timeStep, int velocityIterations, int positionIterations);
			void Update();

			b2World* GetPhysicsWorld();

			void SetGravity(Types::Vector2 gravity);
			Types::Vector2 GetGravity();

		};

	}
}