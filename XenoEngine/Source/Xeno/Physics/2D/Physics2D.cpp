#include "Physics2D.h"

namespace Xeno
{
	namespace Physics
	{

		Physics2D::Physics2D()
		{
			
		}

		Physics2D& Physics2D::GetInstance()
		{
			static Physics2D instance;
			return instance;
		}

		void Physics2D::Initialize(Types::Vector2 gravity, float timeStep, int velocityIterations, int positionIterations)
		{
			m_gravity = gravity;
			m_physicsWorld = new b2World(b2Vec2(m_gravity.x, m_gravity.y));
			m_timeStep = timeStep;
			m_velocityIterations = velocityIterations;
			m_positionIterations = positionIterations;
		}

		void Physics2D::Update()
		{
			m_physicsWorld->Step(m_timeStep, m_velocityIterations, m_positionIterations);
		}

		b2World* Physics2D::GetPhysicsWorld()
		{
			return m_physicsWorld;
		}

		void Physics2D::SetGravity(Types::Vector2 gravity)
		{
			m_physicsWorld->SetGravity(b2Vec2(m_gravity.x, m_gravity.y));
		}

		Types::Vector2 Physics2D::GetGravity()
		{
			return m_gravity;
		}

	}
}