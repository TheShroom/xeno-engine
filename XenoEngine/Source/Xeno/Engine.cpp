#include "Engine.h"
#include "EngineIncludes.h"
#include "Time/Time.h"
#include "Graphics/Temporary/Triangle.h"
#include "Graphics/Renderable.h"
#include "Graphics/Mesh.h"
#include "Graphics/Mesh Presets/CubeMesh.h"
#include "Graphics/Mesh Presets/SpriteMesh.h"
#include "Graphics/Shader.h"
#include "Graphics/Texture.h"
#include "Graphics/ImageLoader.h"
#include "World/WorldManager.h"
#include "World/World.h"
#include "Entity Component System/Components/Components.h"
#include "Entity Component System/Entity.h"
#include "Graphics/ModelLoader.h"
#include <External Source/IMGUI/imgui.h>
#include "FileIO/ResourceLoader.h"
#include "Error Handling/ExceptionHandler.h"
#include "Error Handling/XenoException.h"
#include "FileIO/PAKLoader.h"
#include <Xeno/Memory/MemoryTracker.h>
#include <Xeno/Editor/Editor.h>
#include <Xeno/Input/Cursor.h>
#include <Box2D/Box2D.h>
#include <Xeno/Entity Component System/Components/Physics/Rigidbody2DComponent.h>
#include <Xeno/Physics/2D/Physics2D.h>

//! Temporary includes
#include <nlohmann-json/json.hpp>
#include <Xeno/Utility/FileSystemUtils.h>

// Todo: Implement? Could be fun.
// #include <Xeno/Misc/Delegate.h>

using Xeno::Input;
using Xeno::Logging::Logger;


namespace Xeno {

	// Static defines
	Engine* Engine::m_instance = nullptr;

	Engine::Engine()
	{
		
	}

	Engine::~Engine()
	{
	}

	Engine& Engine::GetInstance()
	{
		static Engine instance;
		return instance;
	}

	bool Engine::Initialize(int argc, char* argv[])
	{
		try
		{
			XENO_LOG("Initializing engine module.");

			// First argument in argv is always the path to the executable. (is this true for all platforms?)
			FileIO::ResourceLoader::Initialize(argv[0]);

			m_window = Graphics::Window("Xeno Game Engine", 1280, 720);

			m_renderer = &Graphics::Renderer::GetInstance();
			m_renderer->Initialize();

			FileIO::PAKLoader::Initialize(argc, argv);

			m_mainCamera.Initialize(60, 5.0f, 0.1f, 100.0f, Graphics::Camera::CameraMode::Perspective, &m_window);

			// Initialize input after IMGUI, otherwise the callbacks will be overridden by IMGUI.
			Input::GetInstance().Initialize();
			Cursor::GetInstance().Initialize();

			Physics::Physics2D::GetInstance().Initialize(Types::Vector2(0.0f, -9.81f), 1.0f / 60.0f, 8, 3);

			XENO_LOG("Engine module initialized, running engine loop.");

			EngineLoop();
		}
		catch (std::exception e) // Used to report unhandled exceptions.
		{
			Exceptions::ExceptionHandler::HandleException(e); // Reports the exception.
			std::cin.get(); // Confirm exit.
			return false; // Return false, the program did not exit properly.
		}
		catch (...)
		{
			Exceptions::ExceptionHandler::HandleException();
			std::cin.get();
			return false;
		}

		return true; // Exited properly.
	}

	void Engine::EngineLoop()
	{

		XENO_LOG("Engine loop initializing");

		Memory::MemoryTracker::Initialize();

		World world = World();
		WorldManager::GetInstance().LoadWorld(&world);

		Graphics::Shader shader = FileIO::ResourceLoader::LoadShader("Shaders/BasicTexProjShader"); // Graphics::Shader("D:\\Projects\\C++\\XenoEngine\\Xeno\\XenoEngine\\Resources\\Shaders\\BasicTexProjShader");
		Graphics::Texture texture = FileIO::ResourceLoader::LoadTexture("Textures/WhiteSquare.png"); // Graphics::Texture texture = FileIO::ResourceLoader::LoadTexture("Textures/DefaultSprite.png"); // Graphics::Texture(Graphics::ImageLoader::GetInstance().LoadImageAsTexture("D:\\Projects\\C++\\XenoEngine\\Xeno\\XenoEngine\\Resources\\Models\\TexturedCube\\TEXTURE.png"));
		Graphics::Material material = Graphics::Material(&shader);
		// Graphics::Material material = Graphics::Material(&shader, &texture);
		// Graphics::Material material = Graphics::Material(&shader);

		Graphics::Mesh mesh = Graphics::SpriteMesh();

		// Graphics::Mesh mesh = Graphics::Mesh(vertexData, indexData);
		// Graphics::Mesh mesh = Graphics::ModelLoader::LoadModel("Models/Monkey.fbx");
		// Graphics::Mesh mesh = Graphics::ModelLoader::LoadModel("Models/XenoModel.fbx");
		// Graphics::Mesh mesh = Graphics::ModelLoader::LoadModel("Models/Dodge.FBX");
		// Graphics::Mesh mesh = Graphics::ModelLoader::LoadModel("Models/TexturedCube/Cube.dae");
		// Graphics::Mesh mesh = Graphics::CubeMesh();
		
		// Graphics::Mesh mesh = Graphics::ModelLoader::GetInstance().LoadModel(
		//     "Models/Toy_Cube.fbx"
		// );

		// mesh.SetTexCoords(texCoords);

		Entity* cameraEntity = new Entity("Camera");
		cameraEntity->SetTag("MainCamera");
		CameraComponent* camera = cameraEntity->AddComponent<CameraComponent>();
		camera->SetCamera(&m_mainCamera);
		camera->GetCamera()->SetFieldOfView(45);
		camera->GetCamera()->SetNearPlane(0.1f);
		camera->GetCamera()->SetFarPlane(100.0f);
		camera->SetCameraMode(Graphics::Camera::CameraMode::Orthographic);
		TransformComponent* cameraTransform = cameraEntity->GetComponent<TransformComponent>();
		cameraTransform->SetPosition(Types::Vector3(0.0f, 0.0f, 10.0f));
		cameraEntity->AddComponent<CameraLookComponent>();

		// camera->SetLookPosition(Types::Vector3(0.0f, 0.0f, 0.0f));

		Entity* cubeEntity = new Entity("CubeEntity");
		TransformComponent* cubeEntTransform = cubeEntity->GetComponent<TransformComponent>();
		MeshComponent* cubeEntMesh = cubeEntity->AddComponent<MeshComponent>();
		cubeEntMesh->SetMesh(mesh);
		MaterialComponent* cubeEntMat = cubeEntity->AddComponent<MaterialComponent>();
		cubeEntMat->SetMaterial(&material);
		RendererComponent* cubeEntRenderer = cubeEntity->AddComponent<RendererComponent>(); // This takes a large amount of time.
		cubeEntRenderer->SetWindow(&m_window);

		Entity* cubeEntity2 = new Entity("CubeEntity2");
		TransformComponent* cubeEntTransform2 = cubeEntity2->GetComponent<TransformComponent>();
		MeshComponent* cubeEntMesh2 = cubeEntity2->AddComponent<MeshComponent>();
		cubeEntMesh2->SetMesh(mesh);
		MaterialComponent* cubeEntMat2 = cubeEntity2->AddComponent<MaterialComponent>();
		cubeEntMat2->SetMaterial(&material);
		RendererComponent* cubeEntRenderer2 = cubeEntity2->AddComponent<RendererComponent>(); // This takes a large amount of time.
		cubeEntRenderer2->SetWindow(&m_window);
		cubeEntTransform2->SetPosition(Types::Vector3(0.0f, -4.5f, 0.0f));
		cubeEntTransform2->SetScale(Types::Vector3(1.0f, 1.0f, 1.0f));
		cubeEntRenderer2->SetVisible(true);

		Entity* cubeEntity3 = new Entity("CubeEntity3");
		TransformComponent* cubeEntTransform3 = cubeEntity3->GetComponent<TransformComponent>();
		MeshComponent* cubeEntMesh3 = cubeEntity3->AddComponent<MeshComponent>();
		cubeEntMesh3->SetMesh(mesh);
		MaterialComponent* cubeEntMat3 = cubeEntity3->AddComponent<MaterialComponent>();
		cubeEntMat3->SetMaterial(&material);
		RendererComponent* cubeEntRenderer3 = cubeEntity3->AddComponent<RendererComponent>(); // This takes a large amount of time.
		cubeEntRenderer3->SetWindow(&m_window);
		cubeEntTransform3->SetPosition(Types::Vector3(0.0f, 4.5f, 0.0f));
		cubeEntTransform3->SetScale(Types::Vector3(1.0f, 1.0f, 1.0f));
		cubeEntRenderer3->SetVisible(true);

		Rigidbody2DComponent* cubeEntRigid = cubeEntity->AddComponent<Rigidbody2DComponent>();
		cubeEntRigid->SetBodyType(Rigidbody2DComponent::Body2DType::DYNAMIC);

		Rigidbody2DComponent* cubeEntRigid3 = cubeEntity3->AddComponent<Rigidbody2DComponent>();
		cubeEntRigid3->SetBodyType(Rigidbody2DComponent::Body2DType::DYNAMIC);

		Rigidbody2DComponent* cubeEnt2Rigid = cubeEntity2->AddComponent<Rigidbody2DComponent>();
		cubeEnt2Rigid->SetBodyType(Rigidbody2DComponent::Body2DType::STATIC);

		cubeEntity->SetTag("TestTag");

		nlohmann::json ser = cubeEntity->Serialize();
		XENO_LOG("Serialized entity:\n", ser.dump(4));

		bool checkboxTestValue = true;
		float colors[3] = { 0.0f, 0.0f, 0.0f };

		// Assertion testing
		// XENO_STATIC_ASSERT(true, "Assertion failed");
		// XENO_ASSERT(true, "Assertion failed");

		//! Nlohmann JSON library testing
		std::string jsonContent = FileIO::PAKLoader::LoadSingleFileContent("Resources/Data/test.json");
		nlohmann::json jsonObject = nlohmann::json::parse(jsonContent);
		XENO_LOG("Parsed JSON, result:\n", jsonObject.dump(4));

		jsonObject["isTest"] = false;
		XENO_LOG("Changed JSON, result:\n", jsonObject.dump(4));

		FileIO::PAKLoader::WriteSingleFileContent("Resources/Data/test.json", jsonObject.dump(4));

		jsonContent = FileIO::PAKLoader::LoadSingleFileContent("Resources/Data/test.json");
		jsonObject = nlohmann::json::parse(jsonContent);
		XENO_LOG("Wrote JSON, result:\n", jsonObject.dump(4));

		XENO_LOG("IsAbsolute: ", std::to_string(Utility::FileSystemUtils::IsPathAbsolute(std::string("Resources/Data/test.json"))));

		XENO_LOG("Engine loop initialized, starting loop");

		while (!m_window.HasRecievedCloseEvent())
		{
			m_totalFrames++;

			Time::Update();
			Input::GetInstance().Poll();

			Editor::Editor::GetInstance().UpdateEditor();
		
			if (Input::GetInstance().IsKeyDown(GLFW_KEY_F11)) // Just here for some debugging, pressing F11 will result in a glGetError() check.
			{
				XENO_LOG("Checking OpenGL for errors...");
				if (glGetError() != GL_NO_ERROR)
				{
					XENO_LOG("OpenGL errors detected!")
					Types::String msg = std::to_string(glGetError()); // Note: Should probably implement ToString() in Types::String.
					assert(false);
				}
				else
				{
					XENO_LOG("No OpenGL errors detected.");
				}
			}

			if (Input::IsKeyDown(GLFW_KEY_F1))
			{
				camera->SetCameraMode(Graphics::Camera::CameraMode::Perspective);
			}

			if (Input::IsKeyDown(GLFW_KEY_F2))
			{
				camera->SetCameraMode(Graphics::Camera::CameraMode::Orthographic);
			}

			if (Input::GetInstance().IsKeyHeld(GLFW_KEY_L))
			{
				cubeEntTransform->RotateAxis(Types::Vector3(1.0f, 0.0f, 0.0f), 90.0f * 0.01f * Time::GetDeltaTime());
			}

			if (Input::IsKeyHeld(GLFW_KEY_H))
			{
				cubeEntRigid->ApplyForceToCenter(Types::Vector2(3000.0f * Time::GetDeltaTime(), 0.0f));
			}

			m_window.Clear();

			// transformComp->Rotate(Types::Vector3(0.0f, 0.0f, 1.0f));
			// material.SetUniform("time", glm::clamp(glm::sin(Time::GetElapsedTime()), 0.0f, 1.0f));

			// cubeEntTransform->SetPosition(Types::Vector3(1000.0f * Time::GetDeltaTime(), 0.0f, 0.0f) + Types::Vector3(cubeEntTransform->GetPosition()));
			// camera->SetLookPosition(cubeEntTransform->GetPosition());
			
			// cubeEntTransform->SetRotation(cubeEntTransform->GetRotation() + Types::Vector3(0.0f, 1.0f * Time::GetDeltaTime(), 0.0f));
			// cubeEntTransform2->SetRotation(cubeEntTransform2->GetRotation() + Types::Vector3(0.0f, -1.0f * Time::GetDeltaTime(), 0.0f));

			WorldManager::GetInstance().UpdateEntities();

			m_window.SwapBuffers();

			Physics::Physics2D::GetInstance().Update();
			WorldManager::GetInstance().LateUpdateEntities();
			Time::LateUpdate();
			Input::GetInstance().LatePoll();
		}

		Exit(0);
	}

	float Engine::CalculateFramesPerSecond()
	{
		return 1.0f / Time::GetDeltaTime();
	}

	float Engine::CalculateAverageFramesPerSecond()
	{
		return m_totalFrames / Time::GetElapsedTime();
	}

	float Engine::CalculateFrameTime()
	{
		return Time::GetDeltaTime() * 1000;
	}

	float Engine::CalculateAverageFrameTime()
	{
		return (Time::GetElapsedTime() / m_totalFrames) * 1000;
	}

	void Engine::Exit(int status)
	{
		// Todo: Close everything else too.
		std::cout << "============================================" << std::endl;
		std::cout << "============= ENGINE EXITING ===============" << std::endl;
		std::cout << "============================================" << std::endl;
		std::cout << "============== STATUS REPORT ===============" << std::endl;
		std::cout << "============================================" << std::endl;
		std::cout << "Average FPS: " << m_totalFrames / Time::GetElapsedTime() << std::endl;
		std::cout << "Framecount: " << m_totalFrames << std::endl;
		std::cout << "Drawcalls per second: " << Graphics::Renderer::GetDrawCallCount() / Time::GetElapsedTime() << std::endl;
		std::cout << "Drawcall count: " << Graphics::Renderer::GetDrawCallCount() << std::endl;
		std::cout << "Drawcalls per frame: " << Graphics::Renderer::GetDrawCallCount() / m_totalFrames << std::endl;
		std::cout << "Drawcalls per entity: " << Graphics::Renderer::GetDrawCallCount() / m_totalFrames / WorldManager::GetInstance().GetActiveWorld()->GetEntityCount() << std::endl;
		std::cin.get();
	}

}