#pragma once
#include <Xeno/EngineIncludes.h>
#include <Xeno/Time/Time.h>

static class ENGINEAPI Math
{
private:
protected:
public:
	static float ToNormalized(float value, float min, float max)
	{
		return (value - min) / (max - min);
	}

	static float FromNormalized(float value, float min, float max) // Todo: Make this function work.
	{
		return (value + min) / (max + min);
	}

	static bool IsNormalized(float value)
	{
		if (value > 1.0f || value < -1.0f)
			return false;
		else
			return true;
	}

	static float Clamp(float value, float min, float max)
	{
		if (value > max) return max;
		else if (value < min) return min;
		else return value;
	}
};