#pragma once

#include <Xeno/EngineDefines.h>
#include <Xeno/Types/String.h>

namespace Xeno
{
	namespace Serialization
	{

		class ENGINEAPI ISerializable
		{
		private:
		protected:
		public:
			virtual std::string Serialize() = 0;
			virtual void Deserialize(const std::string serialized) = 0;
		};

	}
}