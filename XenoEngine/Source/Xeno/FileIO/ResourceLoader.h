#pragma once
#include <Xeno/Graphics/Texture.h>
#include <Xeno/Graphics/Shader.h>
#include <Xeno/Graphics/Mesh.h>
#include <string>
#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace FileIO
	{
		class ENGINEAPI ResourceLoader
		{
		private:
			/* The absolute path the resource folder on the disk. 
			 * Used by Load* functions in order to know where to load stuff from using a
			 * path relative to the resource folder. */
			static std::string resourceFolderLocation;
		public:
			static void Initialize(const std::string& executablePath);

			// Loads an image as a texture using a path relative to the Resources folder.
			static Xeno::Graphics::Texture LoadTexture(const std::string& path);
			static Xeno::Graphics::Shader LoadShader(const std::string& path);
			static Xeno::Graphics::Mesh LoadModel(const std::string& path);

			static std::string GetResourceFolderPath();
		protected:
		};
	}
}