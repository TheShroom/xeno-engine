#include "ResourceLoader.h"
#include <Xeno/Graphics/ImageLoader.h>
#include <Xeno/Graphics/ModelLoader.h>
#include <Xeno/Error Handling/XenoException.h>
#include <Xeno/FileIO/PAKLoader.h>
#include <Xeno/Types/StringType.h>

using namespace Xeno::Graphics;

namespace Xeno
{
	namespace FileIO
	{
		std::string ResourceLoader::resourceFolderLocation = "RESOURCELOADER NOT INITIALIZED";

		void ResourceLoader::Initialize(const std::string& executablePath)
		{
			std::string strConversion(executablePath.c_str());
			XENO_LOG("Initializing resource loader...");
			XENO_LOG("Executable path: " + executablePath);
			std::string tt = "XenoGame.exe"; // Todo: Have to create variable before sending to RemoveLastOf for some reason.
			std::string temp = StringHelper::RemoveMatchingString(strConversion, tt);
			resourceFolderLocation = temp + "Resources\\";
			XENO_LOG("Resource folder location: " + resourceFolderLocation);
			XENO_LOG("Resource loader initialized.");
		}

		Texture ResourceLoader::LoadTexture(const std::string& path)
		{
			Types::String fileContent = PAKLoader::LoadSingleFileContent("Resources/" + path);
			return ImageLoader::GetInstance().LoadImageFromMemoryAsTexture((unsigned char*)fileContent.ToCString(), fileContent.Count());
		}
		
		Shader ResourceLoader::LoadShader(const std::string& path)
		{
			std::string vertexContent = PAKLoader::LoadSingleFileContent("Resources/" + path + "/vertex.glsl");
			std::string fragmentContent = PAKLoader::LoadSingleFileContent("Resources/" + path + "/fragment.glsl");
			return Shader(vertexContent, fragmentContent);
		}

		Mesh ResourceLoader::LoadModel(const std::string& path)
		{
			std::string modelSource = PAKLoader::LoadSingleFileContent("Resources/" + path);
			return ModelLoader::LoadModel(modelSource);
		}

		std::string ResourceLoader::GetResourceFolderPath()
		{
			return resourceFolderLocation;
		}
	}
}