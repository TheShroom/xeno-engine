#pragma once
#include <string>
#include <Xeno/EngineDefines.h>
#include <Xeno/Types/StringType.h>

#define DATA_PAK_PATH "data.PAK"

namespace Xeno
{
	namespace FileIO
	{
		class ENGINEAPI PAKLoader
		{
		private:
			static std::string m_gameRootPath;
			static std::string m_pakPath;
		public:
			static void Initialize(int argc, char* argv[]);
			static std::string LoadSingleFileContent(const std::string& path);
			static void WriteSingleFileContent(const std::string& path, const std::string& content);
		protected:
		};
	}
}