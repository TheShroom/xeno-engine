#include "PAKLoader.h"
#include <ZipLib/ZipFile.h>
#include <istream>
#include <string>
#include <Xeno/Error Handling/XenoException.h>
#include <sstream>

namespace Xeno
{
	namespace FileIO
	{
		std::string PAKLoader::m_gameRootPath = "GAME ROOT PATH NOT INITIALIZED";
		std::string PAKLoader::m_pakPath = "PAK PATH NOT INITIALIZED";

		void PAKLoader::Initialize(int argc, char* argv[])
		{
			std::string strConversion(argv[0]);
			std::string tt = "XenoGame.exe";

			std::string removed = StringHelper::RemoveMatchingString(strConversion, tt);
			m_gameRootPath = removed;

			m_pakPath = m_gameRootPath + DATA_PAK_PATH;
		}

		std::string PAKLoader::LoadSingleFileContent(const std::string& path)
		{
			ZipArchive::Ptr archive = ZipFile::Open(m_pakPath);
			if (archive == NULL)
			{
				std::string str("Failed to load PAK archive '" + m_pakPath + "'.");
				XENO_THROW_EXCEPTION(str);
			}

			ZipArchiveEntry::Ptr entry = archive->GetEntry(path);
			if (entry == nullptr)
			{
				std::string str("Failed to read data entry '" + path + "' in '" + m_pakPath + "'.");
				XENO_THROW_EXCEPTION(str);
			}

			std::istream* compressionStream = entry->GetDecompressionStream();
			std::string content(std::istreambuf_iterator<char>(*compressionStream), {});

			return content;
		}

		void PAKLoader::WriteSingleFileContent(const std::string& path, const std::string& content)
		{
			ZipArchive::Ptr archive = ZipFile::Open(m_pakPath);
			if (archive == NULL)
			{
				std::string str("Failed to load PAK acrhive '" + m_pakPath + "'.");
				XENO_THROW_EXCEPTION(str);
			}

			ZipArchiveEntry::Ptr entry = archive->GetEntry(path);
			if (entry == nullptr)
			{
				std::string str("Failed to read data entry '" + path + "' in '" + m_pakPath + "'.");
				XENO_THROW_EXCEPTION(str);
			}

			std::istringstream contentStream(content);
			entry->SetCompressionStream(contentStream);
			ZipFile::SaveAndClose(archive, m_pakPath); // Is this bad? Maybe it's better to keep it in memory?
		}
	}
}