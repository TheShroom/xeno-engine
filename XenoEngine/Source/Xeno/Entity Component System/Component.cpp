#include "Component.h"

namespace Xeno
{
	Component::Component(Entity* owner) :
		m_renderDefaultUI(true)
	{ 
		m_owner = owner; 
	}

	Entity* Component::GetOwner()
	{
		return m_owner;
	}
} 