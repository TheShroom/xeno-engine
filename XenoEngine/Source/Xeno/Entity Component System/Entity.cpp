#include "Entity.h"
#include "Components/Components.h"
#include <Xeno/World/WorldManager.h>
#include <Xeno/Editor/Editor.h>

namespace Xeno
{
	Entity::Entity()
	{
		SetupDefaultComponents();
		AddToWorld();
	}
	Entity::Entity(const std::string& name)
	{
		SetName(name);
		SetupDefaultComponents();
		AddToWorld();
	}

	Entity::Entity(const std::string& name, std::vector<Component> components)
	{
		XENO_LOG_WARNING("Constructor not yet implemented, doesn't add component list to Entity");
		SetName(name);
		SetupDefaultComponents();
		AddToWorld();
	}

	Entity::Entity(std::vector<Component> components)
	{
		XENO_LOG_WARNING("Constructor not yet implemented, doesn't add component list to Entity");
		SetupDefaultComponents();
		AddToWorld();
	}

	void Entity::SetName(const std::string& name)
	{
		m_name = name;
	}

	std::string Entity::GetName()
	{
		return m_name;
	}

	void Entity::SetTag(const std::string& tag)
	{
		m_tag = tag;
	}

	std::string Entity::GetTag()
	{
		return m_tag;
	}

	void Entity::Update()
	{
		UpdateComponents();
	}

	void Entity::LateUpdate()
	{
		LateUpdateComponents();
	}

	nlohmann::json Entity::Serialize()
	{
		nlohmann::json jsonEntity;
		jsonEntity[GetName()] = nlohmann::json::object(); // Create a new object inside the current entity json, same as using {}
		jsonEntity[GetName()]["tag"] = GetTag(); // Set the tag
		jsonEntity[GetName()]["components"] = nlohmann::json::array(); // Add a new array to store all components
		
		for (int i = 0; i < m_components.size(); i++)
		{
			// For each component add an identifier and object pair, the id is the index and the value is an object
			// which holds all the serializable values of that specific component (in this case empty since components
			// cannot yet be serialized
			// Todo: Serialize component values here
			jsonEntity[GetName()]["components"][i] = { i, { { } } }; 
		}

		return jsonEntity;
	}

	void Entity::Deserialize(nlohmann::json json)
	{

	}

	void Entity::UpdateComponents()
	{
		for (unsigned int i = 0; i < m_components.size(); i++)
		{
			m_components[i]->Update();
		}
	}

	void Entity::LateUpdateComponents()
	{
		for (unsigned int i = 0; i < m_components.size(); i++)
		{
			m_components[i]->LateUpdate();
		}
	}

	void Entity::AddToWorld()
	{
		WorldManager::GetInstance().GetActiveWorld()->AddEntity(this);
	}

	void Entity::SetupDefaultComponents()
	{
		AddComponent<TransformComponent>();
	}
}