#include "Rigidbody2DComponent.h"
#include <Xeno/Entity Component System/Entity.h>
#include <Xeno/Physics/2D/Physics2D.h>

namespace Xeno
{
	Rigidbody2DComponent::Rigidbody2DComponent(Entity* owner) :
		Component(owner)
	{
		// Setup default values
		m_density = 1.0f;
		m_friction = 0.3f;

		m_transformComponent = GetOwner()->GetComponent<TransformComponent>();
		m_meshComponent = GetOwner()->GetComponent<MeshComponent>();
		Types::Vector2 pos = m_transformComponent->GetPosition();
		m_bodyDef.type = (b2BodyType)m_bodyType;
		m_bodyDef.position = b2Vec2(pos.x - m_transformComponent->GetScale().x, pos.y - (m_transformComponent->GetScale().y));
		m_body = Physics::Physics2D::GetInstance().GetPhysicsWorld()->CreateBody(&m_bodyDef);
		m_shape.SetAsBox(m_transformComponent->GetScale().x * 0.5f, m_transformComponent->GetScale().y * 0.5f);
		XENO_LOG("Size: (", m_transformComponent->GetScale().x * 0.5f, ", ", m_transformComponent->GetScale().y * 0.5f, ")");
		m_fixtureDef.shape = &m_shape;
		m_fixtureDef.density = m_density;
		m_fixtureDef.friction = m_friction;
		m_body->CreateFixture(&m_fixtureDef);
	}

	void Rigidbody2DComponent::Start()
	{
		
	}

	void Rigidbody2DComponent::Update()
	{
		
	}

	void Rigidbody2DComponent::LateUpdate()
	{
		b2Vec2 bodyPos = m_body->GetPosition();
		float bodyRot = m_body->GetAngle();
		m_transformComponent->SetPosition(Types::Vector3(bodyPos.x - (m_transformComponent->GetScale().x * 0.5f), bodyPos.y - (m_transformComponent->GetScale().y * 0.5f), 0.0f));
		m_transformComponent->SetEulerRotation(Types::Vector3(0.0f, 0.0f, bodyRot));
	}

	void Rigidbody2DComponent::ApplyForceToCenter(Types::Vector2 force, bool wake)
	{
		m_body->ApplyForceToCenter(b2Vec2(force.x, force.y), wake);
	}

	void Rigidbody2DComponent::SetBodyType(Body2DType type)
	{
		m_body->SetType((b2BodyType)type);
	}

	Rigidbody2DComponent::Body2DType Rigidbody2DComponent::GetBodyType()
	{
		return (Body2DType)m_body->GetType();
	}
}