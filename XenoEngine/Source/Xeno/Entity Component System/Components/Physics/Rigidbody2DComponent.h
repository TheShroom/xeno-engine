#pragma once

#include <Xeno/Entity Component System/Component.h>
#include <Xeno/Entity Component System/Components/TransformComponent.h>
#include <Xeno/Entity Component System/Components/Graphics/MeshComponent.h>
#include <Box2D/Box2D.h>

namespace Xeno
{
	
	class ENGINEAPI Rigidbody2DComponent : public Component
	{
	public:
		enum Body2DType
		{
			STATIC = b2BodyType::b2_staticBody,
			KINEMATIC = b2BodyType::b2_kinematicBody,
			DYNAMIC = b2BodyType::b2_dynamicBody
		};
	private:
		TransformComponent* m_transformComponent;
		MeshComponent* m_meshComponent;

		Body2DType m_bodyType;
		float m_density;
		float m_friction;

		b2BodyDef m_bodyDef;
		b2Body* m_body;
		b2PolygonShape m_shape;
		b2FixtureDef m_fixtureDef;

	protected:
	public:

		Rigidbody2DComponent(Entity* owner);

		void Start() override;
		void Update() override;
		void LateUpdate() override;

		void ApplyForceToCenter(Types::Vector2 force, bool wake = true);

		void SetBodyType(Body2DType type);
		Body2DType GetBodyType();
	};

}