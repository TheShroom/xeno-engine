#pragma once
#include <Xeno/EngineDefines.h>
#include "../Component.h"
#include "TransformComponent.h"
#include "Graphics/CameraComponent.h"

namespace Xeno
{
	class ENGINEAPI CameraLookComponent : public Component
	{
	private:
		TransformComponent * m_transformComponent;
		CameraComponent* m_cameraComponent;
		const float m_speed = 5.0f;
		const float m_dragSensitivity = 0.10f;
		const float m_zoomSensitivity = 4.0f;

		Types::Vector2 m_cursorPosOffset;
		Types::Vector2 m_cursorPos;
		Types::Vector2 m_cursorPosLastFrame;
		float m_zoomLevel;
	protected:
	public:
		CameraLookComponent(Entity * owner);

		void Update() override;
		void LateUpdate() override;

		void MoveLeft();
		void MoveRight();
		void MoveForward();
		void MoveBackward();
		void MoveUp();
		void MoveDown();
	};
}