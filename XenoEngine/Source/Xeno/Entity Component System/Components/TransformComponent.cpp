#include "TransformComponent.h"
#include <Xeno/EngineIncludes.h>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Xeno/World/WorldManager.h>

namespace Xeno
{
	TransformComponent::TransformComponent(Entity* owner) :
		Component(owner)
	{
		SetPosition(Types::Vector3(0.0f, 0.0f, 0.0f));
		SetEulerRotation(Types::Vector3(0.0f, 0.0f, 0.0f));
		SetScale(Types::Vector3(1.0f, 1.0f, 1.0f));
	}

	void TransformComponent::Translate(Types::Vector3 translation)
	{
		m_position += glm::vec4(translation.x, translation.y, translation.z, 1.0f);
	}

	void TransformComponent::Rotate(Types::Quaternion rotation)
	{
		m_rotation *= rotation;
	}

	void TransformComponent::EulerRotate(Types::Vector3 eulerRotation)
	{
		m_rotation *= glm::quat(eulerRotation);
	}

	void TransformComponent::RotateAxis(Types::Vector3 axis, float amount)
	{
		m_rotation *= glm::angleAxis(amount, axis);
	}

	void TransformComponent::Scale(Types::Vector3 scale)
	{
		m_scale += scale;
	}

	void TransformComponent::SetPosition(Types::Vector3 position)
	{
		m_position = Types::Vector4(position.x, position.y, position.z, 1.0f);
	}

	Types::Vector3 TransformComponent::GetPosition()
	{
		return m_position;
	}

	void TransformComponent::SetRotation(Types::Quaternion rotation)
	{
		m_rotation = rotation;
	}

	Types::Quaternion TransformComponent::GetRotation()
	{
		return m_rotation;
	}

	void TransformComponent::SetEulerRotation(Types::Vector3 rotation)
	{
		m_rotation = glm::quat(rotation);
	}

	Types::Vector3 TransformComponent::GetEulerRotation()
	{
		return glm::radians(glm::eulerAngles(m_rotation)); // Might have to do something else here, idk.
	}

	void TransformComponent::SetScale(Types::Vector3 scale)
	{
		m_scale = scale;
	}

	Types::Vector3 TransformComponent::GetScale()
	{
		return m_scale;
	}

	void TransformComponent::Start()
	{
		XENO_LOG("Xeno::TransformComponent::Start()");
	}

	void TransformComponent::Update()
	{
	}

	void TransformComponent::LateUpdate()
	{

	}

}