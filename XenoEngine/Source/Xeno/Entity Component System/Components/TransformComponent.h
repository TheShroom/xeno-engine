#pragma once
#include <Xeno/EngineDefines.h>
#include "../Component.h"
#include "Graphics/CameraComponent.h"
#include <Xeno/Types/Types.h>

namespace Xeno
{
	class ENGINEAPI TransformComponent : public Component
	{
	private:
		Types::Vector3 m_scale;
		Types::Vector4 m_position;
		Types::Quaternion m_rotation;

		Types::Vector3 m_localPosition;

	protected:
	public:
		TransformComponent(Entity* owner);

		void Translate(Types::Vector3 translation);
		void Rotate(Types::Quaternion rotation);
		void EulerRotate(Types::Vector3 eulerRotation);
		void RotateAxis(Types::Vector3 axis, float degrees);
		void Scale(Types::Vector3 scale);

		void SetPosition(Types::Vector3 position);
		Types::Vector3 GetPosition();

		void SetRotation(Types::Quaternion rotation);
		Types::Quaternion GetRotation();

		void SetEulerRotation(Types::Vector3);
		Types::Vector3 GetEulerRotation();

		void SetScale(Types::Vector3 scale);
		Types::Vector3 GetScale();

		void Start();
		void Update();
		void LateUpdate();
	};
}