#pragma once

/* Contains includes for all built-in components. */

// Basic components
#include "TransformComponent.h"

// Graphics components
#include "Graphics/RendererComponent.h"
#include "Graphics/MeshComponent.h"
#include "Graphics/MaterialComponent.h"
#include "Graphics/CameraComponent.h"

// Physics components
#include "Physics/Rigidbody2DComponent.h"

// Temporary components
#include "CameraLookComponent.h"