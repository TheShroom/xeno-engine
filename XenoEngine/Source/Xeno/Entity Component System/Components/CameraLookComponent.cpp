#include "CameraLookComponent.h"
#include "../Entity.h"
#include <Xeno/Input/Input.h>
#include <Xeno/Input/Cursor.h>
#include <Xeno/Time/Time.h>

namespace Xeno
{
	CameraLookComponent::CameraLookComponent(Entity * owner) :
		Component(owner)
	{
		m_transformComponent = m_owner->GetComponent<TransformComponent>();
		m_cameraComponent = m_owner->GetComponent<CameraComponent>();
	}

	void CameraLookComponent::Update()
	{
		m_cursorPos = Cursor::GetPosition();
		float fov = m_cameraComponent->GetCamera()->GetFieldOfView();
		fov = fov + -Input::GetScrollAmount().y * m_zoomSensitivity;
		fov = Math::Clamp(fov, 1.0, 180.0f);
		m_zoomLevel = fov / 180.0f;
		m_cameraComponent->GetCamera()->SetFieldOfView(fov);

		// 13.0f is the division required to remove the difference between FOV and ortho size
		// in order to make objects same size in window on both perspective and orthographic, I think?
		// when a sprite is on position (0.0, 0.0f, 0.0) on orthographic with cameraSize 5.0f, it has the same size as
		// the sprite on same position but with 65 FOV on perspective view, therefore the cameraSize will always be 13 times 
		// bigger than the orthographic cameraSize, so we need to divide the viewSize by 13 to maintain the size.
		// This might be totally incorrect, but it does make some sense in my head. lol
		m_cameraComponent->GetCamera()->SetViewSize(fov / 13.0f);

		if (m_cameraComponent->GetCameraMode() == Graphics::Camera::CameraMode::Perspective)
		{
			if (Input::IsKeyHeld(GLFW_KEY_A))
			{
				MoveLeft();
			}

			if (Input::IsKeyHeld(GLFW_KEY_D))
			{
				MoveRight();
			}

			if (Input::IsKeyHeld(GLFW_KEY_W))
			{
				MoveForward();
			}

			if (Input::IsKeyHeld(GLFW_KEY_S))
			{
				MoveBackward();
			}

			if (Input::IsKeyHeld(GLFW_KEY_LEFT_SHIFT))
			{
				MoveUp();
			}

			if (Input::IsKeyHeld(GLFW_KEY_LEFT_CONTROL))
			{
				MoveDown();
			}
		}
		else if (m_cameraComponent->GetCameraMode() == Graphics::Camera::CameraMode::Orthographic)
		{
			if (Input::IsKeyHeld(GLFW_KEY_A))
			{
				MoveLeft();
			}

			if (Input::IsKeyHeld(GLFW_KEY_D))
			{
				MoveRight();
			}

			if (Input::IsKeyHeld(GLFW_KEY_W))
			{
				MoveUp();
			}

			if (Input::IsKeyHeld(GLFW_KEY_S))
			{
				MoveDown();
			}
		}

		if (Input::IsMouseButtonHeld(GLFW_MOUSE_BUTTON_RIGHT))
		{
			m_transformComponent->SetPosition(
				m_transformComponent->GetPosition() +
				Types::Vector3((
					m_cursorPosOffset.x * m_dragSensitivity) * m_zoomLevel,
					(-m_cursorPosOffset.y * m_dragSensitivity) * m_zoomLevel,
					0.0f
				)
			);
		}
	}

	void CameraLookComponent::LateUpdate()
	{
		m_cursorPosOffset = m_cursorPosLastFrame - m_cursorPos;
		m_cursorPosOffset.x = Math::Clamp(m_cursorPosOffset.x, -20.0f, 20.0f);
		m_cursorPosOffset.y = Math::Clamp(m_cursorPosOffset.y, -20.0f, 20.0f);
		m_cursorPosLastFrame = m_cursorPos;
	}

	void CameraLookComponent::MoveLeft()
	{
		m_transformComponent->SetPosition(m_transformComponent->GetPosition() + Types::Vector3(m_speed * Time::GetDeltaTime(), 0.0f, 0.0f));
	}

	void CameraLookComponent::MoveRight()
	{
		m_transformComponent->SetPosition(m_transformComponent->GetPosition() + Types::Vector3(-m_speed * Time::GetDeltaTime(), 0.0f, 0.0f));
	}

	void CameraLookComponent::MoveForward()
	{
		m_transformComponent->SetPosition(m_transformComponent->GetPosition() + Types::Vector3(0.0f, 0.0f, -m_speed * Time::GetDeltaTime()));
	}

	void CameraLookComponent::MoveBackward()
	{
		m_transformComponent->SetPosition(m_transformComponent->GetPosition() + Types::Vector3(0.0f, 0.0f, m_speed * Time::GetDeltaTime()));
	}

	void CameraLookComponent::MoveUp()
	{
		m_transformComponent->SetPosition(m_transformComponent->GetPosition() + Types::Vector3(0.0f, -m_speed * Time::GetDeltaTime(), 0.0f));
	}

	void CameraLookComponent::MoveDown()
	{
		m_transformComponent->SetPosition(m_transformComponent->GetPosition() + Types::Vector3(0.0f, m_speed * Time::GetDeltaTime(), 0.0f));
	}
}