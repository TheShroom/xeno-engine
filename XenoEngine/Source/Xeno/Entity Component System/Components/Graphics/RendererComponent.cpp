#include "RendererComponent.h"
#include <Xeno/Debug/XenoDebug.h>
#include "../../Entity.h"
#include <Xeno/World/WorldManager.h>
#include <Xeno/Logging/Logger.h>
#include <string>
#include <glm/gtx/string_cast.hpp>

#include <Xeno/Time/Time.h>

namespace Xeno
{
	RendererComponent::RendererComponent(Entity* owner) :
		Component(owner), m_renderer(&Graphics::Renderer::GetInstance())
	{
		m_meshComponent = m_owner->GetComponent<MeshComponent>();
		m_materialComponent = m_owner->GetComponent<MaterialComponent>();
		m_transformComponent = m_owner->GetComponent<TransformComponent>();
		m_mainCameraComponent = WorldManager::GetEntityWithTag("MainCamera")->GetComponent<CameraComponent>();

		m_renderable.Initialize();
		m_renderable.SetMaterial(m_materialComponent->GetMaterial());
		m_renderable.SetMesh(*m_meshComponent->GetMesh());
		m_renderable.UpdateMeshData();

		m_mvpLocation = m_materialComponent->GetMaterial()->GetUniformIdByName("mvp");
		m_texCoordLocation = m_materialComponent->GetMaterial()->GetUniformIdByName("texCoords");

		m_visible = true;
	}

	void RendererComponent::Start()
	{

	}

	void RendererComponent::Update()
	{
		Render();
	}

	void RendererComponent::LateUpdate()
	{

	}

	void RendererComponent::Render()
	{
		glm::mat4 model = glm::mat4(1.0f);

		glm::mat4 scaleMatrix = glm::mat4(1.0f);
		scaleMatrix = glm::scale(model, m_transformComponent->GetScale());

		glm::mat4 rotationMatrix = glm::mat4(1.0f);
		rotationMatrix = glm::toMat4(m_transformComponent->GetRotation());

		glm::mat4 transformationMatrix = glm::mat4(1.0f);
		transformationMatrix = glm::translate(model, glm::vec3(m_transformComponent->GetPosition()));

		model = transformationMatrix * rotationMatrix * scaleMatrix;

		glm::mat4 view = glm::mat4(1.0f);

		// view = glm::lookAt(
		// 	glm::vec3(m_mainCameraComponent->GetOwner()->GetComponent<TransformComponent>()->GetPosition()) + glm::vec3(0.0f, 0.0f, -1.0f), // Camera direction?
		// 	m_mainCameraComponent->GetLookPosition(), // Look position
		// 	glm::vec3(0.0f, 1.0f, 0.0f)
		// ); // World Up Direction
		
		view = glm::lookAt(
			m_mainCameraComponent->GetOwner()->GetComponent<TransformComponent>()->GetPosition(),
			m_mainCameraComponent->GetOwner()->GetComponent<TransformComponent>()->GetPosition() + m_mainCameraComponent->GetCamera()->GetFrontDir(),
			m_mainCameraComponent->GetCamera()->GetWorldUp() // This should really be the camera up direction but there's no way of calculating that currently
		);

		glm::mat4 projection = glm::mat4(1.0f);
		projection = m_mainCameraComponent->GetCameraProjectionMatrix();

		glm::mat4 mvp = projection * view * model;
		m_materialComponent->GetMaterial()->SetUniformById(m_mvpLocation, mvp);

		// Only draw if the renderer is set to be visible
		if (m_visible)
		{
			m_renderer->Render(&m_renderable);
		}
	}

	Graphics::Mesh* RendererComponent::GetMesh()
	{
		return m_meshComponent->GetMesh();
	}

	Graphics::Material* RendererComponent::GetMaterial()
	{
		return m_materialComponent->GetMaterial();
	}

	void RendererComponent::SetWindow(Graphics::Window* window)
	{
		m_window = window;
	}

	void RendererComponent::SetVisible(bool visible)
	{
		m_visible = visible;
	}
}