#include "CameraComponent.h"
#include <GLFW/glfw3.h>

namespace Xeno
{
	CameraComponent::CameraComponent(Entity* owner) :
		Component(owner)
	{
		
	}

	void CameraComponent::SetCamera(Graphics::Camera* camera)
	{
		m_camera = camera;
	}

	Graphics::Camera* CameraComponent::GetCamera()
	{
		return m_camera;
	}

	void CameraComponent::SetLookPosition(glm::vec3 lookPosition)
	{
		m_lookPosition = lookPosition;
	}

	glm::vec3 CameraComponent::GetLookPosition()
	{
		return m_lookPosition;
	}

	glm::highp_mat4 CameraComponent::GetCameraProjectionMatrix()
	{
		return m_camera->GetCameraProjectionMatrix();
	}

	void CameraComponent::SetCameraMode(Graphics::Camera::CameraMode cameraMode)
	{
		m_camera->SetCameraMode(cameraMode);
	}

	Graphics::Camera::CameraMode CameraComponent::GetCameraMode()
	{
		return m_camera->GetCameraMode();
	}
}