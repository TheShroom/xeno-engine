#include "MeshComponent.h"

namespace Xeno
{
	MeshComponent::MeshComponent(Entity* owner) :
		Component(owner)
	{

	}

	MeshComponent::MeshComponent(Entity* owner, Graphics::Mesh mesh) :
		Component(owner)
	{
		SetMesh(mesh);
	}

	Graphics::Mesh* MeshComponent::GetMesh()
	{
		return &m_mesh;
	}

	void MeshComponent::SetMesh(Graphics::Mesh mesh)
	{
		m_mesh = mesh;
	}

	void MeshComponent::Start()
	{

	}

	void MeshComponent::Update()
	{

	}

	void MeshComponent::LateUpdate()
	{

	}
}