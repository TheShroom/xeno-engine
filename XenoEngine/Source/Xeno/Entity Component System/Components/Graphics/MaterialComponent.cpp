#include "MaterialComponent.h"
#include "MeshComponent.h"
#include <Xeno/Graphics/Mesh.h>
#include <Xeno/Graphics/ImageLoader.h>
#include "../../Entity.h"
#include <vector>

namespace Xeno
{
	MaterialComponent::MaterialComponent(Entity* owner) :
		Component(owner)
	{
	}

	void MaterialComponent::Start()
	{

	}

	void MaterialComponent::Update()
	{

	}

	void MaterialComponent::LateUpdate()
	{

	}

	Graphics::Material* MaterialComponent::GetMaterial()
	{
		return m_material;
	}

	void MaterialComponent::SetMaterial(Graphics::Material* material)
	{
		m_material = material;
	}
}