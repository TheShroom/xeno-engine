#pragma once
#include "../../Component.h"
#include <Xeno/Graphics/Material.h>

namespace Xeno
{
	class ENGINEAPI MaterialComponent : public Component
	{
	private:
		Graphics::Material* m_material;
	protected:
	public:
		MaterialComponent(Entity* owner);

		void Start();
		void Update();
		void LateUpdate();

		Graphics::Material* GetMaterial();
		void SetMaterial(Graphics::Material* material);
	};
}