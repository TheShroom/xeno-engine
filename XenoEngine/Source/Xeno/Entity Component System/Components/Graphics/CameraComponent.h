#pragma once
#include <Xeno/Entity Component System/Component.h>
#include <Xeno/EngineDefines.h>
#include <Xeno/Graphics/Camera.h>
#include "../TransformComponent.h"
#include <glm/glm.hpp>

namespace Xeno
{
	class ENGINEAPI CameraComponent : public Component
	{
	private:
		Graphics::Camera* m_camera;
		glm::vec3 m_lookPosition;
	protected:
	public:
		CameraComponent(Entity* owner);

		void SetCamera(Graphics::Camera* camera);
		Graphics::Camera* GetCamera();
		void SetLookPosition(glm::vec3 lookPosition);
		glm::vec3 GetLookPosition();
		glm::highp_mat4 GetCameraProjectionMatrix();
		void SetCameraMode(Graphics::Camera::CameraMode cameraMode);
		Graphics::Camera::CameraMode GetCameraMode();
	};
}