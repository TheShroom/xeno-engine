#pragma once
#include <Xeno/EngineDefines.h>
#include "../../Component.h"
#include "MaterialComponent.h"
#include "../TransformComponent.h"
#include "MeshComponent.h"
#include <Xeno/Graphics/Renderer.h>
#include <Xeno/Graphics/Renderable.h>
#include "CameraComponent.h"
#include <Xeno/Graphics/Window.h>

namespace Xeno
{
	class ENGINEAPI RendererComponent : public Component
	{
	private:
		Graphics::Renderer* m_renderer;

		TransformComponent* m_transformComponent;
		MeshComponent* m_meshComponent;
		MaterialComponent* m_materialComponent;
		CameraComponent* m_mainCameraComponent;

		Graphics::Renderable m_renderable;

		int m_modelMatrixLocation;
		int m_viewMatrixLocation;
		int m_projectionMatrixLocation;
		int m_mvpLocation;
		int m_texCoordLocation;

		bool m_visible;

		Graphics::Window* m_window;
	protected:
	public:
		RendererComponent(Entity* owner);

		void Start();
		void Update();
		void LateUpdate();

		void Render();

		Graphics::Mesh* GetMesh();
		Graphics::Material* GetMaterial();

		void SetWindow(Graphics::Window* window);
		void SetVisible(bool visible);
	};
}