#pragma once
#include "../../Component.h"
#include <Xeno/EngineDefines.h>
#include <Xeno/Graphics/Mesh.h>

namespace Xeno
{
	class ENGINEAPI MeshComponent : public Component
	{
	private:
		Graphics::Mesh m_mesh;
	protected:
	public:
		MeshComponent(Entity* owner);
		MeshComponent(Entity* owner, Graphics::Mesh mesh);

		Graphics::Mesh* GetMesh();
		void SetMesh(Graphics::Mesh mesh);

		void Start() override;
		void Update() override;
		void LateUpdate() override;
	};
}