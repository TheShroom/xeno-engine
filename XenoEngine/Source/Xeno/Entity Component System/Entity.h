#pragma once
#include <vector>
#include <Xeno/EngineIncludes.h>
#include <Xeno/EngineDefines.h>
#include <Xeno/Types/Types.h>
#include "Component.h"
#include <Xeno/Serialization/ISerializable.h>
#include <nlohmann-json/json.hpp>

namespace Xeno
{
	class ENGINEAPI Entity : public ISerializable
	{
	private:
		std::string m_name;
		std::string m_tag;
		std::vector<Component*> m_components;

		void UpdateComponents();
		void LateUpdateComponents();

		void AddToWorld();
		void SetupDefaultComponents();
	protected:
	public:
		Entity();
		Entity(const std::string& name);
		Entity(const std::string& name, std::vector<Component> components);
		Entity(std::vector<Component> components);

		~Entity() { XENO_LOG("Entity destructed."); }

		void SetName(const std::string& name);
		std::string GetName();

		void SetTag(const std::string& tag);
		std::string GetTag();

		void Update();
		void LateUpdate();

		nlohmann::json Serialize() override;
		void Deserialize(nlohmann::json json) override;

		template<class T>
		T* AddComponent()
		{
			static_assert(std::is_base_of<Component, T>::value, "Type parameter 'T' does not derive from Component.");

			if (!IsComponentAdded<T>())
			{
				m_components.push_back(new T(this));
				return (T*)m_components[m_components.size() - 1]; // Gets the last component in the vector which is the one we just added.
			}
			else
			{
				XENO_LOG_WARNING("Tried to add a component which was already added to the entity.");
				return nullptr;
			}
		}

		template<class T>
		void DeleteComponent()
		{
			static_assert(std::is_base_of<Component, T>::value, "Type parameter 'T' does not derive from Component.");

			for (unsigned int i = 0; i < m_components.size(); i++)
			{
				if (typeid(*m_components[i]).hash_code() == typeid(T).hash_code())
				{
					m_components.erase(m_components.begin() + i);
					return;
				}
			}

			XENO_LOG_WARNING("Tried to remove a component, but no such component exists on the entity.");
		}

		template<class T>
		T* GetComponent()
		{
			static_assert(std::is_base_of<Component, T>::value, "Type parameter 'T' does not derive from Component.");

			Entity* th = this;
			for (unsigned int i = 0; i < m_components.size(); i++)
			{
				if (typeid(*m_components[i]).hash_code() == typeid(T).hash_code())
				{
					return dynamic_cast<T*>(m_components[i]);
				}
			}

			XENO_LOG_WARNING("Tried to get a component which doesn't exist on entity.");
			return nullptr;
		}

		template<class T>
		bool IsComponentAdded()
		{
			static_assert(std::is_base_of<Component, T>::value, "Type parameter 'T' does not derive from Component.");

			for (unsigned int i = 0; i < m_components.size(); i++)
			{
				if (typeid(*m_components[i]).hash_code() == typeid(T).hash_code())
				{
					return true;
				}
			}

			return false;
		}
	};
}