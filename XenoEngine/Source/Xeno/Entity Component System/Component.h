#pragma once
#include <Xeno/EngineDefines.h>
#include <vector>

namespace Xeno
{
	class Entity;

	class ENGINEAPI Component
	{
	private:
	protected:
		Entity* m_owner;
		bool m_renderDefaultUI; // This is set to true by default.
	public:
		Component(Entity* owner);

		virtual void Start() { };
		virtual void Update() { };
		virtual void LateUpdate() { };

		Entity* GetOwner();
	};
}