#pragma once

/****** Todo *******/
/* Add cross-platform DLL export ( http://gcc.gnu.org/wiki/Visibility ) */

// Declare macros for exporting and importing DLLs just to make code more readable.
#define DLLEXPORT __declspec(dllexport)
#define DLLIMPORT __declspec(dllimport)

// Declare the ENGINEAPI macro to make it simple to create classes that are part of the engine.
#define ENGINEAPI DLLEXPORT

// GLEW is linked statically to the engine, so we need to tell GLEW that we are doing just that.
// We area also building the engine as a DLL so we need to let GLEW know about that, too.
#define GLEW_STATIC
#define GLEW_BUILD

// Xeno engine debug flag
#define XENO_DEBUG _DEBUG

// Make sure NULL is consistent.
#define NULL 0
#define NULLPTR nullptr

// Works like __FILE__ except it only includes the actual file name and not the path.
// Note: Changed from std::string to Types::String, had to change substr to SubString, find_last_of to FindLastOf and length to Count.
#define XENO__FILE__ { std::string filep = std::string(__FILE__); filep.substr(filep.find_last_of('\\') + 1, filep.length()); #filep.c_str(); }