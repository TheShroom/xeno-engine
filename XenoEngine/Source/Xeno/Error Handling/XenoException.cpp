#include "XenoException.h"

namespace Xeno
{
	namespace Exceptions
	{

		XenoException::XenoException(const std::string& eMsg, const std::string& file, int line)
		{
			m_message = std::string();
			m_message += "File: " + file + "\n\t";
			m_message += "Line: " + std::to_string(line) + "\n\t";
			m_message += "Message: " + eMsg;

			m_file = file;
			m_line = line;
		}

		XenoException::XenoException(std::exception e, const std::string& file, int line)
		{
			m_message = std::string();
			m_message += "File: " + file + "\n\t";
			m_message += "Line: " + std::to_string(line) + "\n\t";
			m_message += std::string("Message: ") + e.what();

			m_file = file;
			m_line = line;
		}

		std::string XenoException::GetExceptionMessage()
		{
			return m_message;
		}

		std::string XenoException::GetFile()
		{
			return m_file;
		}

		int XenoException::GetLine()
		{
			return m_line;
		}
	}
}