#pragma once
#include <exception>
#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Exceptions
	{
		class ENGINEAPI ExceptionHandler
		{
		private:
		public:
			static void HandleException(std::exception e);
			static void HandleException(); // Handles an unknown exception with no information.
		protected:
		};
	}
}