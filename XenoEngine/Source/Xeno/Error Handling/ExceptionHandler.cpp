#include "ExceptionHandler.h"
#include <Xeno/Logging/Logger.h>
#include <Xeno/Debug/XenoDebug.h>

namespace Xeno
{
	namespace Exceptions
	{
		void ExceptionHandler::HandleException(std::exception e)
		{
			XENO_LOG_ERROR(std::string("Unhandled exception caught in XenoEngine!\n\tException information:\n\t") + e.what());
		}

		void ExceptionHandler::HandleException()
		{
			XENO_LOG_ERROR("Unknown exception caught in XenoEngine!\n\tNo information was supplied with the exception, sorry. :(");
		}
	}
}