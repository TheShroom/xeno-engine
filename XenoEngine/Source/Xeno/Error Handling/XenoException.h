#pragma once
#include <exception>
#include <string>
#include <Xeno/Types/Types.h>
#include <Xeno/EngineDefines.h>

/* 
 * Always use this macro to throw exceptions instead of using throw().
 * Using throw() could cause the exception to not be reported properly
 * and is considered undefined behavior!
 * */

// Todo: Remove comment
#define XENO_THROW_EXCEPTION(msg) Xeno::Exceptions::XenoException exception = Xeno::Exceptions::XenoException(msg, StringHelper::RemovePath(std::string(__FILE__)), __LINE__); exception.Throw();

namespace Xeno
{
	namespace Exceptions
	{
		class ENGINEAPI XenoException
		{
		private:
			std::string m_message;
			std::string m_file;
			int m_line;
		public:
			XenoException(std::exception e, const std::string& file, int line);
			XenoException(const std::string& eMsg, const std::string& file, int line);

			std::string GetExceptionMessage();
			std::string GetFile();
			int GetLine();

			inline void Throw() { throw(std::exception(m_message.c_str())); };
		protected:
		};
	}
}