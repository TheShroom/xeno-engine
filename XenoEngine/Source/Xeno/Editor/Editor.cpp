#include "Editor.h"
#include <Xeno/Input/Input.h>

namespace Xeno
{
	namespace Editor
	{
		Editor& Editor::GetInstance()
		{
			static Editor instance;
			return instance;
		}

		Editor::Editor()
		{
			
		}

		void Editor::UpdateEditor()
		{
			if (Input::IsKeyDown(GLFW_KEY_F10))
			{
				if (!m_isEditorOpen)
				{
					OpenEditor();
				}
				else
				{
					CloseEditor();
				}
			}
		}

		void Editor::OpenEditor()
		{
			if (m_isEditorOpen) return; // Already open, return.



			m_isEditorOpen = true;
		}

		void Editor::CloseEditor()
		{
			if (!m_isEditorOpen) return; // Already closed, return.
			


			m_isEditorOpen = false;
		}

		bool Editor::IsEditorOpen()
		{
			return m_isEditorOpen;
		}

	}
}