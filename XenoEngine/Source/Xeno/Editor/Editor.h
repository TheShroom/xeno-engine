#pragma once
#include <Xeno/EngineDefines.h>
namespace Xeno
{
	namespace Editor
	{

		class ENGINEAPI Editor
		{
		private:
			Editor(); // Private since this is a singleton.

			bool m_isEditorOpen;
		protected:
		public:
			static Editor& GetInstance();

			void UpdateEditor();
			bool IsEditorOpen();

			// Opens the editor window, calls Initialize().
			void OpenEditor();
			// Closes the editor window, calls Cleanup().
			void CloseEditor();
		};

	}
}