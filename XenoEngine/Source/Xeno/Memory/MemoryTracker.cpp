#include "MemoryTracker.h"

namespace Xeno
{
	namespace Memory
	{
		size_t MemoryTracker::m_heapMemoryUsage = 0;
		size_t MemoryTracker::m_stackMemoryUsage = 0;

		void MemoryTracker::Initialize()
		{
			AddMemoryTrack(sizeof(m_heapMemoryUsage));
			AddMemoryTrack(sizeof(m_stackMemoryUsage));
		}

		void MemoryTracker::AddMemoryTrack(size_t size)
		{
			m_stackMemoryUsage += size;
		}

		void MemoryTracker::RemoveMemoryTrack(size_t size)
		{
			m_stackMemoryUsage -= size;
		}

		size_t MemoryTracker::GetHeapMemoryUsage()
		{
			return m_heapMemoryUsage;
		}

		size_t MemoryTracker::GetStackMemoryUsage()
		{
			return m_stackMemoryUsage;
		}
	}
}