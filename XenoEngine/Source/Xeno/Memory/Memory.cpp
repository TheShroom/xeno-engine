#include "Memory.h"
#include <memory>

namespace Xeno
{
	namespace Utility
	{
		void Memory::SetMemory(void* ptr, int size, int set)
		{
			for (unsigned int i = 0; i < size; i++)
			{
				((int*)ptr)[i] = set;
			}
		}

		void Memory::ZeroMemory(void* ptr, int size)
		{
			SetMemory(ptr, size, 0);
		}

		void* Memory::AllocateMemory(int size)
		{
			return malloc(size);
		}

		void Memory::FreeMemory(void* block)
		{
			free(block);
		}
	}
}