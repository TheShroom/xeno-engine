#pragma once

#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Memory
	{

		static class ENGINEAPI MemoryTracker
		{
		private:
			static size_t m_heapMemoryUsage;
			static size_t m_stackMemoryUsage;
		public:
			static void Initialize();
			inline static void AddMemoryTrack(size_t size);
			inline static void RemoveMemoryTrack(size_t size);

			static size_t GetHeapMemoryUsage();
			static size_t GetStackMemoryUsage();
		};

	}
}