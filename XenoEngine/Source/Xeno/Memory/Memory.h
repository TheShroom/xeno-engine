#pragma once

#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Utility
	{
		// Contains utility functions related to memory.
		class ENGINEAPI Memory
		{
		private:
		protected:
		public:
			// Sets all memory from ptr to ptr[size] to the value of set.
			static void SetMemory(void* ptr, int size, int set);
			// Sets all memory from ptr to ptr[size] to 0.
			static void ZeroMemory(void* ptr, int size);

			// Allocates a block of memory and returns the starting pointer of the block.
			static void* AllocateMemory(int size);
			// Frees a block of memory.
			static void FreeMemory(void* block);
		};
	}
}