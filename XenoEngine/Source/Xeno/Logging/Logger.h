#pragma once

#include <Xeno/EngineIncludes.h>
#include <string>
#include <iostream>
#include <Xeno/Types/String.h>



// Handles the formatting of the log messages. This can be used directly if you want custom log tags.
// errType is the log tag that will show up.
// This can be any string, such as WARNING, ERROR,LOG, BANANA or THE CAKE IS A LIE
#define XENO_LOG_CUSTOM(errType, x, ...) { \
	std::string logMsg = std::string(); \
	std::string filePath = __FILE__;  \
	filePath = StringHelper::RemovePath(filePath); \
	char* buff; \
	std::string line = std::to_string(__LINE__); \
	logMsg += "["; \
	logMsg += errType; \
	logMsg += "] ["; \
	logMsg += filePath; \
	logMsg += "] [Line "; \
	logMsg += line; \
	logMsg += "] -> "; \
	logMsg += x; \
	Xeno::Logging::Logger::GetInstance().Log(logMsg, __VA_ARGS__); \
} \



// Predefined logging functions

#define XENO_LOG(x, ...) XENO_LOG_CUSTOM("LOG", x, __VA_ARGS__)
#define XENO_LOG_WARNING(x, ...) XENO_LOG_CUSTOM("WARNING", x, __VA_ARGS__)
#define XENO_LOG_ERROR(x, ...) XENO_LOG_CUSTOM("ERROR", x, __VA_ARGS__)



namespace Xeno
{
	namespace Logging
	{

		class ENGINEAPI Logger
		{
		private:
			Logger();
		protected:
		public:
			static Logger& GetInstance();

			template<typename T, typename ... Args>
			void Log(T message, Args... args)
			{
				std::cout << message;
				Log(args...);
			}

			template<typename T>
			void Log(T s)
			{
				std::cout << s << std::endl;
			}
		};

	}
}