#include "FloatType.h"
#include <Xeno/Memory/MemoryTracker.h>

namespace Xeno
{
	namespace Types
	{

		/**
			Constructors
		*/

		Float::Float()
		{
			Memory::MemoryTracker::AddMemoryTrack(sizeof(Float));
		}

		Float::Float(const Float& other)
		{
			Set(other);
		}

		Float::Float(const float& other)
		{
			Set(other);
		}

		Float::~Float()
		{
			Memory::MemoryTracker::RemoveMemoryTrack(sizeof(Float));
		}

		/**
			Mathematical Methods
		*/

		Float& Float::Add(const Float& other)
		{
			m_value += other;
			return *this;
		}

		Float& Float::Subtract(const Float& other)
		{
			m_value -= other;
			return *this;
		}

		Float& Float::Multiply(const Float& other)
		{
			m_value *= other;
			return *this;
		}

		Float& Float::Divide(const Float& other)
		{
			m_value /= other;
			return *this;
		}

		/**
			Getters & Setters
		*/

		void Float::Set(const float& other)
		{
			m_value = other;
		}

		float Float::Get() const {
			return m_value;
		}

		/**
			Operator Overloads
		*/

		Float& Float::operator+=(const Float& other)
		{
			return Add(other);
		}

		Float& Float::operator+=(const float& other)
		{
			return Add(other);
		}

		Float& Float::operator-=(const Float& other)
		{
			return Subtract(other);
		}

		Float& Float::operator-=(const float& other)
		{
			return Subtract(other);
		}

		Float& Float::operator*=(const Float& other)
		{
			return Multiply(other);
		}

		Float& Float::operator*=(const float& other)
		{
			return Multiply(other);
		}

		Float& Float::operator/=(const Float& other)
		{
			return Divide(other);
		}

		Float& Float::operator/=(const float& other)
		{
			return Divide(other);
		}

		Float::operator float() const
		{
			return Get();
		}

	}
}