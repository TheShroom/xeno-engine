#include "ShortIntType.h"
#include <Xeno/Memory/MemoryTracker.h>

namespace Xeno 
{
	namespace Types
	{

		/**
			Constructors
		*/

		SInt::SInt()
		{
			Memory::MemoryTracker::AddMemoryTrack(sizeof(SInt));
		}

		SInt::SInt(const SInt& other)
		{
			Set(other);
		}

		SInt::SInt(const short int& other)
		{
			Set(other);
		}

		SInt::~SInt()
		{
			Memory::MemoryTracker::RemoveMemoryTrack(sizeof(SInt));
		}

		/** 
			Mathematical Methods
		*/

		SInt& SInt::Add(const SInt& other)
		{
			m_value += other;
			return *this;
		}

		SInt& SInt::Subtract(const SInt& other)
		{
			m_value -= other;
			return *this;
		}

		SInt& SInt::Multiply(const SInt& other)
		{
			m_value *= other;
			return *this;
		}

		SInt& SInt::Divide(const SInt& other)
		{
			m_value /= other;
			return *this;
		}

		/**
			Getters & Setters
		*/

		void SInt::Set(const short int& other)
		{
			m_value = other;
		}

		short int SInt::Get() const
		{
			return m_value;
		}

		/**
			Operator Overloads
		*/

		SInt& SInt::operator+=(const SInt& other)
		{
			return Add(other);
		}

		SInt& SInt::operator+=(const short int& other)
		{
			return Add(other);
		}

		SInt& SInt::operator-=(const SInt& other)
		{
			return Subtract(other);
		}

		SInt& SInt::operator-=(const short int& other)
		{
			return Subtract(other);
		}

		SInt& SInt::operator*=(const SInt& other)
		{
			return Multiply(other);
		}

		SInt& SInt::operator*=(const short int& other)
		{
			return Multiply(other);
		}

		SInt& SInt::operator/=(const SInt& other)
		{
			return Divide(other);
		}

		SInt& SInt::operator/=(const short int& other)
		{
			return Divide(other);
		}

		SInt::operator short int() const
		{
			return Get();
		}

	}
}