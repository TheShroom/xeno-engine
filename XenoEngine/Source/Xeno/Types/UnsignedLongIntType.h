#pragma once

#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Types
	{

		class ENGINEAPI ULInt
		{
		private:
			// The value of the ULInt.
			unsigned long long int m_value;
		public:

			/**
				Constructors & Deconstructors
			*/

			// Default constructor, sets the initial value to 0.
			ULInt();

			// Copy constructors.
			ULInt(const ULInt& other);
			ULInt(const unsigned long long int& other);

			// Destructor
			~ULInt();

			/**
				Mathematical Mathods
			*/

			// Addition
			ULInt& Add(const ULInt& other);

			// Subtraction
			ULInt& Subtract(const ULInt& other);

			// Multiplication
			ULInt& Multiply(const ULInt& other);

			// Division
			ULInt& Divide(const ULInt& other);

			/**
				Getters & Setters
			*/

			// Sets the value of the unsigned long long int.
			void Set(const unsigned long long int& value);
			// Converts the ULInt to unsigned long long int and returns it.
			unsigned long long int Get() const;

			/**
				Operator Overloads
			*/

			// Add-assign operator.
			ULInt& operator+=(const ULInt& other);
			ULInt& operator+=(const unsigned long long int& other);

			// Subtract-assign operator.
			ULInt& operator-=(const ULInt& other);
			ULInt& operator-=(const unsigned long long int& other);

			// Multiply-assign operator.
			ULInt& operator*=(const ULInt& other);
			ULInt& operator*=(const unsigned long long int& other);

			// Divide-assign operator.
			ULInt& operator/=(const ULInt& other);
			ULInt& operator/=(const unsigned long long int& other);

			// Conversion operator to unsigned long long int.
			operator unsigned long long int() const;

		};

	}
}