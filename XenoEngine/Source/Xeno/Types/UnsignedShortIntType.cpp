#include "UnsignedShortIntType.h"
#include <Xeno/Memory/MemoryTracker.h>

namespace Xeno
{
	namespace Types
	{

		/**
			Constructors
		*/

		USInt::USInt()
		{
			Memory::MemoryTracker::AddMemoryTrack(sizeof(USInt));
		}

		USInt::USInt(const USInt& other)
		{
			Set(other);
		}

		USInt::USInt(const unsigned short int& other)
		{
			Set(other);
		}

		USInt::~USInt()
		{
			Memory::MemoryTracker::RemoveMemoryTrack(sizeof(USInt));
		}

		/**
			Mathematical Methods
		*/

		USInt& USInt::Add(const USInt& other)
		{
			m_value += other;
			return *this;
		}

		USInt& USInt::Increment()
		{
			m_value++;
			return *this;
		}

		USInt& USInt::Decrement()
		{
			m_value--;
			return *this;
		}

		USInt& USInt::Subtract(const USInt& other)
		{
			m_value -= other;
			return *this;
		}

		USInt& USInt::Multiply(const USInt& other)
		{
			m_value *= other;
			return *this;
		}

		USInt& USInt::Divide(const USInt& other)
		{
			m_value /= other;
			return *this;
		}

		/**
			Getters & Setters
		*/

		void USInt::Set(const unsigned short int& other)
		{
			m_value = other;
		}

		unsigned short int USInt::Get() const
		{
			return m_value;
		}

		/** 
			Operator Overloads
		*/

		USInt& USInt::operator+=(const USInt& other)
		{
			return Add(other);
		}

		USInt& USInt::operator+=(const unsigned short int& other)
		{
			return Add(other);
		}

		USInt& USInt::operator-=(const USInt& other)
		{
			return Subtract(other);
		}

		USInt& USInt::operator-=(const unsigned short int& other)
		{
			return Subtract(other);
		}

		USInt& USInt::operator*=(const USInt& other)
		{
			return Multiply(other);
		}

		USInt& USInt::operator*=(const unsigned short int& other)
		{
			return Multiply(other);
		}

		USInt& USInt::operator/=(const USInt& other)
		{
			return Divide(other);
		}

		USInt& USInt::operator/=(const unsigned short int& other)
		{
			return Divide(other);
		}

		USInt& USInt::operator++(int other)
		{
			return Increment();
		}

		USInt& USInt::operator++()
		{
			return Increment();
		}

		USInt& USInt::operator--(int other)
		{
			return Decrement();
		}

		USInt& USInt::operator--()
		{
			return Decrement();
		}

		USInt::operator unsigned short int() const
		{
			return Get();
		}

	}
}