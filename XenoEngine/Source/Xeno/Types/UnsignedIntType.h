#pragma once

#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Types
	{

		class ENGINEAPI UInt
		{
		private:
			// The value of the UInt.
			unsigned int m_value;
		public:

			/**
				Constructors & Deconstructors
			*/

			// Default constructor, sets the initial value to 0.
			UInt();

			// Copy constructors.
			UInt(const UInt& other);
			UInt(const unsigned int& other);

			// Destructor.
			~UInt();

			/**
				Nathematical Methods
			*/

			// Addition
			UInt& Add(const UInt& other);

			// Increment
			UInt& Increment();

			// Decrement
			UInt& Decrement();

			// Subtraction
			UInt& Subtract(const UInt& other);

			// Multiplication
			UInt& Multiply(const UInt& other);

			// Division
			UInt& Divide(const UInt& other);

			/** 
				Getters & Setters
			*/

			// Sets the value of the unsigned int.
			void Set(const unsigned int& value);
			// Converts the UInt to unsigned int and returns it.
			unsigned int Get() const;

			/**
				Operator Overloads
			*/

			// Add-assign operator.
			UInt& operator+=(const UInt& other);
			UInt& operator+=(const unsigned int& other);

			// Subtract-assign operator.
			UInt& operator-=(const UInt& other);
			UInt& operator-=(const unsigned int& other);

			// Multiply-assign operator.
			UInt& operator*=(const UInt& other);
			UInt& operator*=(const unsigned int& other);

			// Divide-assign operator.
			UInt& operator/=(const UInt& other);
			UInt& operator/=(const unsigned int& other);

			// Increment operator.
			UInt& operator++(int other);
			UInt& operator++();

			// Decrement operator.
			UInt& operator--(int other);
			UInt& operator--();

			// Conversion operator to unsigned int.
			operator unsigned int() const;

		};

	}
}