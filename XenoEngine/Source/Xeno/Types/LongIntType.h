#pragma once

#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Types
	{

		class ENGINEAPI LInt
		{
		private:
			// The value of the LInt.
			long long int m_value;
		public:

			/**
				Constructors & Deconstructors
			*/

			// Default constructors, sets the initial value to 0.
			LInt();

			// Copy constructors.
			LInt(const LInt& other);
			LInt(const long long int& other);

			// Destructor.
			~LInt();

			/**
				Mathematical Methods
			*/

			// Addition
			LInt& Add(const LInt& other);

			// Subtraction
			LInt& Subtract(const LInt& other);

			// Multiplication
			LInt& Multiply(const LInt& other);

			// Division
			LInt& Divide(const LInt& other);

			/** 
				Getters & Setters
			*/

			// Sets the value of the long long int.
			void Set(const long long int& value);
			// Converts the LInt to long long int and returns it.
			long long int Get() const;

			/**
				Operator Overloads
			*/

			// Add-assign operator
			LInt& operator+=(const LInt& other);
			LInt& operator+=(const long long int& other);

			// Subtract-assign operator
			LInt& operator-=(const LInt& other);
			LInt& operator-=(const long long int& other);

			// Multiply-assign operator
			LInt& operator*=(const LInt& other);
			LInt& operator*=(const long long int& other);
			
			// Divide-assign operator
			LInt& operator/=(const LInt& other);
			LInt& operator/=(const long long int& other);

			// Conversion operator to long long int.
			operator long long int() const;

		};

	}
}