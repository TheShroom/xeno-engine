#pragma once

#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Types
	{

		class ENGINEAPI Float
		{
		private:
			// The value of the float.
			float m_value;
		public:

			/**
				Constructors & Deconstructors
			*/

			// Default constructor, sets the inital value to 0.
			Float();

			// Copy constructors.
			Float(const Float& other);
			Float(const float& other);

			// Destructor.
			~Float();

			/**
				Mathematical Methods
			*/

			// Addition.
			Float& Add(const Float& other);

			// Subtraction
			Float& Subtract(const Float& other);

			// Multiplication
			Float& Multiply(const Float& other);

			// Division
			Float& Divide(const Float& other);

			/**
				Getters & Setters
			*/

			// Sets the value of the float.
			void Set(const float& value);
			// Converts the Float to float and returns it.
			float Get() const;

			/**
				Operator Overloads
			*/

			// Add-assign operator.
			Float& operator+=(const Float& other);
			Float& operator+=(const float& other);

			// Subtract-assign operator.
			Float& operator-=(const Float& other);
			Float& operator-=(const float& other);

			// Multiply-assign operator.
			Float& operator*=(const Float& other);
			Float& operator*=(const float& other);

			// Divide-assign operator.
			Float& operator/=(const Float& other);
			Float& operator/=(const float& other);

			// Conversion operator to float.
			operator float() const;
		};

	}
}