#include "BoolType.h"
#include <Xeno/Memory/MemoryTracker.h>

namespace Xeno
{
	namespace Types
	{

		/**
			Constructors
		*/

		Bool::Bool()
		{
			Memory::MemoryTracker::AddMemoryTrack(sizeof(Bool));
		}

		Bool::Bool(const Bool& other)
		{
			Set(other);
		}

		Bool::Bool(const bool& other)
		{
			Set(other);
		}

		Bool::~Bool()
		{
			Memory::MemoryTracker::RemoveMemoryTrack(sizeof(Bool));
		}

		/**
			Getters & Setters
		*/

		void Bool::Set(const bool& other)
		{
			m_value = other;
		}

		bool Bool::Get() const {
			return m_value;
		}

		Bool::operator bool() const
		{
			return Get();
		}

	}
}