#pragma once

#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Types
	{

		class ENGINEAPI Double
		{
		private:
			// The value of the double.
			double m_value;
		public:

			/**
				Constructors & Deconstructors
			*/

			// Default constructor, sets the inital value to 0.
			Double();

			// Copy constructors.
			Double(const Double& other);
			Double(const double& other);

			// Destructor.
			~Double();

			/**
				Mathematical Methods
			*/

			// Addition.
			Double& Add(const Double& other);

			// Subtraction
			Double& Subtract(const Double& other);

			// Multiplication
			Double& Multiply(const Double& other);

			// Division
			Double& Divide(const Double& other);

			/**
				Getters & Setters
			*/

			// Sets the value of the double.
			void Set(const double& value);
			// Converts the Double to double and returns it.
			double Get() const;

			/**
				Operator Overloads
			*/

			// Add-assign operator.
			Double& operator+=(const Double& other);
			Double& operator+=(const double& other);

			// Subtract-assign operator.
			Double& operator-=(const Double& other);
			Double& operator-=(const double& other);

			// Multiply-assign operator.
			Double& operator*=(const Double& other);
			Double& operator*=(const double& other);

			// Divide-assign operator.
			Double& operator/=(const Double& other);
			Double& operator/=(const double& other);

			// Conversion operator to float.
			operator double() const;
		};

	}
}