#include "String.h"

namespace Xeno
{
	namespace Types
	{

		std::string StringHelper::RemovePath(const std::string& str)
		{
			return str.substr(str.find_last_of('\\') + 1, str.length());
		}

		std::string StringHelper::RemoveMatchingString(const std::string& str, const std::string& substring)
		{
			Int matchedCount = 0;
			Int matchedIndex;
			bool isFirstMatch = true;
			for (Int i = 0; i < str.length(); i++)
			{
				if (str[i] == substring[matchedCount])
				{
					if (isFirstMatch) matchedIndex = i;
					matchedCount++;
					isFirstMatch = false;
				}
				else
				{
					matchedIndex = -1;
					matchedCount = 0;
					isFirstMatch = true;
				}
			}

			if (matchedCount != substring.length())
			{
				return str; // No matching substring could be found in target, return original.
			}

			std::string newString = std::string(str.length() - matchedCount, 'x');
			for (unsigned int i = 0; i < newString.length(); i++) // removed -1 (seems to have fixed it)
			{
				newString[i] = str[i];
			}

			return newString;
		}

	}
}