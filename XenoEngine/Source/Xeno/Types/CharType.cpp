#include "CharType.h"
#include <Xeno/Memory/MemoryTracker.h>

namespace Xeno
{
	namespace Types
	{

		/**
			Constructors
		*/

		Char::Char()
		{
			Memory::MemoryTracker::AddMemoryTrack(sizeof(Char));
		}

		Char::Char(const Char& other)
		{
			Set((char)other);
		}

		Char::Char(const char& other)
		{
			Set(other);
		}

		Char::~Char()
		{
			Memory::MemoryTracker::RemoveMemoryTrack(sizeof(Char));
		}

		/**
			Getters & Setters
		*/

		void Char::Set(const char& other)
		{
			m_value = other;
		}

		char Char::Get() const {
			return m_value;
		}

		Char::operator char() const
		{
			return Get();
		}

	}
}