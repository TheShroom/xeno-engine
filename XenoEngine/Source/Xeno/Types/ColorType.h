#pragma once
#include <Xeno/EngineIncludes.h>
#include <Xeno/Math/Math.h>

namespace Xeno
{
	namespace Types
	{
		class ENGINEAPI Color
		{
		public:
			static const Color black;
			static const Color white;
			static const Color grey;
			static const Color red;
			static const Color blue;
			static const Color green;

			float r, g, b, a;

			Color();
			Color(float r, float g, float b, float a);
			Color(const Color& other);

			Color Normalize();
			static Color ToNormalized(Color color);
			static Color FromNormalized(Color color);
		};

	}
}