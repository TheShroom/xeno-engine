#include "UnsignedIntType.h"
#include <Xeno/Memory/MemoryTracker.h>

namespace Xeno
{
	namespace Types
	{

		/**
			Constructors
		*/

		UInt::UInt()
		{
			Memory::MemoryTracker::AddMemoryTrack(sizeof(UInt));
		}

		UInt::UInt(const UInt& other)
		{
			Set(other);
		}

		UInt::UInt(const unsigned int& other)
		{
			Set(other);
		}

		UInt::~UInt()
		{
			Memory::MemoryTracker::RemoveMemoryTrack(sizeof(UInt));
		}

		/**
			Mathematical Methods
		*/

		UInt& UInt::Add(const UInt& other)
		{
			m_value += other;
			return *this;
		}

		UInt& UInt::Increment()
		{
			m_value++;
			return *this;
		}

		UInt& UInt::Decrement()
		{
			m_value--;
			return *this;
		}

		UInt& UInt::Subtract(const UInt& other)
		{
			m_value -= other;
			return *this;
		}

		UInt& UInt::Multiply(const UInt& other)
		{
			m_value *= other;
			return *this;
		}

		UInt& UInt::Divide(const UInt& other)
		{
			m_value /= other;
			return *this;
		}

		/**
			Getters & Setters
		*/

		void UInt::Set(const unsigned int& other)
		{
			m_value = other;
		}

		unsigned int UInt::Get() const
		{
			return m_value;
		}

		/**
			Operator Overloads
		*/

		UInt& UInt::operator+=(const UInt& other)
		{
			return Add(other);
		}

		UInt& UInt::operator+=(const unsigned int& other)
		{
			return Add(other);
		}
		
		UInt& UInt::operator-=(const UInt& other)
		{
			return Subtract(other);
		}

		UInt& UInt::operator-=(const unsigned int& other)
		{
			return Subtract(other);
		}

		UInt& UInt::operator*=(const UInt& other)
		{
			return Multiply(other);
		}

		UInt& UInt::operator*=(const unsigned int& other)
		{
			return Multiply(other);
		}

		UInt& UInt::operator/=(const UInt& other)
		{
			return Divide(other);
		}

		UInt& UInt::operator/=(const unsigned int& other)
		{
			return Divide(other);
		}

		UInt& UInt::operator++(int other)
		{
			return Increment();
		}

		UInt& UInt::operator++()
		{
			return Increment();
		}

		UInt& UInt::operator--(int other)
		{
			return Decrement();
		}

		UInt& UInt::operator--()
		{
			return Decrement();
		}

		UInt::operator unsigned int() const
		{
			return Get();
		}

	}
}