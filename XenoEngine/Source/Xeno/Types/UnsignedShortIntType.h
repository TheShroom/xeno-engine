#pragma once

#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Types
	{

		class ENGINEAPI USInt
		{
		private:
			// The value of the USInt.
			unsigned short int m_value;
		public:

			/**
				Constructors & Deconstructors
			*/

			// Default constructor, sets the initial value to 0.
			USInt();

			// Copy constructors.
			USInt(const USInt& other);
			USInt(const unsigned short int& other);

			// Destructor.
			~USInt();

			/**
				Mathematical Methods
			*/

			// Addition
			USInt& Add(const USInt& other);

			// Increment
			USInt& Increment();

			// Decrement
			USInt& Decrement();

			// Subtraction
			USInt& Subtract(const USInt& other);

			// Multiplication
			USInt& Multiply(const USInt& other);

			// Division
			USInt& Divide(const USInt& other);

			/** 
				Getters & Setters
			*/

			// Sets the value of the unsigned short int.
			void Set(const unsigned short int& value);
			// Convers the USInt to unsigned short int and returns it.
			unsigned short int Get() const;

			/**
				Operator Overloads
			*/

			// Add-assign operator
			USInt& operator+=(const USInt& other);
			USInt& operator+=(const unsigned short int& other);

			// Subtract-assign operator
			USInt& operator-=(const USInt& other);
			USInt& operator-=(const unsigned short int& other);

			// Multiply-assign operator
			USInt& operator*=(const USInt& other);
			USInt& operator*=(const unsigned short int& other);

			// Divide-assign operator
			USInt& operator/=(const USInt& other);
			USInt& operator/=(const unsigned short int& other);

			// Increment operator
			USInt& operator++(int other);
			USInt& operator++();

			// Decrement operator
			USInt& operator--(int other);
			USInt& operator--();

			// Conversion operator to unsigned short int.
			operator unsigned short int() const;

		};

	}
}