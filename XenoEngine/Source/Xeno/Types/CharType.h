#pragma once

#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Types
	{

		class ENGINEAPI Char
		{
		private:
			// The value of the char.
			char m_value;
		public:

			/**
				Constructors & Deconstructors
			*/

			// Default constructor, sets the inital value to 0.
			Char();

			// Copy constructors.
			Char(const Char& other);
			Char(const char& other);

			// Destructor.
			~Char();

			/**
				Getters & Setters
			*/

			// Sets the value of the char.
			void Set(const char& value);
			// Converts the Char to char and returns it.
			char Get() const;

			// Conversion operator to char.
			operator char() const;
		};

	}
}