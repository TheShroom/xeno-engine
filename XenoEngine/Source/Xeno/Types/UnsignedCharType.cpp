#include "UnsignedCharType.h"
#include <Xeno/Memory/MemoryTracker.h>

namespace Xeno
{
	namespace Types
	{

		/** 
			Constructors
		*/

		UChar::UChar()
		{
			Memory::MemoryTracker::AddMemoryTrack(sizeof(UChar));
		}

		UChar::UChar(const UChar& other)
		{
			Set(other);
		}

		UChar::UChar(const unsigned char& other)
		{
			Set(other);
		}

		UChar::~UChar()
		{
			Memory::MemoryTracker::RemoveMemoryTrack(sizeof(UChar));
		}

		/**
			Mathematical Methods
		*/

		UChar& UChar::Add(const UChar& other)
		{
			m_value += other;
			return *this;
		}

		UChar& UChar::Subtract(const UChar& other)
		{
			m_value -= other;
			return *this;
		}

		UChar& UChar::Multiply(const UChar& other)
		{
			m_value *= other;
			return *this;
		}

		UChar& UChar::Divide(const UChar& other)
		{
			m_value /= other;
			return *this;
		}

		/**
			Getters & Setters
		*/

		void UChar::Set(const unsigned char& other)
		{
			m_value = other;
		}

		unsigned char UChar::Get() const
		{
			return m_value;
		}

		/**
			Operator Overloads
		*/

		UChar& UChar::operator+=(const UChar& other)
		{
			return Add(other);
		}

		UChar& UChar::operator+=(const unsigned char& other)
		{
			return Add(other);
		}

		UChar& UChar::operator-=(const UChar& other)
		{
			return Subtract(other);
		}

		UChar& UChar::operator-=(const unsigned char& other)
		{
			return Subtract(other);
		}

		UChar& UChar::operator*=(const UChar& other)
		{
			return Multiply(other);
		}

		UChar& UChar::operator*=(const unsigned char& other)
		{
			return Multiply(other);
		}

		UChar& UChar::operator/=(const UChar& other)
		{
			return Divide(other);
		}

		UChar& UChar::operator/=(const unsigned char& other)
		{
			return Divide(other);
		}

		UChar::operator unsigned char() const
		{
			return Get();
		}

	}
}