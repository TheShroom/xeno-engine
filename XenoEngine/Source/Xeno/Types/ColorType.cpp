#include "ColorType.h"

namespace Xeno
{
	namespace Types
	{

		const Color Color::white = Color(255, 255, 255, 0);
		const Color Color::black = Color(  0,   0,   0, 0);
		const Color Color::grey	 = Color(155, 155, 155, 0);
		const Color Color::red   = Color(255,   0,   0, 0);
		const Color Color::blue  = Color(  0, 255,   0, 0);
		const Color Color::green = Color(  0,   0, 255, 0);

		Color::Color() :
			r(0), g(0), b(0), a(0)
		{

		}

		Color::Color(float r, float g, float b, float a) :
			r(r), g(g), b(b), a(a)
		{

		}

		Color::Color(const Color& other) :
			r(other.r), g(other.g), b(other.b), a(other.a)
		{

		}

		Color Color::Normalize()
		{
			return *this = ToNormalized(*this);
		}

		Color Color::ToNormalized(Color color)
		{
			return Color(Math::ToNormalized(color.r, 0, 255), Math::ToNormalized(color.g, 0, 255), Math::ToNormalized(color.b, 0, 255), Math::ToNormalized(color.a, 0, 255));
		}

		Color Color::FromNormalized(Color color)
		{
			return Color(Math::ToNormalized(color.r, 0, 255), Math::ToNormalized(color.g, 0, 255), Math::ToNormalized(color.b, 0, 255), Math::ToNormalized(color.a, 0, 255));
		}

	}
}