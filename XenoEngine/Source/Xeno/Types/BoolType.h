#pragma once

#include <Xeno/EngineDefines.h>

#define False Xeno::Types::Bool(false)
#define True Xeno::Types::Bool(true)

namespace Xeno
{
	namespace Types
	{

		class ENGINEAPI Bool
		{
		private:
			// The value of the bool.
			bool m_value;
		public:

			/**
				Constructors & Deconstructors
			*/

			// Default constructor, sets the inital value to 0.
			Bool();

			// Copy constructors.
			Bool(const Bool& other);
			Bool(const bool& other);

			// Destructor.
			~Bool();

			/**
				Getters & Setters
			*/

			// Sets the value of the bool.
			void Set(const bool& value);
			// Converts the Bool to bool and returns it.
			bool Get() const;

			// Conversion operator to bool.
			operator bool() const;
		};

	}
}