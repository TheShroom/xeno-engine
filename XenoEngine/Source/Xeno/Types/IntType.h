#pragma once

#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Types
	{

		class ENGINEAPI Int
		{
		private:
			// The value of the Int.
			int m_value;
		public:

			/**
				Constructors & Deconstructors
			*/

			// Default constructor, sets the inital value to 0.
			Int();

			// Copy constructors.
			Int(const Int& other);
			Int(const int& other);

			// Destructor.
			~Int();

			/** 
				Mathematical Methods
			*/

			// Addition.
			Int& Add(const Int& other);

			// Increment
			Int& Increment();

			// Decrement
			Int& Decrement();

			// Subtraction
			Int& Subtract(const Int& other);

			// Multiplication
			Int& Multiply(const Int& other);

			// Division
			Int& Divide(const Int& other);

			/**
				Getters & Setters
			*/

			// Sets the value of the int.
			void Set(const int& value);
			// Converts the Int to int and returns it.
			int Get() const;

			/**
				Operator Overloads
			*/

			// Add-assign operator.
			Int& operator+=(const Int& other);
			Int& operator+=(const int& other);

			// Subtract-assign operator.
			Int& operator-=(const Int& other);
			Int& operator-=(const int& other);

			// Multiply-assign operator.
			Int& operator*=(const Int& other);
			Int& operator*=(const int& other);

			// Divide-assign operator.
			Int& operator/=(const Int& other);
			Int& operator/=(const int& other);

			// Increment operator.
			Int& operator++(int other);
			Int& operator++();

			// Decrement operator.
			Int& operator--(int other);
			Int& operator--();

			// Conversion operator to int.
			operator int() const;
		};

	}
}