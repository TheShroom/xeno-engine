#include "LongIntType.h"
#include <Xeno/Memory/MemoryTracker.h>

namespace Xeno
{
	namespace Types
	{

		/** 
			Constructors
		*/

		LInt::LInt()
		{
			Memory::MemoryTracker::AddMemoryTrack(sizeof(LInt));
		}

		LInt::LInt(const LInt& other)
		{
			Set(other);
		}

		LInt::LInt(const long long int& other)
		{
			Set(other);
		}

		LInt::~LInt()
		{
			Memory::MemoryTracker::RemoveMemoryTrack(sizeof(LInt));
		}

		/**
			Mathematical Methods
		*/

		LInt& LInt::Add(const LInt& other)
		{
			m_value += other;
			return *this;
		}

		LInt& LInt::Subtract(const LInt& other)
		{
			m_value -= other;
			return *this;
		}

		LInt& LInt::Multiply(const LInt& other)
		{
			m_value *= other;
			return *this;
		}

		LInt& LInt::Divide(const LInt& other)
		{
			m_value /= other;
			return *this;
		}

		/**
			Getters & Setters
		*/

		void LInt::Set(const long long int& other)
		{
			m_value = other;
		}

		long long int LInt::Get() const
		{
			return m_value;
		}

		/**
			Operator Overloads
		*/

		LInt& LInt::operator+=(const LInt& other)
		{
			return Add(other);
		}

		LInt& LInt::operator+=(const long long int& other)
		{
			return Add(other);
		}

		LInt& LInt::operator-=(const LInt& other)
		{
			return Subtract(other);
		}

		LInt& LInt::operator-=(const long long int& other)
		{
			return Subtract(other);
		}

		LInt& LInt::operator*=(const LInt& other)
		{
			return Multiply(other);
		}

		LInt& LInt::operator*=(const long long int& other)
		{
			return Multiply(other);
		}

		LInt& LInt::operator/=(const LInt& other)
		{
			return Divide(other);
		}

		LInt& LInt::operator/=(const long long int& other)
		{
			return Divide(other);
		}

		LInt::operator long long int() const
		{
			return Get();
		}

	}
}