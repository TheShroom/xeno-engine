#include "DoubleType.h"
#include <Xeno/Memory/MemoryTracker.h>

namespace Xeno
{
	namespace Types
	{

		/**
			Constructors
		*/

		Double::Double()
		{
			Memory::MemoryTracker::AddMemoryTrack(sizeof(Double));
		}

		Double::Double(const Double& other)
		{
			Set(other);
		}

		Double::Double(const double& other)
		{
			Set(other);
		}

		Double::~Double()
		{
			Memory::MemoryTracker::RemoveMemoryTrack(sizeof(Double));
		}

		/**
			Mathematical Methods
		*/

		Double& Double::Add(const Double& other)
		{
			m_value += other;
			return *this;
		}

		Double& Double::Subtract(const Double& other)
		{
			m_value -= other;
			return *this;
		}

		Double& Double::Multiply(const Double& other)
		{
			m_value *= other;
			return *this;
		}

		Double& Double::Divide(const Double& other)
		{
			m_value /= other;
			return *this;
		}

		/**
			Getters & Setters
		*/

		void Double::Set(const double& other)
		{
			m_value = other;
		}

		double Double::Get() const {
			return m_value;
		}

		/**
			Operator Overloads
		*/

		Double& Double::operator+=(const Double& other)
		{
			return Add(other);
		}

		Double& Double::operator+=(const double& other)
		{
			return Add(other);
		}

		Double& Double::operator-=(const Double& other)
		{
			return Subtract(other);
		}

		Double& Double::operator-=(const double& other)
		{
			return Subtract(other);
		}

		Double& Double::operator*=(const Double& other)
		{
			return Multiply(other);
		}

		Double& Double::operator*=(const double& other)
		{
			return Multiply(other);
		}

		Double& Double::operator/=(const Double& other)
		{
			return Divide(other);
		}

		Double& Double::operator/=(const double& other)
		{
			return Divide(other);
		}

		Double::operator double() const
		{
			return Get();
		}

	}
}