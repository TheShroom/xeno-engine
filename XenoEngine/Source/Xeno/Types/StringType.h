#pragma once

#include <Xeno/EngineDefines.h>
#include "UnsignedIntType.h"
#include "CharType.h"
#include "BoolType.h"
#include "UnsignedIntType.h"
#include "FloatType.h"
#include "DoubleType.h"
#include "ShortIntType.h"
#include "LongIntType.h"
#include "IntType.h"
#include "UnsignedShortIntType.h"
#include "UnsignedLongIntType.h"
#include <string>

namespace Xeno
{
	namespace Types
	{

		class ENGINEAPI String
		{
		private:
			// The value of the String. (Char dynamic pointer array)
			Types::Char* m_value;

			// The amount of characters in the string. Not including the null-terminator.
			Types::UInt count;
		public:

			/**
				Constructors & Deconstructors
			*/

			// Default constructor. Creates an uninitialized String.
			String();

			// Creates a new String with the specified count.
			String(UInt count);

			// Copy constructors.
			String(const String& other);
			String(const std::string& other);
			String(const char* other);

			// Deconstructor
			~String();

			/**
				Utility Methods
			*/

			// Returns a reference to the character located at the specified index.
			Types::Char& Get(UInt index);

			// Returns a copy of the character located at the specified index.
			Types::Char GetAt(UInt index) const;

			// Sets the character at the specified index.
			void SetAt(UInt index, Types::Char value);

			// Returns a new copy of the current string.
			Types::String Copy() const;

			// Makes the current String a copy of another String.
			void CopyFrom(const String& other);

			// Makes the current String a copy of another std-string.
			void CopyFrom(const std::string& other);

			// Makes the current String a copy of another c-style string.
			void CopyFrom(const char* other);

			// Returns the count of the string. Not including the null-terminator.
			Types::UInt Count() const;

			// Returns true if the String is empty, false if not. (A String with no characters, and only a null-terminator would still be considered unempty)
			Types::Bool Empty() const;

			// Frees the memory used up by the String.
			void Clear();

			// Concatenates the current String with another one.
			Types::String& Concatenate(const String& other);
			Types::String& Concatenate(Char other);
			Types::String& Concatenate(const std::string& other);
			Types::String& Concatenate(const char* other);

			static Types::String Concatenate(const String& first, const String& other);
			static Types::String Concatenate(const std::string& first, const std::string& second);

			// Compares two Strings
			Types::Bool IsEqual(const String& other);
			Types::Bool IsLess(const String& other);
			Types::Bool IsGreater(const String& other);

			// Returns a new String composed of the character between the startIndex and the endIndex of the current String.
			String SubString(UInt startIndex, UInt endIndex);

			// Returns a new string composed of the non-matching characters from the String.
			String RemoveMatchingString(const String& substring);

			// Removes the path from the String, leaving only the filename.
			void RemovePath();

			// Returns the index of the last occurence of the specified character.
			Types::Int FindLastOf(Types::Char character);

			// Returns a copy of the String as a c-style string.
			char* ToCString() const;

			// Returns a copy of the String as a std-style String.
			std::string ToStdString() const;

			/**
				Operator Overloads
			*/

			// Assign operators (default assign operator only copies the pointer, need to copy value)
			String& operator=(const String& other);
			String& operator=(const std::string& other);
			String& operator=(const char* other);

			// Comparison operators
			Bool operator==(const String& other);
			bool operator<(const String& other); // Used in order to be able to use the String class in std containers, such as std::map. (They use the <, > operators for sorting)
			bool operator>(const String& other); // Used in order to be able to use the String class in std containers, such as std::map.

			/**
				Array-index operators

				The non-const will ALWAYS return a reference.
				If copying is intended, use GetAt() instead.
				GetAt() returns a copy instead of a reference.
			*/

			Types::Char& operator[](UInt index);
			Types::Char operator[](UInt index) const;

			// Add-assign operator / concatenation operators
			String& operator+=(const String& other);
			String& operator+=(Char other);
			String& operator+=(const std::string& other);
			String& operator+=(const char* other);

			String& operator+(const String& other);
			String& operator+(const std::string& other);
			String& operator+(const char* other);
			String& operator+(const Types::Char other);

			explicit operator const char*();

			/**
				Static Utility Functions
			*/

			// Copies a C-style string and returns it as a Xeno String.
			static Types::String CopyFromCStr(const char* source);

			// Copes a std-style string and returns it as a Xeno String.
			static Types::String CopyFromStdStr(const std::string& source);

			// Returns the count of a c-style string, not including the null-terminator.
			static UInt CStrCount(const char* source);

			// Returns the count of a std-style string, not including the null-terminator.
			static UInt StdStrCount(const std::string& source);

			// Parses an int and returns it as a String.
			static Types::String ParseInt(Types::Int value);
			// Parses an unsigned int and returns it as a String.
			static Types::String ParseUInt(Types::UInt value);
			// Parses a long int and returns it as a String.
			static Types::String ParseLInt(Types::LInt value);
			// Parses a short int and returns it as a String.
			static Types::String ParseSInt(Types::SInt value);
			// Parses an unsigned long int and returns it as a String.
			static Types::String ParseULInt(Types::ULInt value);
			// Parses an unsigned short int and returns it as a String.
			static Types::String ParseUSInt(Types::USInt value);
			// Parses a float and returns it as a String.
			static Types::String ParseFloat(Types::Float value);
			// Parses a double and returns it as a String.
			static Types::String ParseDouble(Types::Double value);
			// Parses a bool and returns it as a String.
			static Types::String ParseBool(Types::Bool value);

		};
		
		// Todo: Move definition to .cpp?
		static Types::String operator+(const char* cstr, const Xeno::Types::String& str)
		{
			Xeno::Types::String newString(cstr);
			newString.Concatenate(str);
			return newString;
		}
	}
}

// bool operator<(const Xeno::Types::String& left, const Xeno::Types::String& right)
// {
// 	return left < right;
// }
// 
// bool operator>(const Xeno::Types::String& left, const Xeno::Types::String& right)
// {
// 	return left > right;
// }