#include "StringType.h"
#include <Xeno/Logging/Logger.h>
#include <iostream>
#include <sstream>

namespace Xeno
{
	namespace Types
	{

		String::String()
		{
			m_value = nullptr;
		}

		String::String(UInt count)
		{
			m_value = new Types::Char[count + 1];
			m_value[count] = '\0'; // Add null-terminator to end.
			this->count = count;
		}

		String::String(const String& other)
		{
			CopyFrom(other);
		}

		String::String(const std::string& other)
		{
			CopyFrom(other);
		}

		String::String(const char* other)
		{
			CopyFrom(other);
		}

		String::~String()
		{
			Clear();
		}

		Types::Char& String::Get(UInt index)
		{
			return m_value[index];
		}

		Types::Char String::GetAt(UInt index) const
		{
			return m_value[index];
		}

		void String::SetAt(UInt index, Types::Char value)
		{
			m_value[index] = value;
		}

		Types::UInt String::Count() const
		{
			return count;
		}

		Types::String String::Copy() const
		{
			String copy(count);

			// Copy every character including null-terminator.
			for (UInt i = 0; i < Count() + 1; i++)
			{
				copy.SetAt(i, m_value[i]);
			}

			return copy;
		}

		void String::CopyFrom(const String& other)
		{
			Clear(); // Clear in case it is occupied.
			m_value = new Types::Char[other.Count() + 1];
			count = other.Count();

			for (UInt i = 0; i < Count() + 1; i++)
			{
				SetAt(i, other[i]); // The copy should have a null-terminator.
			}
		}

		void String::CopyFrom(const std::string& other)
		{
			Clear(); // Clear in case it is occupied.
			m_value = new Types::Char[StdStrCount(other) + 1];
			count = StdStrCount(other);

			for (UInt i = 0; i < Count() + 1; i++)
			{
				SetAt(i, other[i]);
			}
		}

		void String::CopyFrom(const char* other)
		{
			Clear(); // Clear in case it is occupied.
			m_value = new Types::Char[CStrCount(other) + 1];
			count = CStrCount(other);

			for (UInt i = 0; i < Count() + 1; i++)
			{
				SetAt(i, other[i]);
			}
		}

		Types::Bool String::Empty() const
		{
			if (m_value == nullptr || m_value == (Types::Char*)0xCCCCCCCC)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		void String::Clear()
		{
			// Only clear if it is needed.
			if (!Empty())
			{
				delete[] m_value;
				m_value = nullptr;
				count = 0;
			}
		}

		String& String::Concatenate(const String& other)
		{
			String old = *this;

			UInt combinedCount = old.Count() + other.Count();
			Clear(); // Remove old content.

			m_value = new Types::Char[combinedCount + 1];
			for (UInt i = 0; i < old.Count(); i++)
			{
				m_value[i] = old[i];
			}

			UInt otherIndex = 0;
			for (UInt i = old.Count(); i < combinedCount; i++)
			{
				m_value[i] = other[otherIndex];
				otherIndex++;
			}

			count = combinedCount;

			return *this;
		}

		String& String::Concatenate(Char other)
		{
			String old = *this;

			UInt combinedCount = old.Count() + 1;
			Clear(); // Remove old content.

			m_value = new Types::Char[combinedCount + 1];
			for (UInt i = 0; i < old.Count(); i++)
			{
				m_value[i] = old[i];
			}

			UInt otherIndex = 0;
			for (UInt i = old.Count(); i < combinedCount; i++)
			{
				m_value[i] = other;
				otherIndex++;
			}

			count = combinedCount;

			return *this;
		}

		String& String::Concatenate(const std::string& other)
		{
			String old = *this;

			UInt combinedCount = old.Count() + StdStrCount(other);
			Clear(); // Remove old content.

			m_value = new Types::Char[combinedCount + 1];
			for (UInt i = 0; i < old.Count(); i++)
			{
				m_value[i] = old[i];
			}

			UInt otherIndex = 0;
			for (UInt i = old.Count(); i < combinedCount; i++)
			{
				m_value[i] = other[otherIndex];
				otherIndex++;
			}

			count = combinedCount;

			m_value[Count()] = '\0'; // Add null-terminator.

			return *this;
		}

		String& String::Concatenate(const char* other)
		{
			String old = *this;

			UInt combinedCount = old.Count() + CStrCount(other);
			Clear(); // Remove old content.

			m_value = new Types::Char[combinedCount + 1];
			for (UInt i = 0; i < old.Count(); i++)
			{
				m_value[i] = old[i];
			}

			UInt otherIndex = 0;
			for (UInt i = old.Count(); i < combinedCount; i++)
			{
				m_value[i] = other[otherIndex];
				otherIndex++;
			}

			count = combinedCount;

			m_value[Count()] = '\0'; // Add null-terminator.

			return *this;
		}

		String String::Concatenate(const String& first, const String& second)
		{
			UInt combinedCount = first.Count() + second.Count();
			String concatenated(combinedCount);

			for (UInt i = 0; i < first.Count(); i++)
			{
				concatenated[i] = first[i];
			}

			UInt otherIndex = 0;
			for (UInt i = first.Count(); i < combinedCount; i++)
			{
				concatenated[i] = second[otherIndex];
				otherIndex++;
			}

			return concatenated;
		}

		String String::Concatenate(const std::string& first, const std::string& second)
		{
			UInt combinedCount = first.length() + second.length();
			String concatenated(combinedCount);

			for (UInt i = 0; i < first.length(); i++)
			{
				concatenated[i] = first[i];
			}

			UInt otherIndex = 0;
			for (UInt i = first.length(); i < combinedCount; i++)
			{
				concatenated[i] = second[otherIndex];
				otherIndex++;
			}

			return concatenated;
		}

		Bool String::IsEqual(const String& other)
		{
			for (Int i = 0; i < other.Count(); i++)
			{
				// If one character doesn't match we can just return False since it isn't equal.
				if (Get((UInt)i) != other[(UInt)i])
					return False;
			}

			// If code reaches here, no non-matching characters were found so the Strings are equal.
			return True;
		}

		Bool String::IsLess(const String& other)
		{
			return Get(0) < other[0];
		}

		Bool String::IsGreater(const String& other)
		{
			return Get(0) > other[0];
		}

		String String::SubString(UInt startIndex, UInt endIndex)
		{
			String substr((endIndex - startIndex) + 1); // Add 1 to convert into count. from index endIndex to startIndex is endIndex - startIndex + 1 characters. (Not counting the null-terminator)
			UInt substrIndex = 0;
			// Seems to add two null-terminators at end?
			for (UInt i = startIndex; i < endIndex + 1; i++) // Add 1 to convert into count. Same as above. ^
			{
				substr[substrIndex] = m_value[i];
				substrIndex++;
			}

			// Add 1 to make space for null-terminator, since it isn't added to the count of the String. (Still accessible, though)
			// substr[substr.Count() + 1] = '\0'; // Add null-terminator.
			return substr;
		}

		String String::RemoveMatchingString(const String& substring)
		{
			Int matchedCount = 0;
			Int matchedIndex;
			bool isFirstMatch = true;
			for (Int i = 0; i < Count(); i++)
			{
				if (m_value[i] == substring[(UInt)matchedCount])
				{
					if (isFirstMatch) matchedIndex = i;
					matchedCount++;
					isFirstMatch = false;
				}
				else
				{
					matchedIndex = -1;
					matchedCount = 0;
					isFirstMatch = true;
				}
			}

			if (matchedCount != substring.Count())
			{
				return *this; // No matching substring could be found in target, return original.
			}

			String newString = String(Count() - matchedCount);
			for (unsigned int i = 0; i < newString.Count(); i++) // removed -1 (seems to have fixed it)
			{
				newString[i] = GetAt(i);
			}

			return newString;
		}

		void String::RemovePath()
		{
			Int lastof = FindLastOf('\\');
			if (lastof == -1)
			{
				lastof = FindLastOf('/');
				if (lastof == -1)
				{
					return; // No path found, do nothing.
				}
			}

			String toBeRemoved(Count() - lastof);
			UInt otherIndex = 0;
			for (UInt i = (UInt)lastof; i++;)
			{
				toBeRemoved[otherIndex] = m_value[i];
			}

			*this = toBeRemoved;
		}

		Types::Int String::FindLastOf(Types::Char character)
		{
			UInt matchedCount = 0;
			UInt matchedIndex;
			bool isFirstMatch = true;
			for (UInt i = 0; i < Count(); i++)
			{
				if (m_value[i] == character)
				{
					if (isFirstMatch) matchedIndex = i;
					matchedCount++;
					isFirstMatch = false;
				}
				else
				{
					matchedIndex = -1;
					matchedCount = 0;
					isFirstMatch = true;
				}
			}

			if (matchedCount == 0)
			{
				return -1; // No matching character found, return negative.
			}
		}

		// Todo: Check if std::string is null-terminated in memory by using the Memory Debug View.
		char* String::ToCString() const
		{
			// std::string::c_str
			return (char*)&m_value[0];
		}

		std::string String::ToStdString() const
		{
			std::string copy;
			for (UInt i = 0; i < Count(); i++)
			{
				copy += m_value[i];
			}

			return copy;
		}

		Types::String& String::operator=(const String& other)
		{
			CopyFrom(other);
			return *this;
		}

		Types::String& String::operator=(const std::string& other)
		{
			CopyFrom(other);
			return *this;
		}

		Types::String& String::operator=(const char* other)
		{
			CopyFrom(other);
			return *this;
		}

		Types::Bool String::operator==(const String& other)
		{
			return IsEqual(other);
		}

		bool String::operator<(const String& other)
		{
			return IsLess(other);
		}

		bool String::operator>(const String& other)
		{
			return IsGreater(other);
		}

		Types::Char& String::operator[](UInt index)
		{
			return Get(index);
		}

		Types::Char String::operator[](UInt index) const
		{
			return GetAt(index);
		}

		Types::String& String::operator+=(const String& other)
		{
			return Concatenate(other);
		}

		Types::String& String::operator+=(Char other)
		{
			return Concatenate(other);
		}

		Types::String& String::operator+=(const std::string& other)
		{
			return Concatenate(other);
		}

		Types::String& String::operator+=(const char* other)
		{
			return Concatenate(other);
		}

		String& String::operator+(const String& other)
		{
			return Concatenate(other);
		}

		String& String::operator+(const std::string& other)
		{
			return Concatenate(other);
		}

		String& String::operator+(const char* other)
		{
			return Concatenate(other);
		}
		
		String& String::operator+(const Types::Char other)
		{
			return Concatenate(other);
		}

		String::operator const char*()
		{
			return ToCString();
		}

		Types::String String::CopyFromCStr(const char* source)
		{
			String copy = String(CStrCount(source));

			// Copy every character of the string, including null-terminator.
			for (UInt i = 0; i < copy.Count() + 1; i++)
			{
				copy[i] = source[i];
			}

			return copy;
		}

		Types::String String::CopyFromStdStr(const std::string& source)
		{
			String copy = String(StdStrCount(source));

			// Copy ever character of the string.
			for (UInt i = 0; i < copy.Count() + 1; i++)
			{
				copy[i] = source[i];
			}

			return copy;
		}

		Types::UInt String::CStrCount(const char* source)
		{
			return strlen(source);
		}

		Types::UInt String::StdStrCount(const std::string& source)
		{
			return source.length();
		}

		Types::String String::ParseInt(Types::Int value)
		{
			return std::to_string(value); // Todo: Make custom parsing?
		}

		Types::String String::ParseUInt(Types::UInt value)
		{
			return std::to_string(value);
		}

		Types::String String::ParseLInt(Types::LInt value)
		{
			return std::to_string(value);
		}

		Types::String String::ParseSInt(Types::SInt value)
		{
			return std::to_string(value);
		}

		Types::String String::ParseULInt(Types::ULInt value)
		{
			return std::to_string(value);
		}

		Types::String String::ParseUSInt(Types::USInt value)
		{
			return std::to_string(value);
		}

		Types::String String::ParseFloat(Types::Float value)
		{
			return std::to_string(value);
		}

		Types::String String::ParseDouble(Types::Double value)
		{
			return std::to_string(value);
		}

		Types::String String::ParseBool(Types::Bool value)
		{
			return std::to_string(value);
		}

	}

}