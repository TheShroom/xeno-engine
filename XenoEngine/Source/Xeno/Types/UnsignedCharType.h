#pragma once

#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Types
	{
		
		class ENGINEAPI UChar
		{
		private:
			// The value of the UChar.
			unsigned char m_value;
		public:

			/**
				Constructors & Deconstructors
			*/

			// Default constructor, sets the initial value to 0.
			UChar();

			// Copy constructors.
			UChar(const UChar& other);
			UChar(const unsigned char& other);

			// Destructor
			~UChar();

			/** 
				Mathematical Methods
			*/

			// Addition
			UChar& Add(const UChar& other);

			// Subtraction
			UChar& Subtract(const UChar& other);

			// Multiplication
			UChar& Multiply(const UChar& other);

			// Division
			UChar& Divide(const UChar& other);

			/** 
				Getters & Setters
			*/

			// Sets the value of the unsigned char.
			void Set(const unsigned char& value);
			// Converts the UChar to unsigned char and returns it.
			unsigned char Get() const;

			/**
				Operator Overloads
			*/

			// Add-assign operator.
			UChar& operator+=(const UChar& other);
			UChar& operator+=(const unsigned char& other);

			// Subtract-assign operator.
			UChar& operator-=(const UChar& other);
			UChar& operator-=(const unsigned char& other);

			// Multiply-assign operator.
			UChar& operator*=(const UChar& other);
			UChar& operator*=(const unsigned char& other);

			// Divide-assign operator.
			UChar& operator/=(const UChar& other);
			UChar& operator/=(const unsigned char& other);

			// Conversion operator to unsigned char.
			operator unsigned char() const;

		};

	}
}