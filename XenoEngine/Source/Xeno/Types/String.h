#pragma once
#include <Xeno/EngineIncludes.h>
#include <string>

namespace Xeno
{
	namespace Types
	{

		class ENGINEAPI StringHelper
		{
		private:
		protected:
		public:
			static std::string RemovePath(const std::string& str);
			static std::string RemoveMatchingString(const std::string& str, const std::string& substring);
		};

	}
}