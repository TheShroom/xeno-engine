#include "IntType.h"
#include <Xeno/Memory/MemoryTracker.h>

namespace Xeno
{
	namespace Types
	{

		/**
			Constructors
		*/

		Int::Int()
		{
			Memory::MemoryTracker::AddMemoryTrack(sizeof(Int));
		}

		Int::Int(const Int& other)
		{
			Set(other);
		}

		Int::Int(const int& other)
		{
			Set(other);
		}

		Int::~Int()
		{
			Memory::MemoryTracker::RemoveMemoryTrack(sizeof(Int));
		}

		/**
			Mathematical Methods
		*/

		Int& Int::Add(const Int& other)
		{
			m_value += other;
			return *this;
		}


		Int& Int::Increment()
		{
			m_value++;
			return *this;
		}

		Int& Int::Decrement()
		{
			m_value--;
			return *this;
		}

		Int& Int::Subtract(const Int& other)
		{
			m_value -= other;
			return *this;
		}

		Int& Int::Multiply(const Int& other)
		{
			m_value *= other;
			return *this;
		}

		Int& Int::Divide(const Int& other)
		{
			m_value /= other;
			return *this;
		}

		/**
			Getters & Setters
		*/

		void Int::Set(const int& other)
		{
			m_value = other;
		}

		int Int::Get() const 
		{
			return m_value;
		}

		/**
			Operator Overloads
		*/

		Int& Int::operator+=(const Int& other)
		{
			return Add(other);
		}

		Int& Int::operator+=(const int& other)
		{
			return Add(other);
		}

		Int& Int::operator-=(const Int& other)
		{
			return Subtract(other);
		}

		Int& Int::operator-=(const int& other)
		{
			return Subtract(other);
		}

		Int& Int::operator*=(const Int& other)
		{
			return Multiply(other);
		}

		Int& Int::operator*=(const int& other)
		{
			return Multiply(other);
		}

		Int& Int::operator/=(const Int& other)
		{
			return Divide(other);
		}

		Int& Int::operator/=(const int& other)
		{
			return Divide(other);
		}


		Int& Int::operator++(int other)
		{
			return Increment();
		}

		Int& Int::operator++()
		{
			return Increment();
		}

		Int& Int::operator--(int other)
		{
			return Decrement();
		}

		Int& Int::operator--()
		{
			return Decrement();
		}

		Int::operator int() const
		{
			return Get();
		}

	}
}