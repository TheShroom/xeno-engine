#pragma once
#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Types
	{
		
		class ENGINEAPI SInt
		{
		private:
			// The value of the SInt.
			short int m_value;
		public:

			/**
				Constructors & Destructors
			*/

			// Default constructor, sets the initial value to 0.
			SInt();

			// Copy constructors.
			SInt(const SInt& other);
			SInt(const short int& other);

			// Destructor.
			~SInt();

			/**
				Mathematical Methods
			*/

			// Addition.
			SInt& Add(const SInt& other);

			// Subtraction.
			SInt& Subtract(const SInt& other);

			// Multiplication.
			SInt& Multiply(const SInt& other);

			// Division.
			SInt& Divide(const SInt& other);

			/**
				Getters & Setters
			*/

			// Sets the value of the short int.
			void Set(const short int& value);
			// Convers the SInt to short int and returns it.
			short int Get() const;

			/**
				Operator Overloads
			*/

			// Add-assign operator.
			SInt& operator+=(const SInt& other);
			SInt& operator+=(const short int& other);

			// Subtract-assign operator.
			SInt& operator-=(const SInt& other);
			SInt& operator-=(const short int& other);

			// Multiply-assign operator.
			SInt& operator*=(const SInt& other);
			SInt& operator*=(const short int& other);

			// Divide-assign operator.
			SInt& operator/=(const SInt& other);
			SInt& operator/=(const short int& other);

			// Converstion operator to short int.
			operator short int() const;
		};

	}
}