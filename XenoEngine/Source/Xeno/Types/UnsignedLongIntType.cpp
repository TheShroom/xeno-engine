#include "UnsignedLongIntType.h"
#include <Xeno/Memory/MemoryTracker.h>

namespace Xeno
{
	namespace Types
	{

		/** 
			Constructors
		*/

		ULInt::ULInt()
		{
			Memory::MemoryTracker::AddMemoryTrack(sizeof(ULInt));
		}

		ULInt::ULInt(const ULInt& other)
		{
			Set(other);
		}

		ULInt::ULInt(const unsigned long long int& other)
		{
			Set(other);
		}

		ULInt::~ULInt()
		{
			Memory::MemoryTracker::RemoveMemoryTrack(sizeof(ULInt));
		}

		/**
			Mathematical Methods
		*/

		ULInt& ULInt::Add(const ULInt& other)
		{
			m_value += other;
			return *this;
		}

		ULInt& ULInt::Subtract(const ULInt& other)
		{
			m_value -= other;
			return *this;
		}

		ULInt& ULInt::Multiply(const ULInt& other)
		{
			m_value *= other;
			return *this;
		}

		ULInt& ULInt::Divide(const ULInt& other)
		{
			m_value /= other;
			return *this;
		}

		/**
			Getters & Setters
		*/

		void ULInt::Set(const unsigned long long int& other)
		{
			m_value = other;
		}

		unsigned long long int ULInt::Get() const
		{
			return m_value;
		}

		/**
			Operator Overloads
		*/

		ULInt& ULInt::operator+=(const ULInt& other)
		{
			return Add(other);
		}

		ULInt& ULInt::operator+=(const unsigned long long int& other)
		{
			return Add(other);
		}

		ULInt& ULInt::operator-=(const ULInt& other)
		{
			return Subtract(other);
		}

		ULInt& ULInt::operator-=(const unsigned long long int& other)
		{
			return Subtract(other);
		}

		ULInt& ULInt::operator*=(const ULInt& other)
		{
			return Multiply(other);
		}

		ULInt& ULInt::operator*=(const unsigned long long int& other)
		{
			return Multiply(other);
		}

		ULInt& ULInt::operator/=(const ULInt& other)
		{
			return Divide(other);
		}

		ULInt& ULInt::operator/=(const unsigned long long int& other)
		{
			return Divide(other);
		}

		ULInt::operator unsigned long long() const
		{
			return Get();
		}

	}
}