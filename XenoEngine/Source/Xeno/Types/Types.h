#pragma once

// Forward declare namespace.
namespace Xeno { namespace Types {  } }

// Remove this line if you wish to use a non-Xeno type wrapper to prevent from namespace invasion.
#define USE_CUSTOM_TYPE_WRAPPER_NAMESPACE

#ifdef USE_CUSTOM_TYPE_WRAPPER_NAMESPACE
using namespace Xeno::Types;
#endif

/**
	Include all basic custom types
*/

// Ints
#include "IntType.h"
#include "UnsignedIntType.h"
#include "ShortIntType.h"
#include "LongIntType.h"
#include "UnsignedShortIntType.h"
#include "UnsignedLongIntType.h"

// Floats
#include "FloatType.h"

// Doubles
#include "DoubleType.h"

// Bool
#include "BoolType.h"

// Chars
#include "CharType.h"
#include "UnsignedCharType.h"

// String
#include "StringType.h"

// Color
#include "ColorType.h"

/**
	Typedef external types to make them easier to use
*/

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

namespace Xeno
{
	namespace Types
	{
		// Vectors
		typedef glm::vec2 Vector2;
		typedef glm::vec3 Vector3;
		typedef glm::vec4 Vector4;

		// Quaternion
		typedef glm::quat Quaternion;

		// Matrices
		typedef glm::mat2x2 Matrix2x2;
		typedef glm::mat2x3 Matrix2x3;
		typedef glm::mat3x2 Matrix3x2;
		typedef glm::mat3x3 Matrix3x3;
		typedef glm::mat4x3 Matrix4x3;
		typedef glm::mat3x4 Matrix3x4;
		typedef glm::mat4x4 Matrix4x4;
	}
}