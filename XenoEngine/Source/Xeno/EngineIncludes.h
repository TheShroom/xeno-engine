#pragma once

// Includes all basic files that every single file should have access to.
// You don't need to include this in every file, but it does simplify things.

#include "EngineDefines.h"
#include "Debug/XenoDebug.h"
#include "Logging/Logger.h"
#include "Error Handling/ExceptionHandler.h"
#include "Error Handling/XenoException.h"
// #include "Types/Types.h"