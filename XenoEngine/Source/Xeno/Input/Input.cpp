#include "Input.h"
#include <iostream>

namespace Xeno
{

	std::unordered_map<int, bool> Input::m_keys;
	std::unordered_map<int, bool> Input::m_keysLastFrame;
	std::unordered_map<int, bool> Input::m_mouseButtons;
	std::unordered_map<int, bool> Input::m_mouseButtonsLastFrame;

	Input& Input::GetInstance()
	{
		static Input instance;
		return instance;
	}

	void Input::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		if (action == GLFW_PRESS)
		{
			m_keys[key] = true;
		}
		else if (action == GLFW_RELEASE)
		{
			m_keys[key] = false;
		}
	}

	void Input::MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
	{
		if (action == GLFW_PRESS)
		{
			m_mouseButtons[button] = true;
		}
		else if (action == GLFW_RELEASE)
		{
			m_mouseButtons[button] = false;
		}
	}

	void Input::ScrollCallback(GLFWwindow* window, double xOffset, double yOffset)
	{
		Input& i = GetInstance();
		i.m_scrollAmount.x = xOffset;
		i.m_scrollAmount.y = yOffset;
	}

	Input::Input()
	{

	}

	Input::~Input()
	{

	}

	void Input::Initialize()
	{
		XENO_LOG("Initializing input module.")
		m_window = glfwGetCurrentContext();
		if (m_window == nullptr)
		{
			XENO_THROW_EXCEPTION("Failed to get the current context")
		}
		glfwSetKeyCallback(m_window, KeyCallback);
		glfwSetMouseButtonCallback(m_window, MouseButtonCallback);
		glfwSetScrollCallback(m_window, ScrollCallback);

		XENO_LOG("Input module initialized")
	}
	
	void Input::Poll()
	{
		m_scrollAmount = Types::Vector2(0.0f, 0.0f);
		glfwPollEvents();
	}

	void Input::LatePoll()
	{
		m_keysLastFrame = m_keys;
		m_mouseButtonsLastFrame = m_mouseButtons;
	}

	void Input::SetKeyBind(const std::string& action, int key)
	{
		GetInstance().m_keyBinds.at(action) = key;
	}

	void Input::SetKeyBind(int oldKey, int newKey)
	{
		for (std::pair<std::string, int> element : GetInstance().m_keyBinds)
		{
			if (element.second == oldKey)
			{
				element.second = newKey; // Keep iterating through the next ones with the same keybind as well.
			}
		}
	}

	bool Input::IsKeyDown(int key)
	{
		return (m_keys[key] && !m_keysLastFrame[key]);
	}

	bool Input::IsKeyUp(int key)
	{
		return (!m_keys[key] && m_keysLastFrame[key]);
	}

	bool Input::IsKeyHeld(int key)
	{
		return m_keys[key];
	}

	bool Input::IsMouseButtonDown(int button)
	{
		return (m_mouseButtons[button] && !m_mouseButtonsLastFrame[button]);
	}

	bool Input::IsMouseButtonUp(int button)
	{
		return (!m_mouseButtons[button] && m_mouseButtonsLastFrame[button]);
	}

	bool Input::IsMouseButtonHeld(int button)
	{
		return m_mouseButtons[button];
	}

	bool Input::IsActionDown(const std::string& action)
	{
		int keybind = GetInstance().m_keyBinds[action];
		return IsKeyDown(keybind);
	}

	bool Input::IsActionUp(const std::string& action)
	{
		int keybind = GetInstance().m_keyBinds[action];
		return IsKeyUp(keybind);
	}

	bool Input::IsActionHeld(const std::string& action)
	{
		int keybind = GetInstance().m_keyBinds[action];
		return IsKeyHeld(keybind);
	}

	Types::Vector2 Input::GetScrollAmount()
	{
		return GetInstance().m_scrollAmount;
	}

}