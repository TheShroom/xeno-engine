#include "Cursor.h"

namespace Xeno
{

	void Cursor::CursorPositionCallback(GLFWwindow* window, double xPos, double yPos)
	{
		Cursor& c = GetInstance();
		c.m_cursorPosition.x = xPos;
		c.m_cursorPosition.y = yPos;
	}

	Cursor::Cursor()
	{

	}

	Cursor& Cursor::GetInstance()
	{
		static Cursor instance;
		return instance;
	}

	void Cursor::Initialize()
	{
		m_window = glfwGetCurrentContext();
		if (m_window == nullptr)
		{
			XENO_THROW_EXCEPTION("Failed to get the current context")
		}

		glfwSetCursorPosCallback(m_window, CursorPositionCallback);
	}

	void Cursor::SetMode(CursorMode mode)
	{
		glfwSetInputMode(GetInstance().m_window, GLFW_CURSOR, (int)mode);
	}

	Cursor::CursorMode Cursor::GetMode()
	{
		return (CursorMode)glfwGetInputMode(GetInstance().m_window, GLFW_CURSOR);
	}

	Types::Vector2 Cursor::GetPosition()
	{
		return GetInstance().m_cursorPosition;
	}

}