#pragma once
#include <GLEW/GL/glew.h>
#include <GLFW/glfw3.h>
#include <Xeno/EngineIncludes.h>
#include <unordered_map>
#include <string>
#include <Xeno/Types/Types.h>

#define INIT_KEY(key) std::make_pair<int, bool>(key, false)

namespace Xeno
{

	class ENGINEAPI Input
	{
	private:
		GLFWwindow* m_window;
		std::unordered_map<std::string, int> m_keyBinds;
		static std::unordered_map<int, bool> m_keys;
		static std::unordered_map<int, bool> m_keysLastFrame;

		static std::unordered_map<int, bool> m_mouseButtons;
		static std::unordered_map<int, bool> m_mouseButtonsLastFrame;

		Types::Vector2 m_scrollAmount;

		static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
		static void ScrollCallback(GLFWwindow* window, double xOffset, double yOffset);

	protected:
	public:

		static Input& GetInstance();

		Input();
		~Input();

		void Initialize();

		void Poll();
		void LatePoll();

		static void SetKeyBind(const std::string& action, int key);
		static void SetKeyBind(int oldKey, int newKey);

		static bool IsKeyDown(int key);
		static bool IsKeyUp(int key);
		static bool IsKeyHeld(int key);

		static bool IsMouseButtonDown(int button);
		static bool IsMouseButtonUp(int button);
		static bool IsMouseButtonHeld(int button);

		static bool IsActionDown(const std::string& action);
		static bool IsActionUp(const std::string& action);
		static bool IsActionHeld(const std::string& action);

		static Types::Vector2 GetScrollAmount();
	};

}