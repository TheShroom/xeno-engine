#pragma once
#include <Xeno/EngineDefines.h>
#include <Xeno/Types/Types.h>
#include <GLFW/glfw3.h>

namespace Xeno
{
	class ENGINEAPI Cursor
	{
	private:
		static void CursorPositionCallback(GLFWwindow* window, double xPos, double yPos);

		Cursor();

		GLFWwindow* m_window;
		Types::Vector2 m_cursorPosition;

	protected:
	public:
		static Cursor& GetInstance();

		void Initialize();

		enum CursorMode
		{
			MOUSE_LOCKED = GLFW_CURSOR_DISABLED,
			MOUSE_HIDDEN = GLFW_CURSOR_HIDDEN,
			MOUSE_UNLOCKED = GLFW_CURSOR_NORMAL
		};

		static void SetMode(CursorMode mode);
		static CursorMode GetMode();

		static Types::Vector2 GetPosition();
	};
}