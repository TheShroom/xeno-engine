#pragma once

#include <string>

#include <nlohmann-json/json.hpp>

namespace Xeno
{
	class ISerializable
	{
		virtual nlohmann::json Serialize() = 0;
		virtual void Deserialize(nlohmann::json json) = 0;
	};
}