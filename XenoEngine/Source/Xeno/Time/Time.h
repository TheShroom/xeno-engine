#pragma once
#include <Xeno/EngineIncludes.h>

namespace Xeno
{

	class ENGINEAPI Time
	{
	private:
		friend class Engine;

		static float deltaTime;
		static float lastDeltaTime;
		static void Update();
		static void LateUpdate();
	protected:
	public:
		static float GetDeltaTime();
		static float GetElapsedTime();
		static struct tm GetLocalTime();
		static std::string GetLocalTimeAsString();
	};

}