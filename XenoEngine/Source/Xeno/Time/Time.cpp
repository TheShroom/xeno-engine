#include "Time.h"
#include <GLFW/glfw3.h>
#include <ctime>

namespace Xeno
{

	float Time::deltaTime = 0.0f;
	float Time::lastDeltaTime = 0.0f;

	float Time::GetDeltaTime()
	{
		return deltaTime;
	}

	void Time::Update()
	{
		lastDeltaTime = GetElapsedTime();
	}

	void Time::LateUpdate()
	{
		deltaTime = GetElapsedTime() - lastDeltaTime;
		lastDeltaTime = GetElapsedTime();
	}

	float Time::GetElapsedTime()
	{
		return (float)glfwGetTime();
	}

	struct tm Time::GetLocalTime()
	{
		time_t rawtime;
		struct tm timeinfo;
		time(&rawtime);
		localtime_s(&timeinfo, &rawtime);
		return timeinfo;
	}

	std::string Time::GetLocalTimeAsString()
	{
		char buffer[100];
		strftime(buffer, sizeof(buffer), "%H:%M:%S", &GetLocalTime());
		return std::string(buffer);
	}
}