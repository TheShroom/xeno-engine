#include "CubeMesh.h"

namespace Xeno
{
	namespace Graphics
	{
		CubeMesh::CubeMesh() :
			Mesh(
				std::vector<glm::vec3> {
					// front
					glm::vec3(-1.0, -1.0, 1.0),
					glm::vec3( 1.0, -1.0, 1.0),
					glm::vec3( 1.0, 1.0, 1.0),
					glm::vec3(-1.0, 1.0, 1.0),
					// back
					glm::vec3(-1.0, -1.0, -1.0),
					glm::vec3( 1.0, -1.0, -1.0),
					glm::vec3( 1.0, 1.0, -1.0),
					glm::vec3(-1.0, 1.0, -1.0)
				},

				std::vector<unsigned int> {
					// front
					0, 1, 2,
					2, 3, 0,
					// right
					1, 5, 6,
					6, 2, 1,
					// back
					7, 6, 5,
					5, 4, 7,
					// left
					4, 0, 3,
					3, 7, 4,
					// bottom
					4, 5, 1,
					1, 0, 4,
					// top
					3, 2, 6,
					6, 7, 3,
				},

				// These values are incorrect
				std::vector<float> {
					// Quad 1
					0.0f, 0.0f, // Bottom left of texture
					1.0f, 0.0f, // Bottom right
					1.0f, 1.0f, // Top right
					0.0f, 1.0f, // Top left

					// Quad 2
					0.0f, 0.0f, // Bottom left of texture
					1.0f, 0.0f, // Bottom right
					1.0f, 1.0f, // Top right
					0.0f, 1.0f, // Top left

					// Quad 3
					0.0f, 0.0f, // Bottom left of texture
					1.0f, 0.0f, // Bottom right
					1.0f, 1.0f, // Top right
					0.0f, 1.0f, // Top left

					// Quad 4
					0.0f, 0.0f, // Bottom left of texture
					1.0f, 0.0f, // Bottom right
					1.0f, 1.0f, // Top right
					0.0f, 1.0f, // Top left

					// Quad 5
					0.0f, 0.0f, // Bottom left of texture
					1.0f, 0.0f, // Bottom right
					1.0f, 1.0f, // Top right
					0.0f, 1.0f, // Top left

					// Quad 6
					0.0f, 0.0f, // Bottom left of texture
					1.0f, 0.0f, // Bottom right
					1.0f, 1.0f, // Top right
					0.0f, 1.0f, // Top left
				}

			)
		{
			
		}
	}
}