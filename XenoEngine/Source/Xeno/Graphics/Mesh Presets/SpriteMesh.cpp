#include "SpriteMesh.h"

namespace Xeno
{
	namespace Graphics
	{
		SpriteMesh::SpriteMesh() :
			Mesh(

				// Vertices
				std::vector<glm::vec3> {
					glm::vec3( 0.0,  0.0,  1.0),
					glm::vec3( 1.0,  0.0,  1.0),
					glm::vec3( 1.0,  1.0,  1.0),
					glm::vec3( 0.0,  1.0,  1.0),
				},

				// Indices
				std::vector<unsigned int> {
					0, 1, 2,
					2, 3, 0,
				},

				// UV
				std::vector<float> {
					0.0f, 1.0f,
					1.0f, 1.0f,
					1.0f, 0.0f,
					0.0f, 0.0f,
				}

			)
		{

		}
	}
}