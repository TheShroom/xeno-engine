#pragma once
#include <Xeno/EngineDefines.h>
#include <Xeno/Graphics/Mesh.h>

namespace Xeno
{
	namespace Graphics
	{
		class ENGINEAPI CubeMesh : public Mesh
		{
		private:
		protected:
		public:
			CubeMesh();
		};
	}
}