#include "Triangle.h"
#include <GLEW/GL/glew.h>
#include <Xeno/Debug/XenoGLDebug.h>
#include <glm/gtx/string_cast.hpp>
#include <Xeno/FileIO/ResourceLoader.h>

namespace Xeno
{
	namespace Temporary
	{

		Triangle::Triangle(Graphics::Camera* mainCamera)
		{
			m_attachedCamera = mainCamera;
			m_shader = FileIO::ResourceLoader::LoadShader("D:\\Projects\\C++\\XenoEngine\\Xeno\\XenoEngine\\Resources\\Shaders\\BasicProjectionShader");

			XENO_GL_CALL(glGenVertexArrays(1, &m_vertexArrayID)); // TODO: Find out if this can fail. If so, what happens to m_vertexArrayID?
			
			// TODO: Find out if my theory is correct, but I think this: Creates a simple array-based buffer on the GPU.
			XENO_GL_CALL(glGenBuffers(1, &m_vertexBufferID)); // TODO: Find out of this can fail. If so, what happens to m_vertexBufferID?
			BindVertexArray();
			TransferData();

			m_modelViewProjectionMatrixLocation = glGetUniformLocation(m_shader.GetProgramID(), "MVP");
			XENO_LOG("MPV Location: " + std::to_string(m_modelViewProjectionMatrixLocation));
			XENO_LOG("Program ID: " + std::to_string(m_shader.GetProgramID()));
		}

		Triangle::~Triangle()
		{
		}

		void Triangle::BindVertexArray()
		{
			XENO_GL_CALL(glBindVertexArray(m_vertexArrayID));
		}

		void Triangle::BindVertexBuffer()
		{
			XENO_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferID));
		}

		void Triangle::TransferData()
		{
			// TODO: Find out how I should handle drawing modes.
			BindVertexBuffer();
			XENO_GL_CALL(glBufferData(GL_ARRAY_BUFFER, sizeof(m_vertexBufferData), m_vertexBufferData, GL_STATIC_DRAW)); 
		}

		void Triangle::Render()
		{
			glm::mat4 modelMatrix = glm::mat4(1.0f); // TODO: What is this?
			glm::mat4 viewMatrix = glm::lookAt(m_attachedCamera->GetPosition(), m_attachedCamera->GetLookPosition(), m_attachedCamera->GetWorldUp());
			glm::mat4 projectionMatrix = glm::perspective(glm::radians(m_attachedCamera->GetFieldOfView()), m_attachedCamera->GetAspectRatio(), m_attachedCamera->GetNearPlane(), m_attachedCamera->GetFarPlane());
			m_modelViewProjectionMatrix = projectionMatrix * viewMatrix * modelMatrix;

			XENO_LOG("Model Mat: " + glm::to_string(modelMatrix));
			XENO_LOG("View Mat: " + glm::to_string(viewMatrix));
			XENO_LOG("Projection Mat: " + glm::to_string(projectionMatrix));
			XENO_LOG("MVP: " + glm::to_string(m_modelViewProjectionMatrix));

			// Enable the first attribute (0) which represents the vertex list in the shader? TODO: Find out if that's correct.
			BindVertexBuffer();
			XENO_GL_CALL(glUseProgram(m_shader.GetProgramID()));
			// TODO: Find out what all parameters here does.
			glUniformMatrix4fv(m_modelViewProjectionMatrixLocation, 1, GL_FALSE, &m_modelViewProjectionMatrix[0][0]);

			XENO_GL_CALL(glEnableVertexAttribArray(0));
			XENO_GL_CALL(glVertexAttribPointer( // Sends the vertex data to a specific attribute in the shader? TODO: Find out if that's correct.
				0, // Tells OpenGL which attribute you want to send the data to. In this case 0 which is the vertex list in the shader.
				3, // The amount of values in a single "value". (Basically in this case a vector3, aka x, y and z. so 3 values)
				GL_FLOAT, // The type of the values.
				GL_FALSE, // True of they should be normalized by OpenGL. We set it to false since they already are normalized.
				0, // The stride, aka the amount of space between each value.
				(void*)0 // The offset which the values have in the array buffer. In this case they start right away so it's just 0.
			));

			XENO_GL_CALL(glDrawArrays(GL_TRIANGLES, 0, 3)); // Start from vertex 0 and stop at vertex 3 (which is the last one).
			XENO_GL_CALL(glDisableVertexAttribArray(0)); // We can now disable the vertex attribute we activated earlier.
		};

	}
}