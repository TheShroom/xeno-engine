#pragma once
#include <Xeno/EngineIncludes.h>
#include <Xeno/Graphics/Shader.h>
#include <Xeno/Graphics/Camera.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

namespace Xeno
{
	namespace Temporary
	{

		class ENGINEAPI Triangle
		{
		private:
			unsigned int m_vertexArrayID;
			unsigned int m_vertexBufferID;
			unsigned int m_modelViewProjectionMatrixLocation;

			Graphics::Shader m_shader;
			Graphics::Camera* m_attachedCamera;

			const float m_vertexBufferData[9] = {
				-1.0f, -1.0f,  0.0f, // Vertex 1 (bottom left)
				 1.0f, -1.0f,  0.0f, // Vertex 2 (bottom right)
				 0.0f,  1.0f,  0.0f	 // Vertex 3 (top middle) 
			};

			glm::mat4 m_modelViewProjectionMatrix;
		protected:
		public:
			// Creates a new triangle.
			Triangle(Graphics::Camera* mainCamera);
			~Triangle();

			// Binds the vertex array object of the triangle.
			void BindVertexArray();
			// Binds the vertex buffer object of the triangle.
			void BindVertexBuffer();
			// Transfers the data on the CPU/RAM to the GPU/VRAM. TODO: Find out when this should be called. Maybe when the data on the CPU has changed?
			void TransferData();
			// Draws the triangle to the screen buffer thingy.
			void Render();
		};

	}
}