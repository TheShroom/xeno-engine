#pragma once
#include <Xeno/EngineDefines.h>
#include <Xeno/Types/String.h>

namespace Xeno
{
	namespace Graphics
	{
		class ENGINEAPI Shader
		{
		private:
			unsigned int m_vertexShaderID;
			unsigned int m_fragmentShaderID;
			unsigned int m_programID;
		protected:
		public:
			Shader();
			// Creates a new shader using the provided shader code.
			Shader(const std::string& vertexCode, const std::string& fragmentCode);

			// Creates a new shader using the provided shader code. Returns true on success.
			bool CreateShader(const std::string& vertexCode, const std::string& fragmentCode);

			// Returns the shader program ID.
			unsigned int GetProgramID();
		};
	}
}