#pragma once
#include "Mesh.h"
#include "Shader.h"
#include "Material.h"
#include <Xeno/EngineDefines.h>

namespace Xeno
{
	namespace Graphics
	{
		class ENGINEAPI Renderable
		{
		private:
			Mesh m_mesh;
			Material* m_material;

			// OpenGL Stuff
			unsigned int m_vertexArray;
			unsigned int m_vertexBuffer;
			unsigned int m_indexBuffer;

			std::vector<float> m_vertexBufferData; // NOTE: Not sure if this is required or not
		protected:
		public:
			Renderable();
			Renderable(Mesh mesh, Material* material);

			void Initialize();
			void UpdateMeshData();

			Mesh GetMesh();
			void SetMesh(Mesh mesh);

			Material* GetMaterial();
			void SetMaterial(Material* shader);

			unsigned int GetVertexArray();
			unsigned int GetVertexBuffer();
			unsigned int GetIndexBuffer();
		};
	}
}