#pragma once
#include "../EngineIncludes.h"
#include "Renderable.h"

namespace Xeno
{
	namespace Graphics
	{
		// Abstract class representing a renderer.
		class ENGINEAPI IRenderer
		{
		private:
		protected:
		public:
			// Initializes the renderer.
			virtual bool Initialize() = 0;
			// Renders a renderable object.
			virtual void Render(const Renderable& renderable) = 0;
			// Destroys the renderer, freeing all its resources.
			virtual void Destroy() = 0;
		};

	}
}