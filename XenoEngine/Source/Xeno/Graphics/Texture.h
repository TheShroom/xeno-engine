#pragma once
#include <Xeno/EngineDefines.h>
#include <GLEW/GL/glew.h>
#include <vector>

namespace Xeno
{
	namespace Graphics
	{
		class ENGINEAPI Texture
		{
		public:
			enum ColorType : int
			{
				RGB = GL_RGB,
				RGBA = GL_RGBA
			};

			enum TextureFilter : int
			{
				TEXTURE_LINEAR = GL_LINEAR,
				TEXTURE_NEAREST_NEIGHBOR = GL_NEAREST,
				TEXTURE_LINEAR_MIPMAP_LINEAR = GL_LINEAR_MIPMAP_LINEAR,
				TEXTURE_LINEAR_MIPMAP_NEAREST_NEIGHBOR = GL_LINEAR_MIPMAP_NEAREST,
				TEXTURE_NEAREST_NEIGHBOR_MIPMAP_NEAREST_NEIGHBOR = GL_NEAREST_MIPMAP_NEAREST,
				TEXTURE_NEAREST_NEIGHBOR_MIPMAP_LINEAR = GL_NEAREST_MIPMAP_LINEAR
			};

			enum TextureWrap : int
			{
				REPEAT = GL_REPEAT,
				MIRRORED_REPEAT = GL_MIRRORED_REPEAT,
				CLAMP_TO_EDGE = GL_CLAMP_TO_EDGE,
				CLAMP_TO_BORDER = GL_CLAMP_TO_BORDER
			};
		private:
			int m_width;
			int m_height;
			bool m_useMipmaps;
			unsigned short m_colorChannelCount;
			ColorType m_colorType;
			unsigned int m_texture;
			TextureFilter m_magnificationFilter;
			TextureFilter m_minificationFilter;
			TextureWrap m_xWrap;
			TextureWrap m_yWrap;
		protected:
		public:
			Texture();
			Texture(std::vector<unsigned char> data, int width, int height, ColorType colorType, bool mipmap, TextureFilter minificationFilter, TextureFilter maginificationFilter, TextureWrap xWrap, TextureWrap yWrap);
			void SetColorType(ColorType colorType);
			ColorType GetColorType();

			void SetHeight(int height);
			int GetHeight();
			void SetWidth(int width);
			int GetWidth();

			void SetMipmapped(bool useMipmaps);
			bool IsUsingMipmaps();

			void SetMagnificationFiltering(TextureFilter filter);
			TextureFilter GetMagnificationFiltering();
			void SetMinificationFiltering(TextureFilter filter);
			TextureFilter GetMinificationFiltering();

			void SetXWrap(TextureWrap wrap);
			TextureWrap GetXWrap();
			void SetYWrap(TextureWrap wrap);
			TextureWrap GetYWrap();

			void BindTexture();
			void UnbindTexture();
			void TransferTextureData(std::vector<unsigned char> data);
		};
	}
}