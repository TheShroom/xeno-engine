#include "Camera.h"

namespace Xeno
{
	namespace Graphics
	{

		Camera::Camera() { }
		Camera::~Camera() { }

		bool Camera::Initialize(int fieldOfView, float viewSize, float nearPlane, float farPlane, CameraMode cameraMode, Window* window)
		{
			XENO_LOG("Initializing camera");
			m_cameraFrontDir = glm::vec3(0.0f, 0.0f, -1.0f);
			m_window = window;
			SetCameraMode(cameraMode);
			SetFieldOfView(fieldOfView);
			SetViewSize(viewSize);
			SetNearPlane(nearPlane);
			SetFarPlane(farPlane);
			SetPosition(glm::vec3(15.0f, 0.0f, -5.0f));
			SetLookPosition(glm::vec3(0.0f, 0.33f, 0.0f));
			SetWorldUp(glm::vec3(0.0f, 1.0f, 0.0f));
			return true; // Can never fail.
		}

		void Camera::SetFieldOfView(int fieldOfView)
		{
			m_fieldOfView = fieldOfView;
		}

		float Camera::GetFieldOfView()
		{
			return m_fieldOfView;
		}

		void Camera::SetViewSize(float viewSize)
		{
			m_viewSize = viewSize;
		}

		float Camera::GetViewSize()
		{
			return m_viewSize;
		}

		float Camera::GetAspectRatio()
		{
			return m_window->GetWindowSize().x / m_window->GetWindowSize().y;
		}

		void Camera::SetNearPlane(float nearPlane)
		{
			m_nearPlane = nearPlane;
		}

		float Camera::GetNearPlane()
		{
			return m_nearPlane;
		}

		void Camera::SetFarPlane(float farPlane)
		{
			m_farPlane = farPlane;
		}

		float Camera::GetFarPlane()
		{
			return m_farPlane;
		}

		void Camera::SetPosition(glm::vec3 position)
		{
			m_position = position;
		}

		glm::vec3 Camera::GetPosition()
		{
			return m_position;
		}

		void Camera::SetLookPosition(glm::vec3 lookPosition)
		{
			m_lookPosition = lookPosition;
		}

		glm::vec3 Camera::GetLookPosition()
		{
			return m_lookPosition;
		}

		void Camera::SetWorldUp(glm::vec3 worldUp)
		{
			m_worldUpDir = worldUp;
		}

		glm::vec3 Camera::GetWorldUp()
		{
			return m_worldUpDir;
		}

		glm::vec3 Camera::GetFrontDir()
		{
			return m_cameraFrontDir;
		}

		glm::highp_mat4 Camera::GetCameraProjectionMatrix()
		{
			if (m_cameraMode == CameraMode::Perspective)
			{
				return glm::perspective(
					glm::radians(GetFieldOfView()), 
					GetAspectRatio(),
					GetNearPlane(), 
					GetFarPlane()
				);
			}
			else if (m_cameraMode == CameraMode::Orthographic)
			{
				return glm::ortho((-1.0f * GetViewSize())  * GetAspectRatio(), (1.0f * GetViewSize()) * GetAspectRatio(), (-1.0f * GetViewSize()), (1.0f * GetViewSize()), GetNearPlane(), GetFarPlane());
			}
		}

		void Camera::SetCameraMode(CameraMode cameraMode)
		{
			m_cameraMode = cameraMode;
		}

		Camera::CameraMode Camera::GetCameraMode()
		{
			return m_cameraMode;
		}

	}
}