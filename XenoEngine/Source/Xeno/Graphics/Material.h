#pragma once
#include <GLEW/GL/glew.h>
#include <Xeno/EngineDefines.h>
#include <Xeno/Debug/XenoGLDebug.h>
#include "Shader.h"
#include <glm/glm.hpp>
#include <map>
#include <Xeno/Types/Types.h>
#include "Texture.h"

namespace Xeno
{
	namespace Graphics
	{
		class ENGINEAPI Material
		{
		private:
			Shader* m_shader;
			Texture* m_texture;

			std::map<Types::String, int> m_shaderBufferIds;
		protected:
		public:
			Material();
			Material(Shader* shader);
			Material(Shader* shader, Texture* texture);

			void UseMaterial();
			static void UnuseMaterial();

			void SetShader(Shader* shader);
			Shader* GetShader();

			void SetTexture(Texture* texture);
			Texture* GetTexture();

			/* Contains all different possible overloads for setting uniforms.
			 * Everything from a single float to a 4 by 4 matrix, or a 4-value vector. */

			void SetUniform(const std::string& name, float value);
			void SetUniform(const std::string& name, float* value);
			void SetUniform(const std::string& name, Types::Vector2 value);
			void SetUniform(const std::string& name, Types::Vector3 value);
			void SetUniform(const std::string& name, Types::Vector4 value);
			void SetUniform(const std::string& name, Types::Matrix2x2* value);
			void SetUniform(const std::string& name, Types::Matrix3x2* value);
			void SetUniform(const std::string& name, Types::Matrix2x3* value);
			void SetUniform(const std::string& name, Types::Matrix3x3* value);
			void SetUniform(const std::string& name, Types::Matrix4x3* value);
			void SetUniform(const std::string& name, Types::Matrix3x4* value);
			void SetUniform(const std::string& name, Types::Matrix4x4 value);

			void SetUniformById(int id, float value);
			void SetUniformById(int id, float* value);
			void SetUniformById(int id, Types::Vector2 value);
			void SetUniformById(int id, Types::Vector3 value);
			void SetUniformById(int id, Types::Vector4 value);
			void SetUniformById(int id, Types::Matrix2x2* value);
			void SetUniformById(int id, Types::Matrix3x2* value);
			void SetUniformById(int id, Types::Matrix2x3* value);
			void SetUniformById(int id, Types::Matrix3x3* value);
			void SetUniformById(int id, Types::Matrix4x3* value);
			void SetUniformById(int id, Types::Matrix3x4* value);
			void SetUniformById(int id, Types::Matrix4x4 value);

			int GetUniformIdByName(const std::string& name);

			template<typename T>
			T GetUniform(const std::string& name) 
			{ 
				// Unknown type. Needs to be one of the specialized templates.
				XENO_THROW_EXCEPTION("GetUniform() - Unsupported type. Has to be a type which matches one of the spcialized template functions.");
				// XENO_DEBUG_ASSERT(false);
			}

			template <>
			float GetUniform<float>(const std::string& name)
			{
				int id = GetUniformIdByName(name);
				if (id < 0)
				{
					XENO_THROW_EXCEPTION("Tried to get uniform variable '" + name + "' but it returned error code " + std::to_string(id));
					return 0.0f;
				}

				return GetUniformById<float>(GetUniformIdByName(name));
			}

			template <>
			float* GetUniform<float*>(const std::string& name)
			{
				int id = GetUniformIdByName(name);
				if (id < 0)
				{
					XENO_THROW_EXCEPTION("Tried to get uniform variable '" + name + "' but it returned error code " + std::to_string(id));
					return nullptr;
				}
			}

			template <>
			Types::Vector2 GetUniform<Types::Vector2>(const std::string& name)
			{
				return GetUniformById<Types::Vector2>(GetUniformIdByName(name));
			}

			template <>
			Types::Vector3 GetUniform<Types::Vector3>(const std::string& name)
			{
				return GetUniformById<Types::Vector3>(GetUniformIdByName(name));
			}

			template <>
			Types::Vector4 GetUniform<Types::Vector4>(const std::string& name)
			{
				return GetUniformById<Types::Vector4>(GetUniformIdByName(name));
			}

			template <>
			Types::Matrix2x2 GetUniform<Types::Matrix2x2>(const std::string& name)
			{
				return GetUniformById<Types::Matrix2x2>(GetUniformIdByName(name));
			}

			template <>
			Types::Matrix2x3 GetUniform<Types::Matrix2x3>(const std::string& name)
			{
				return GetUniformById<Types::Matrix2x3>(GetUniformIdByName(name));
			}

			template <>
			Types::Matrix3x2 GetUniform<Types::Matrix3x2>(const std::string& name)
			{
				return GetUniformById<Types::Matrix3x2>(GetUniformIdByName(name));
			}

			template <>
			Types::Matrix3x3 GetUniform<Types::Matrix3x3>(const std::string& name)
			{
				return GetUniformById<Types::Matrix3x3>(GetUniformIdByName(name));
			}

			template <>
			Types::Matrix3x4 GetUniform<Types::Matrix3x4>(const std::string& name)
			{
				return GetUniformById<Types::Matrix3x4>(GetUniformIdByName(name));
			}

			template <>
			Types::Matrix4x3 GetUniform<Types::Matrix4x3>(const std::string& name)
			{
				return GetUniformById<Types::Matrix4x3>(GetUniformIdByName(name));
			}

			template <>
			Types::Matrix4x4 GetUniform<Types::Matrix4x4>(const std::string& name)
			{
				return GetUniformById<Types::Matrix4x4>(GetUniformIdByName(name));
			}

			template <typename T>
			T GetUniformById(int id)
			{
				// Unknown type. needs to be one of the specialized templates.
				XENO_THROW_EXCEPTION("GetUniformById() - Unsupported type. Has to be a type which matches one of the spcialized template functions.");
				// XENO_DEBUG_ASSERT(false);
			}

			template <typename T>
			std::vector<T> GetUniformListById(int id, int count)
			{
				XENO_THROW_EXCEPTION("GetUniformById() - Unsupported type. Has to be a type which matches one of the specialized template functions.");
			}

			template <>
			float GetUniformById<float>(int id)
			{
				GLfloat value = 0;
				XENO_GL_CALL(glGetUniformfv(m_shader->GetProgramID(), id, &value));
				// TODO: Error check if the uniform exists before attempting to use.
				return (float)(value); // Return the first element since there only is one and convert it from GLfloat to normal float.
			}

			template <>
			std::vector<float> GetUniformListById<float>(int id, int count)
			{
				GLfloat* values = new GLfloat[count];
				XENO_GL_CALL(glGetUniformfv(m_shader->GetProgramID(), id, values));
				std::vector<float> vec;
				
				for (int i = 0; i < count; i++)
				{
					vec.push_back((float)values[i]);
				}

				return vec;
			}

			template <>
			Types::Vector2 GetUniformById<Types::Vector2>(int id)
			{
				float* buffer = nullptr;
				XENO_GL_CALL(glGetnUniformfv(m_shader->GetProgramID(), id, sizeof(GLfloat) * 2, buffer));
				return Types::Vector2(buffer[0], buffer[1]);
			}

			template <>
			Types::Vector3 GetUniformById<Types::Vector3>(int id)
			{
				float* buffer = nullptr;
				XENO_GL_CALL(glGetnUniformfv(m_shader->GetProgramID(), id, sizeof(GLfloat) * 3, buffer));
				return Types::Vector3(buffer[0], buffer[1], buffer[2]);
			}

			template <>
			Types::Vector4 GetUniformById<Types::Vector4>(int id)
			{
				GLfloat buffer[4] = {0.0f, 0.0f, 0.0f, 0.0f};
				XENO_GL_CALL(glGetnUniformfv(m_shader->GetProgramID(), id, sizeof(GLfloat) * 4, &buffer[0]));
				return Types::Vector4(buffer[0], buffer[1], buffer[2], buffer[3]);
			}

			template <>
			Types::Matrix2x2 GetUniformById<Types::Matrix2x2>(int id)
			{
				GLfloat buffer[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
				XENO_GL_CALL(glGetnUniformfv(m_shader->GetProgramID(), id, sizeof(GLfloat) * 4, &buffer[0]));
				return Types::Matrix2x2(buffer[0], buffer[1], buffer[2], buffer[3]);
			}

			template <>
			Types::Matrix2x3 GetUniformById<Types::Matrix2x3>(int id)
			{
				GLfloat buffer[6] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
				XENO_GL_CALL(glGetnUniformfv(m_shader->GetProgramID(), id, sizeof(GLfloat) * 6, &buffer[0]));
				return Types::Matrix2x3(buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5]);
			}

			template <>
			Types::Matrix3x2 GetUniformById<Types::Matrix3x2>(int id)
			{
				GLfloat buffer[6] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
				XENO_GL_CALL(glGetnUniformfv(m_shader->GetProgramID(), id, sizeof(GLfloat) * 6, &buffer[0]));
				return Types::Matrix3x2(buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5]);
			}

			template <>
			Types::Matrix3x3 GetUniformById<Types::Matrix3x3>(int id)
			{
				GLfloat buffer[9] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
				XENO_GL_CALL(glGetnUniformfv(m_shader->GetProgramID(), id, sizeof(GLfloat) * 9, &buffer[0]));
				return Types::Matrix3x3(buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7], buffer[8]);
			}

			template <>
			Types::Matrix3x4 GetUniformById<Types::Matrix3x4>(int id)
			{
				GLfloat buffer[12] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
				XENO_GL_CALL(glGetnUniformfv(m_shader->GetProgramID(), id, sizeof(GLfloat) * 12, &buffer[0]));
				return Types::Matrix3x4(buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7], buffer[8], buffer[9], buffer[10], buffer[11]);
			}

			template <>
			Types::Matrix4x3 GetUniformById<Types::Matrix4x3>(int id)
			{
				GLfloat buffer[12] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
				XENO_GL_CALL(glGetnUniformfv(m_shader->GetProgramID(), id, sizeof(GLfloat) * 12, &buffer[0]));
				return Types::Matrix4x3(buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7], buffer[8], buffer[9], buffer[10], buffer[11]);
			}

			template <>
			Types::Matrix4x4 GetUniformById<Types::Matrix4x4>(int id)
			{
				GLfloat buffer[16] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
				XENO_GL_CALL(glGetnUniformfv(m_shader->GetProgramID(), id, sizeof(GLfloat) * 16, &buffer[0]));
				return Types::Matrix4x4(buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7], buffer[8], buffer[9], buffer[10], buffer[11], buffer[12], buffer[13], buffer[14], buffer[15]);
			}

			bool ValidateUniformId(int id);
		};
	}
}