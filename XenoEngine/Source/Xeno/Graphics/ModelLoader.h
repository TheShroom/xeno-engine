#pragma once
#include <Xeno/EngineDefines.h>
#include "Mesh.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <vector>
#include "Texture.h"

namespace Xeno
{
	namespace Graphics
	{
		class ENGINEAPI ModelLoader
		{
		private:
			ModelLoader();

			std::vector<Mesh> ProcessAssimpNode(aiNode * node, const aiScene * scene);
			Mesh ProcessAssimpMesh(aiMesh * mesh, const aiScene * scene);
			std::vector<std::string> LoadMaterialTextures(aiMaterial * mat, aiTextureType type, const std::string& typeName); // TODO: Replace int with Texture when we get texture support.
		protected:
		public:
			static ModelLoader& GetInstance();

			static Mesh LoadModel(const std::string& modelSource);
		};
	}
}