#pragma once
#include <Xeno/EngineDefines.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include "Window.h"

namespace Xeno
{
	namespace Graphics
	{

		class ENGINEAPI Camera
		{
		public:
			enum CameraMode {
				Perspective,
				Orthographic
			};
		private:
			Window* m_window;

			float m_fieldOfView;
			float m_nearPlane;
			float m_farPlane;
			float m_viewSize;
			glm::mat4 m_projectionMatrix;
			glm::mat4 m_viewMatrix;
			glm::mat4 m_cameraMatrix;
			glm::mat4 m_modelMatrix;
			glm::mat4 m_modelViewProjectionMatrix;

			glm::vec3 m_position;
			glm::vec3 m_lookPosition;
			glm::vec3 m_worldUpDir;
			glm::vec3 m_cameraFrontDir;

			CameraMode m_cameraMode;

		protected:
		public:
			Camera();
			~Camera();

			bool Initialize(int fieldOfView, float viewSize, float nearPlane, float farPlane, CameraMode cameraMode, Window* window);

			void SetFieldOfView(int fieldOfView);
			float GetFieldOfView();

			void SetViewSize(float viewSize);
			float GetViewSize();

			float GetAspectRatio();

			void SetNearPlane(float nearPlane);
			float GetNearPlane();

			void SetFarPlane(float farPlane);
			float GetFarPlane();

			void SetPosition(glm::vec3 position);
			glm::vec3 GetPosition();

			void SetLookPosition(glm::vec3 lookPosition);
			glm::vec3 GetLookPosition();

			void SetWorldUp(glm::vec3 worldUp);
			glm::vec3 GetWorldUp();
			glm::vec3 GetFrontDir();

			glm::highp_mat4 GetCameraProjectionMatrix();

			void SetCameraMode(CameraMode cameraMode);
			CameraMode GetCameraMode();
		};

	}
}