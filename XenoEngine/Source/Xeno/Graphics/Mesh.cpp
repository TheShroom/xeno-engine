#include "Mesh.h"
#include <Xeno/Logging/Logger.h>

namespace Xeno
{
	namespace Graphics
	{
		Mesh::Mesh() {}

		Mesh::Mesh(std::vector<glm::vec3> vertices)
		{
			SetVertices(vertices);
			
			// TODO: Should probably remove the below code since it literally just generates a second vector of vertices and takes twice as much memory.
			//		 We could probably just use the vertex vector when calling glDrawElements, or maybe we could have a bool flag in the mesh which
			//		 is set to false when the index list is left uninitialized and then in the renderer check the flag and use glDrawArrays if it's false.
			std::vector<unsigned int> indices;
			for (unsigned int i = 0; i < vertices.size(); i++)
			{
				indices.push_back(i);
			}

			SetIndices(indices);
		}

		Mesh::Mesh(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices)
		{
			SetVertices(vertices);
			SetIndices(indices);
		}

		Mesh::Mesh(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices, std::vector<float> texCoords)
		{
			SetVertices(vertices);
			SetIndices(indices);
			SetTexCoords(texCoords);
		}

		Mesh::Mesh(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices, std::vector<float> texCoords, std::vector<std::string> textures)
		{
			SetVertices(vertices);
			SetIndices(indices);
			SetTexCoords(texCoords);
			SetTextures(textures);

			XENO_LOG("Texture Coordinates:");
			for (int i = 0; i < texCoords.size(); i+=2)
			{
				XENO_LOG(std::to_string(i) + " - X(" + std::to_string(texCoords[i]) + "), Y(" + std::to_string(texCoords[i + 1]) + ")");
			}
		}

		void Mesh::SetVertices(std::vector<glm::vec3> vertices)
		{
			m_vertices = vertices;
		}

		void Mesh::SetIndices(std::vector<unsigned int> indices)
		{
			m_indices = indices;
		}

		void Mesh::SetTexCoords(std::vector<float> texCoords)
		{
			m_texCoords = texCoords;
		}

		void Mesh::SetTextures(std::vector<std::string> textures)
		{
			m_textures = textures;
		}

		std::vector<glm::vec3> Mesh::GetVertices()
		{
			return m_vertices;
		}

		std::vector<unsigned int> Mesh::GetIndices()
		{
			return m_indices;
		}

		std::vector<float> Mesh::GetTexCoords()
		{
			return m_texCoords;
		}

		std::vector<std::string> Mesh::GetTextures()
		{
			return m_textures;
		}
	}
}