#include "Material.h"

namespace Xeno
{
	namespace Graphics
	{
		Material::Material() :
			m_shader(nullptr), m_texture(nullptr)
		{

		}

		Material::Material(Shader* shader) : 
			m_texture(nullptr)
		{
			SetShader(shader);
		}

		Material::Material(Shader* shader, Texture* texture)
		{
			SetShader(shader);
			SetTexture(texture);
		}

		void Material::UseMaterial()
		{
			if (m_texture != nullptr) // Only activate the texture if the Material actually has one.
			{
				m_texture->BindTexture();
			}

			XENO_GL_CALL(glUseProgram(m_shader->GetProgramID()));
		}

		void Material::UnuseMaterial()
		{
			XENO_GL_CALL(glUseProgram(0));
		}

		void Material::SetShader(Shader* shader)
		{
			m_shader = shader;
		}

		Shader* Material::GetShader()
		{
			return m_shader;
		}

		void Material::SetTexture(Texture* texture)
		{
			m_texture = texture;
		}

		Texture* Material::GetTexture()
		{
			return m_texture;
		}

		void Material::SetUniform(const std::string& name, float value)
		{
			SetUniformById(GetUniformIdByName(name), value);
		}

		void Material::SetUniform(const std::string& name, Types::Vector2 value)
		{
			SetUniformById(GetUniformIdByName(name), value);
		}

		void Material::SetUniform(const std::string& name, Types::Vector3 value)
		{
			SetUniformById(GetUniformIdByName(name), value);
		}

		void Material::SetUniform(const std::string& name, Types::Vector4 value)
		{
			SetUniformById(GetUniformIdByName(name), value);
		}

		void Material::SetUniform(const std::string& name, Types::Matrix2x2* value)
		{
			SetUniformById(GetUniformIdByName(name), value);
		}

		void Material::SetUniform(const std::string& name, Types::Matrix3x2* value)
		{
			SetUniformById(GetUniformIdByName(name), value);
		}

		void Material::SetUniform(const std::string& name, Types::Matrix2x3* value)
		{
			SetUniformById(GetUniformIdByName(name), value);
		}

		void Material::SetUniform(const std::string& name, Types::Matrix3x3* value)
		{
			SetUniformById(GetUniformIdByName(name), value);
		}

		void Material::SetUniform(const std::string& name, Types::Matrix4x3* value)
		{
			SetUniformById(GetUniformIdByName(name), value);
		}

		void Material::SetUniform(const std::string& name, Types::Matrix3x4* value)
		{
			SetUniformById(GetUniformIdByName(name), value);
		}

		void Material::SetUniform(const std::string& name, Types::Matrix4x4 value)
		{
			SetUniformById(GetUniformIdByName(name), value);
		}

		void Material::SetUniformById(int id, float value)
		{
			ValidateUniformId(id);

			UseMaterial();
			XENO_GL_CALL(glUniform1f(id, value));
			UnuseMaterial();
		}

		void Material::SetUniformById(int id, Types::Vector2 value)
		{
			ValidateUniformId(id);

			UseMaterial();
			XENO_GL_CALL(glUniform2f(id, value.x, value.y));
			UnuseMaterial();
		}

		void Material::SetUniformById(int id, Types::Vector3 value)
		{
			ValidateUniformId(id);

			UseMaterial();
			XENO_GL_CALL(glUniform3f(id, value.x, value.y, value.z));
			UnuseMaterial();
		}

		void Material::SetUniformById(int id, Types::Vector4 value)
		{
			ValidateUniformId(id);

			UseMaterial();
			XENO_GL_CALL(glUniform4f(id, value.x, value.y, value.z, value.w));
			UnuseMaterial();
		}

		void Material::SetUniformById(int id, Types::Matrix2x2* value)
		{
			ValidateUniformId(id);

			UseMaterial();
			XENO_GL_CALL(glUniformMatrix2fv(id, 2*2, GL_FALSE, &(*value)[0][0])); // This syntax though.
			UnuseMaterial();
		}

		void Material::SetUniformById(int id, Types::Matrix3x2* value)
		{
			ValidateUniformId(id);

			UseMaterial();
			XENO_GL_CALL(glUniformMatrix2fv(id, 2 * 2, GL_FALSE, &(*value)[0][0])); // This syntax though. 
			UnuseMaterial();
		}

		void Material::SetUniformById(int id, Types::Matrix2x3* value)
		{
			ValidateUniformId(id);

			UseMaterial();
			XENO_GL_CALL(glUniformMatrix2fv(id, 2 * 2, GL_FALSE, &(*value)[0][0])); // This syntax though. 
			UnuseMaterial();
		}

		void Material::SetUniformById(int id, Types::Matrix3x3* value)
		{
			ValidateUniformId(id);

			UseMaterial();
			XENO_GL_CALL(glUniformMatrix2fv(id, 2 * 2, GL_FALSE, &(*value)[0][0])); // This syntax though. 
			UnuseMaterial();
		}

		void Material::SetUniformById(int id, Types::Matrix4x3* value)
		{
			ValidateUniformId(id);

			UseMaterial();
			XENO_GL_CALL(glUniformMatrix2fv(id, 2 * 2, GL_FALSE, &(*value)[0][0])); // This syntax though. 
			UnuseMaterial();
		}

		void Material::SetUniformById(int id, Types::Matrix3x4* value)
		{
			ValidateUniformId(id);

			UseMaterial();
			XENO_GL_CALL(glUniformMatrix2fv(id, 2 * 2, GL_FALSE, &(*value)[0][0])); // This syntax though. 
			UnuseMaterial();
		}

		void Material::SetUniformById(int id, Types::Matrix4x4 value)
		{
			ValidateUniformId(id);

			UseMaterial();
			XENO_GL_CALL(glUniformMatrix4fv(id, 1, GL_FALSE, &(value)[0][0])); // This syntax though. 
			UnuseMaterial();
		}

		int Material::GetUniformIdByName(const std::string& name)
		{
			UseMaterial();
			return XENO_GL_CALL(glGetUniformLocation(m_shader->GetProgramID(), name.c_str()));
			UnuseMaterial();
		}

		bool Material::ValidateUniformId(int id)
		{
			if (id < 0)
			{
				XENO_THROW_EXCEPTION("Setting an invalid uniform. ID: " + std::to_string(id));
				return false;
			}

			return true;
		}

	}
}