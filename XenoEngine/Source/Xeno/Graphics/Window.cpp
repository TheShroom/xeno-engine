#include "Window.h"
#include <Xeno/Debug/XenoGLDebug.h>

namespace Xeno
{
	namespace Graphics
	{
		Window::Window(const std::string& title, int width, int height)
		{
			CreateWindow(title, width, height);
		}

		Window::Window()
		{

		}

		Window::~Window()
		{

		}

		void Window::CreateWindow(const std::string& title, int width, int height)
		{
			XENO_LOG("Initializing window module.");

			InitializeGLFW();

			SetSamples(16); // Set the samples to 4 by default. TODO: Find out if this can be set after the window has been created.
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // Set the OpenGL version to 3.3.
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
			glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // Needed for MacOS. TODO: Find out why.
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // Enable new OpenGL functionality using its core profile.

			m_window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL); // Todo: Find out what the last 2 parameters are.
			if (m_window == NULL)
			{
				glfwTerminate();
				XENO_THROW_EXCEPTION("Failed to create GLFW window.");
			}

			glfwSetFramebufferSizeCallback(m_window, Framebuffer_Size_Callback);

			Types::Vector2 windowSize = GetWindowSize();
			SetViewportRect(0, 0, windowSize.x, windowSize.y);

			XENO_LOG("GLFW window created.");

			glfwMakeContextCurrent(m_window); // Set the created window to be the current context.
			XENO_LOG("GLFW OpenGL context set.");

			InitializeGLEW();

			SetVSyncEnabled(true);

			Color clearColor = Color(0.33f, 0.33f, 0.33f, 0.0f); // Todo: Remove?
			SetClearColor(clearColor);

			XENO_LOG("Window module initialized.");
		}

		void Window::InitializeGLFW()
		{
			glfwSetErrorCallback(GLFW_Error_Callback);

			if (!glfwInit())
			{
				glfwTerminate();
				XENO_THROW_EXCEPTION("Failed to initialize GLFW.");
			}
		}

		void Window::InitializeGLEW()
		{
			glewExperimental = true; // Need to set this in order to use the OpenGL core profile functionality, aka modern OpenGL.
			unsigned int glewCode = glewInit();
			if (glewCode != GLEW_OK)
			{
				glfwTerminate();
				XENO_THROW_EXCEPTION("Failed to initialize GLEW, code:" + std::to_string(glewCode));
			}

			// XENO_GL_CALL(glEnable(GL_DEBUG_OUTPUT));
			// XENO_GL_CALL(glDebugMessageCallback((GLDEBUGPROC)GL_Error_Callback, 0));
		}

		void Window::SwapBuffers()
		{
			glfwSwapBuffers(m_window);
		}

		void Window::Clear()
		{
			XENO_GL_CALL(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
		}

		void Window::SetVSyncEnabled(bool enabled)
		{
			glfwSwapInterval(enabled ? 1 : 0);
		}

		void Window::SetSamples(int sampleCount)
		{
			if (sampleCount > 0)
			{
				glEnable(GL_MULTISAMPLE);
			}
			else
			{
				glDisable(GL_MULTISAMPLE);
			}

			glfwWindowHint(GLFW_SAMPLES, sampleCount);
		}

		void Window::SetViewportRect(int x, int y, int width, int height)
		{
			glViewport(x, y, width, height);
		}

		void Window::SetClearColor(Color color)
		{
			glClearColor(color.r, color.g, color.b, color.a);
		}

		Types::Vector2 Window::GetWindowSize()
		{
			int width;
			int height;
			glfwGetFramebufferSize(m_window, &width, &height);
			return Types::Vector2(width, height);
		}

		bool Window::HasRecievedCloseEvent()
		{
			return glfwWindowShouldClose(m_window);
		}

		GLFWwindow* Window::GetWindow()
		{
			return m_window;
		}
	}
}