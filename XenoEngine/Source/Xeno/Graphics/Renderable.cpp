#include "Renderable.h"
#include <Xeno/Debug/XenoGLDebug.h>
#include <GLEW/GL/glew.h>

namespace Xeno 
{
	namespace Graphics
	{
		Renderable::Renderable() { }

		Renderable::Renderable(Mesh mesh, Material* material)
		{
			XENO_LOG("Renderable constructor called!");
			Initialize();
			SetMesh(mesh);
			SetMaterial(material);
			UpdateMeshData();
		}

		void Renderable::Initialize()
		{
			XENO_GL_CALL(glGenVertexArrays(1, &m_vertexArray));
			XENO_GL_CALL(glBindVertexArray(m_vertexArray));
			XENO_GL_CALL(glGenBuffers(1, &m_vertexBuffer));
			XENO_GL_CALL(glGenBuffers(1, &m_indexBuffer));
		}

		void Renderable::UpdateMeshData()
		{
			XENO_LOG("Updating mesh data.");

			m_vertexBufferData = std::vector<float>();
			for (int i = 0; i < m_mesh.GetVertices().size(); i++)
			{
				m_vertexBufferData.push_back(m_mesh.GetVertices()[i].x);
				m_vertexBufferData.push_back(m_mesh.GetVertices()[i].y);
				m_vertexBufferData.push_back(m_mesh.GetVertices()[i].z);
			}

			for (int i = 0; i < m_mesh.GetTexCoords().size(); i++)
			{
				m_vertexBufferData.push_back(m_mesh.GetTexCoords()[i]);
			}

			XENO_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer));
			XENO_GL_CALL(glBufferData(GL_ARRAY_BUFFER, (sizeof(m_vertexBufferData[0]) * m_vertexBufferData.size()), m_vertexBufferData.data(), GL_STATIC_DRAW));
			XENO_GL_CALL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer));
			XENO_GL_CALL(glBufferData(GL_ELEMENT_ARRAY_BUFFER, (sizeof(m_mesh.GetIndices()[0]) * m_mesh.GetIndices().size()), m_mesh.GetIndices().data(), GL_STATIC_DRAW));
		}

		Mesh Renderable::GetMesh()
		{
			return m_mesh;
		}

		void Renderable::SetMesh(Mesh mesh)
		{
			m_mesh = mesh;
			UpdateMeshData();
		}

		Material* Renderable::GetMaterial()
		{
			return m_material;
		}

		void Renderable::SetMaterial(Material* material)
		{
			m_material = material;
		}

		unsigned int Renderable::GetVertexArray()
		{
			return m_vertexArray;
		}

		unsigned int Renderable::GetVertexBuffer()
		{
			return m_vertexBuffer;
		}
	}
}