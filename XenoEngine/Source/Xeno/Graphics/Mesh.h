#pragma once
#include <Xeno/EngineDefines.h>
#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <vector>
#include "Texture.h"
#include <string>
#include <Xeno/Types/Types.h>

namespace Xeno
{
	namespace Graphics
	{
		class ENGINEAPI Mesh
		{
		private:
			std::vector<glm::vec3> m_vertices;
			std::vector<unsigned int> m_indices;
			std::vector<float> m_texCoords;
			std::vector<std::string> m_textures;
		protected:
		public:
			// Only exists for convenience. Does nothing.
			Mesh();
			Mesh(std::vector<glm::vec3> vertices);
			Mesh(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices);
			Mesh(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices, std::vector<float> texCoords);
			Mesh(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices, std::vector<float> texCoords, std::vector<std::string> textures);

			void SetVertices(std::vector<glm::vec3> vertices);
			void SetIndices(std::vector<unsigned int> indices);
			void SetTexCoords(std::vector<float> texCoords);
			void SetTextures(std::vector<std::string> textures);
			std::vector<glm::vec3> GetVertices();
			std::vector<unsigned int> GetIndices();
			std::vector<float> GetTexCoords();
			std::vector<std::string> GetTextures();
		};
	}
}