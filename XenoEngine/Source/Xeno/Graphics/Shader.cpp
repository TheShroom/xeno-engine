#include "Shader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <GLEW/GL/glew.h>
#include <vector>
#include <Xeno/Debug/XenoGLDebug.h>
#include <Xeno/FileIO/PAKLoader.h>

namespace Xeno
{
	namespace Graphics
	{
		Shader::Shader()
		{

		}

		Shader::Shader(const std::string& vertexCode, const std::string& fragmentCode)
		{
			CreateShader(vertexCode, fragmentCode);
		}

		bool Shader::CreateShader(const std::string& vertexCode, const std::string& fragmentCode)
		{
			Types::String vertexSource = vertexCode;
			Types::String fragmentSource = fragmentCode;

			m_vertexShaderID = XENO_GL_CALL(glCreateShader(GL_VERTEX_SHADER));
			m_fragmentShaderID = XENO_GL_CALL(glCreateShader(GL_FRAGMENT_SHADER));

			int result = false;
			int infoLogLength = 0;

			char const* vertexSourcePtr = vertexSource.ToCString();
			char const* fragmentSourcePtr = fragmentSource.ToCString();

			XENO_GL_CALL(glShaderSource(m_vertexShaderID, 1, &vertexSourcePtr, NULL)); // TODO: Find out what the 2nd and last parameters are.
			XENO_GL_CALL(glCompileShader(m_vertexShaderID));

			XENO_GL_CALL(glGetShaderiv(m_vertexShaderID, GL_COMPILE_STATUS, &result));
			XENO_GL_CALL(glGetShaderiv(m_vertexShaderID, GL_INFO_LOG_LENGTH, &infoLogLength));
			if (infoLogLength > 0)
			{
				std::vector<char> vertErrMsg(infoLogLength + 1);
				XENO_GL_CALL(glGetShaderInfoLog(m_vertexShaderID, infoLogLength, NULL, &vertErrMsg[0])); // TODO: Find out what the third parameter is.
				XENO_THROW_EXCEPTION(std::string("Vertex shader compilation failed!\n\nError log: ") + &vertErrMsg[0]);
				return false;
			}

			XENO_GL_CALL(glShaderSource(m_fragmentShaderID, 1, &fragmentSourcePtr, NULL)); // TODO: Find out what the 2nd and last parameters are.
			XENO_GL_CALL(glCompileShader(m_fragmentShaderID));

			XENO_GL_CALL(glGetShaderiv(m_fragmentShaderID, GL_COMPILE_STATUS, &result));
			XENO_GL_CALL(glGetShaderiv(m_fragmentShaderID, GL_INFO_LOG_LENGTH, &infoLogLength));
			if (infoLogLength > 0)
			{
				std::vector<char> fragErrMesg(infoLogLength + 1);
				XENO_GL_CALL(glGetShaderInfoLog(m_fragmentShaderID, infoLogLength, NULL, &fragErrMesg[0])); // TODO: Find out what the third parameter is.
				XENO_THROW_EXCEPTION(std::string("Fragment shader compilation failed!") + "\nError log: " + &fragErrMesg[0]);
				return false;
			}

			m_programID = XENO_GL_CALL(glCreateProgram());
			XENO_GL_CALL(glAttachShader(m_programID, m_vertexShaderID));
			XENO_GL_CALL(glAttachShader(m_programID, m_fragmentShaderID));
			XENO_GL_CALL(glLinkProgram(m_programID));

			XENO_GL_CALL(glGetProgramiv(m_programID, GL_LINK_STATUS, &result));
			XENO_GL_CALL(glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &infoLogLength));
			if (infoLogLength > 0)
			{
				std::vector<char> progLinkErrMsg(infoLogLength + 1);
				XENO_GL_CALL(glGetProgramInfoLog(m_programID, infoLogLength, NULL, &progLinkErrMsg[0])); // TODO: Find out what third parameter is.
				XENO_THROW_EXCEPTION(std::string("Shader program linkage failed!") + "\nError log: " + &progLinkErrMsg[0]);
				return false;
			}

			GLint uniforms;
			glGetProgramiv(m_programID, GL_ACTIVE_UNIFORMS, &uniforms);
			XENO_LOG("Active uniforms in shader is " + std::to_string((int)uniforms));

			XENO_GL_CALL(glDetachShader(m_programID, m_vertexShaderID));
			XENO_GL_CALL(glDetachShader(m_programID, m_fragmentShaderID));
			XENO_GL_CALL(glDeleteShader(m_vertexShaderID));
			XENO_GL_CALL(glDeleteShader(m_fragmentShaderID));

			return true;
		}

		unsigned int Shader::GetProgramID()
		{
			return m_programID;
		}
	}
}