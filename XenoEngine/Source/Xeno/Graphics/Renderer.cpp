#include "Renderer.h"
#include <Xeno/Debug/XenoGLDebug.h>

#include <Xeno/Time/Time.h>

namespace Xeno
{
	namespace Graphics
	{
		int Renderer::m_drawCallCount = 0;

		Renderer::Renderer()
		{
		}

		Renderer& Renderer::GetInstance()
		{
			static Renderer instance;
			return instance;
		}

		void Renderer::Initialize()
		{
			XENO_LOG("Initializing renderer...");

			// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // If this AND blending is enabled, nothing will be rendered.

			// Enable depth testing
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);

			// Enable blending (transparency)
			glEnable(GL_BLEND); 
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			XENO_LOG("Renderer initialized.");
		}

		unsigned int Renderer::GenerateVertexArray()
		{
			unsigned int vertexArrayID;
			XENO_GL_CALL(glGenVertexArrays(1, &vertexArrayID));
			return vertexArrayID;
		}

		unsigned int Renderer::GenerateVertexBuffer()
		{
			unsigned int vertexBufferID;
			XENO_GL_CALL(glGenBuffers(GL_ARRAY_BUFFER, &vertexBufferID));
			return vertexBufferID;
		}

		unsigned int Renderer::GenerateIndexBuffer()
		{
			unsigned int indexBufferID;
			XENO_GL_CALL(glGenBuffers(GL_ELEMENT_ARRAY_BUFFER, &indexBufferID));
			return indexBufferID;
		}

		void Renderer::BufferVertexPositionData(size_t size, void* data)
		{
			XENO_GL_CALL(glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW)); // TODO: Not sure if this should be static or not.
		}

		void Renderer::BufferVertexIndexData(size_t size, void* data)
		{
			XENO_GL_CALL(glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW)); // TODO: Not sure if this should be static or not.
		}

		void Renderer::BindVertexArray(unsigned int id)
		{
			XENO_GL_CALL(glBindVertexArray(id));
		}

		void Renderer::BindVertexBuffer(unsigned int id)
		{
			XENO_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, id));
		}

		void Renderer::BindIndexBuffer(unsigned int id)
		{
			XENO_GL_CALL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id));
		}

		void Renderer::Render(Renderable* renderable)
		{
			renderable->GetMaterial()->UseMaterial();
			XENO_GL_CALL(glEnableVertexAttribArray(0)); // The vertices in the shader is located on attribute layout 0.
			XENO_GL_CALL(glEnableVertexAttribArray(1)); // The texture data in the shader is located on attribute layout 1.

			BindVertexArray(renderable->GetVertexArray());
			int vertexCount = renderable->GetMesh().GetVertices().size();
			unsigned int indexCount = renderable->GetMesh().GetIndices().size();
			XENO_GL_CALL(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0)); // Vertices always cconsists of 3 floats
			XENO_GL_CALL(glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(float) * vertexCount * 3))); // UVs always consist of 2 floats
			m_drawCallCount++;
			XENO_GL_CALL(glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0));
			XENO_GL_CALL(glDisableVertexAttribArray(0));
			XENO_GL_CALL(glDisableVertexAttribArray(1));
			renderable->GetMaterial()->UnuseMaterial();
		}

		int Renderer::GetDrawCallCount()
		{
			return m_drawCallCount;
		}
	}
}