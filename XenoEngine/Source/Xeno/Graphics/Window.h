#pragma once
#include <Xeno/EngineIncludes.h>
#include <Xeno/Types/String.h>
#include <GLEW/GL/glew.h>
#include <GLFW/glfw3.h>
#include <Xeno/Types/Types.h>

namespace Xeno
{
	namespace Graphics
	{
		// Handles the game window.
		class ENGINEAPI Window
		{
		private:
			GLFWwindow * m_window;

			void CreateWindow(const std::string& title, int width, int height);
		protected:
		public:
			static void GLFW_Error_Callback(int code, const char* errorStr)
			{
				XENO_THROW_EXCEPTION("GLFW error!\nError code: " + std::to_string(code) + "\nError: " + errorStr);
			}

			static void GL_Error_Callback(GLenum source,
				GLenum type,
				GLuint id,
				GLenum severity,
				GLsizei length,
				const GLchar* message,
				const void* userParam)
			{
				std::string msg = "OpenGL error:";
				msg += "\n\tSource: " + std::to_string(source);
				msg += "\n\tType: " + std::to_string(type);
				msg += "\n\tId: " + std::to_string(id);
				msg += "\n\tSeverity: " + std::to_string(severity);
				msg += "\n\tLength: " + std::to_string(length);
				msg += "\n\tMessage: " + std::string(message);

				XENO_THROW_EXCEPTION(msg);
			}

			static void Framebuffer_Size_Callback(GLFWwindow* window, int width, int height)
			{
				XENO_LOG("Window size changed, new size (" + std::to_string(width) + ", " + std::to_string(height) + ")");
				glViewport(0, 0, width, height);
			}

			// Creates a new instance of the game window.
			Window(const std::string& title, int width, int height);
			// MEH.
			Window();
			// Closes the window.
			~Window();

			// Initializes the GLFW library.
			void InitializeGLFW();
			// Initializes the GLEW library. (Has to be called AFTER the window context has been created!)
			void InitializeGLEW();
			// Sets the amount of samples the window will use.
			void SetSamples(int sampleCount);
			// Updates the window.
			void SwapBuffers();
			// Clears the window.
			void Clear();
			// Enables or disable VSYNC
			void SetVSyncEnabled(bool enabled);

			void SetViewportRect(int x, int y, int width, int height);

			// Sets the glClearColor.
			void SetClearColor(Color color);

			Types::Vector2 GetWindowSize();

			// Returns true if the window has received a close event.
			bool HasRecievedCloseEvent();

			GLFWwindow* GetWindow();
		};
	}
}