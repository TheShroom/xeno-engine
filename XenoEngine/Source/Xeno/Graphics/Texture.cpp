#include "Texture.h"
#include <Xeno/Debug/XenoGLDebug.h>

namespace Xeno
{
	namespace Graphics
	{
		Texture::Texture()
		{

		}

		Texture::Texture(std::vector<unsigned char> data, int width, int height, ColorType colorType, bool mipmap, TextureFilter minificationFilter, TextureFilter maginificationFilter, TextureWrap xWrap, TextureWrap yWrap)
		{
			switch (colorType)
			{
			case RGB:
				m_colorChannelCount = 3;
				break;
			case RGBA:
				m_colorChannelCount = 4;
				break;
			}

			SetColorType(colorType);

			SetWidth(width);
			SetHeight(height);
			m_useMipmaps = mipmap;

			XENO_GL_CALL(glGenTextures(1, &m_texture));
			BindTexture();
			SetMagnificationFiltering(maginificationFilter);
			SetMinificationFiltering(minificationFilter);
			SetXWrap(xWrap);
			SetYWrap(yWrap);
			TransferTextureData(data);
		}

		void Texture::SetColorType(ColorType colorType)
		{
			m_colorType = colorType;
		}

		Texture::ColorType Texture::GetColorType()
		{
			return m_colorType;
		}

		void Texture::SetHeight(int height)
		{
			m_height = height;
		}

		int Texture::GetHeight()
		{
			return m_height;
		}

		void Texture::SetWidth(int width)
		{
			m_width = width;
		}

		int Texture::GetWidth()
		{
			return m_width;
		}

		void Texture::SetMipmapped(bool useMipmaps)
		{
			m_useMipmaps = useMipmaps;
		}

		bool Texture::IsUsingMipmaps()
		{
			return m_useMipmaps;
		}

		void Texture::SetMagnificationFiltering(TextureFilter filter)
		{
			m_magnificationFilter = filter;
			BindTexture();
			XENO_GL_CALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter));
		}

		Texture::TextureFilter Texture::GetMagnificationFiltering()
		{
			return m_magnificationFilter;
		}

		void Texture::SetMinificationFiltering(TextureFilter filter)
		{
			m_minificationFilter = filter;
			BindTexture();
			XENO_GL_CALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter));
		}

		Texture::TextureFilter Texture::GetMinificationFiltering()
		{
			return m_minificationFilter;
		}

		void Texture::SetXWrap(TextureWrap wrap)
		{
			m_xWrap = wrap;
			XENO_GL_CALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap));
		}

		Texture::TextureWrap Texture::GetXWrap()
		{
			return m_xWrap;
		}

		void Texture::SetYWrap(TextureWrap wrap)
		{
			m_yWrap = wrap;
			XENO_GL_CALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap));
		}

		Texture::TextureWrap Texture::GetYWrap()
		{
			return m_yWrap;
		}

		void Texture::BindTexture()
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, m_texture);
		}

		void Texture::UnbindTexture()
		{
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		void Texture::TransferTextureData(std::vector<unsigned char> data)
		{
			BindTexture();
			XENO_GL_CALL(glTexImage2D(GL_TEXTURE_2D, 0, m_colorType, m_width, m_height, 0, m_colorType, GL_UNSIGNED_BYTE, data.data()));
			if (m_useMipmaps)
			{
				XENO_GL_CALL(glGenerateMipmap(GL_TEXTURE_2D));
			}
		}
	}
}