#pragma once
#include <Xeno/EngineDefines.h>
#include <string>
#include "Texture.h"
#include <Xeno/Types/StringType.h>

namespace Xeno
{
	namespace Graphics
	{
		class ENGINEAPI ImageLoader
		{
		private:
			ImageLoader();
		protected:
		public:
			static ImageLoader& GetInstance();

			Texture LoadImageAsTexture(const std::string& path);
			Texture LoadImageFromMemoryAsTexture(unsigned char* buffer, int length);
		};
	}
}