#pragma once
#include <Xeno/EngineDefines.h>
#include "Renderable.h"
#include <GLEW/GL/glew.h>

namespace Xeno
{
	namespace Graphics
	{
		class ENGINEAPI Renderer
		{
		private:
			Renderer();

			static int m_drawCallCount;
		protected:
		public:
			static Renderer& GetInstance();

			void Initialize();

			unsigned int GenerateVertexArray();
			unsigned int GenerateVertexBuffer();
			unsigned int GenerateIndexBuffer();
			void BufferVertexPositionData(size_t size, void* data);
			void BufferVertexIndexData(size_t size, void* data);
			void BindVertexArray(unsigned int id);
			void BindVertexBuffer(unsigned int id);
			void BindIndexBuffer(unsigned int id);

			void Render(Renderable* renderable);

			static int GetDrawCallCount();
		};
	}
}