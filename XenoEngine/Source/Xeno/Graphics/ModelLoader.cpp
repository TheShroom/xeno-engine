#include "ModelLoader.h"
#include <Xeno/Logging/Logger.h>
#include "ImageLoader.h"
#include <Xeno/FileIO/PAKLoader.h>

namespace Xeno
{
	namespace Graphics
	{
		ModelLoader::ModelLoader() {  }

		ModelLoader& ModelLoader::GetInstance()
		{
			static ModelLoader instance;
			return instance;
		}

		Mesh ModelLoader::LoadModel(const std::string& modelSource)
		{
			std::vector<Mesh> generatedMeshes = std::vector<Mesh>();
			Assimp::Importer importer;

			// For now we pass aiProcess_OptimizeMeshes in order to turn all meshes into a single one.
			// This is because the Mesh system in the engine doesn't support having several meshes so
			// we can just do this for simplicity and let Assimp convert it to one mesh instead of doing
			// it manually.
			// Note: Changed std::string to Types::String so changed modelSource.data() to modelSource.ToCString() and size() to Count().
			const aiScene * scene = importer.ReadFileFromMemory(modelSource.c_str(), modelSource.length(), aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_OptimizeMeshes);
			if (scene == nullptr)
			{
				XENO_THROW_EXCEPTION(std::string("Failed to read model file\nError log: ") + importer.GetErrorString());
				return Mesh(); // Return an empty mesh.
			}

			generatedMeshes = GetInstance().ProcessAssimpNode(scene->mRootNode, scene);

			return generatedMeshes[0]; // TODO: For now we return the first mesh only since we don't support multiple meshes yet.
		}

		std::vector<Mesh> ModelLoader::ProcessAssimpNode(aiNode * node, const aiScene * scene)
		{
			static std::vector<Mesh> meshes = std::vector<Mesh>();

			for (unsigned int i = 0; i < node->mNumMeshes; i++)
			{
				aiMesh * mesh = scene->mMeshes[node->mMeshes[i]];
				meshes.push_back(ProcessAssimpMesh(mesh, scene));
			}

			for (unsigned int i = 0; i < node->mNumChildren; i++)
			{
				ProcessAssimpNode(node->mChildren[i], scene);
			}

			return meshes; // Crashes here because no meshes are added to the vector list.
		}

		Mesh ModelLoader::ProcessAssimpMesh(aiMesh * mesh, const aiScene * scene)
		{
			std::vector<glm::vec3> vertexPositions = std::vector<glm::vec3>();
			std::vector<unsigned int> indices = std::vector<unsigned int>(); // TODO: Unused until we get an actual index buffer. =)
			std::vector<float> texCoords = std::vector<float>(); // TODO: Unused until we get texture support. =)
			std::vector<glm::vec3> normals = std::vector<glm::vec3>(); // TODO: Unused until we get normal support.
			std::vector<std::string> textures = std::vector<std::string>();

			for (unsigned int i = 0; i < mesh->mNumVertices; i++)
			{
				glm::vec3 vertexPosition;
				glm::vec3 vertexNormal; // TODO: Unused until we get normal support. =)
				glm::vec3 vertexTexCoord; // TODO: Unused until we get texture support. =)

				vertexPosition.x = mesh->mVertices[i].x;
				vertexPosition.y = mesh->mVertices[i].y;
				vertexPosition.z = mesh->mVertices[i].z;
				vertexPositions.push_back(vertexPosition);

				vertexNormal.x = mesh->mNormals[i].x;
				vertexNormal.y = mesh->mNormals[i].y;
				vertexNormal.z = mesh->mNormals[i].z;
				normals.push_back(vertexNormal);

				if (mesh->mTextureCoords[0]) // Assimp supports usage of several texture coords per pixels, we only support one.
				{
					// for (unsigned int i = 0; i < mesh->mNumUVComponents[0]; i++)
					// {
					// 	glm::vec2 texCoord = glm::vec2();
					// 	texCoord.x = mesh->mTextureCoords[0][i]
					// }
					
					glm::vec2 texCoord = glm::vec2();
					texCoord.x = mesh->mTextureCoords[0][i].x;
					texCoord.y = mesh->mTextureCoords[0][i].y;
					texCoords.push_back(texCoord.x);
					texCoords.push_back(texCoord.y);
				}
				else // Model has no textures.
				{
					texCoords.push_back(0.0f);
					texCoords.push_back(0.0f);
					XENO_LOG_WARNING("Loaded model has no texture coordinates.");
				}
			}

			for (unsigned int i = 0; i < mesh->mNumFaces; i++)
			{
				aiFace face = mesh->mFaces[i];
				for (unsigned int j = 0; j < face.mNumIndices; j++)
				{
					indices.push_back(face.mIndices[j]);
				}
			}

			if (mesh->mMaterialIndex >= 0)
			{
				aiMaterial * material = scene->mMaterials[mesh->mMaterialIndex];

				std::vector<std::string> diffuseMaps = LoadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse"); // TODO: Unused until we get texture support.
				textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

				std::vector<std::string> specularMaps = LoadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
				textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
			}

			return Mesh(vertexPositions, indices, texCoords, textures); // TODO: Add normals, tex coords, etc when those get support. =)
		}

		std::vector<std::string> ModelLoader::LoadMaterialTextures(aiMaterial * mat, aiTextureType type, const std::string& typeName)
		{
			std::vector<std::string> textures = std::vector<std::string>(); // TODO: Replace int with Texture when we get texture support.
			for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
			{
				aiString str;
				mat->GetTexture(type, i, &str);
				// Texture texture = Texture(ImageLoader::GetInstance().LoadImageAsTexture(std::string(str.C_Str())));
				textures.push_back(std::string(str.C_Str()));
				XENO_LOG("Loaded texture '" + std::string(str.C_Str()) + "'.");
			}

			if (textures.size() <= 0) // Expression is true, so Assimp could not find any textures, I think?
			{
				XENO_LOG_WARNING("Model with no textures loaded.");
				// TODO: It seems like the .FBX file doesn't contain any texture data but I'm not 100% sure yet.
			}

			return textures;
		}
	}
}