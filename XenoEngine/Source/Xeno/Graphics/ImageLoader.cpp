#include "ImageLoader.h"
#include <Xeno/Logging/Logger.h>
#include <vector>

#define STB_IMAGE_IMPLEMENTATION // Required for stb_image.h
#include <STB/stb_image.h>

namespace Xeno
{
	namespace Graphics
	{
		ImageLoader::ImageLoader() {  }

		ImageLoader& ImageLoader::GetInstance()
		{
			static ImageLoader instance;
			return instance;
		}

		// TODO: Not sure if I should load images directly as textures. Maybe I should make an Image class that the Texture can use to create an actual texture?
		//		 But then again, what would the difference between the implementations between Texture & Image be?
		//		 Answer to that would probably be that the Texture is an actual OpenGL texture while the Image only contains image data.
		Texture ImageLoader::LoadImageAsTexture(const std::string& path)
		{
			int width;
			int height;
			int channelCount;

			unsigned char* ptrBuff = stbi_load(path.c_str(), &width, &height, &channelCount, 0);
			if (ptrBuff == nullptr)
			{
				XENO_THROW_EXCEPTION("Loading texture failed!\n" + std::string(stbi_failure_reason()));
				return Texture(); // Return empty texture.
			}

			std::vector<unsigned char> data = std::vector<unsigned char>(); // TODO: Crashes here.
			data.assign(ptrBuff, ptrBuff + (width * height * channelCount));
			stbi_image_free(ptrBuff); // Since we copied over the data to the vector we can free the old data.

			Texture::ColorType ct;

			if (channelCount == 3)
			{
				ct = Texture::ColorType::RGB;
			}
			else if (channelCount == 4)
			{
				ct = Texture::ColorType::RGBA;
			}
			else
			{
				XENO_THROW_EXCEPTION("Unknown texture color format");
				return Texture(); // Return empty texture.
			}
			
			Texture texture(data, width, height, ct, true, Texture::TEXTURE_LINEAR, Texture::TEXTURE_LINEAR, Texture::REPEAT, Texture::REPEAT);

			return texture;
		}

		Texture ImageLoader::LoadImageFromMemoryAsTexture(unsigned char* buffer, int length)
		{
			int width;
			int height;
			int channelCount;

			unsigned char* ptrBuff = stbi_load_from_memory(buffer, length, &width, &height, &channelCount, 0);
			if (ptrBuff == nullptr)
			{
				XENO_THROW_EXCEPTION("Loading texture failed!\n" + std::string(stbi_failure_reason()));
				return Texture(); // Return empty texture.
			}

			std::vector<unsigned char> data = std::vector<unsigned char>();
			data.assign(ptrBuff, ptrBuff + (width * height * channelCount));
			stbi_image_free(ptrBuff); // Since we copied over the data to the vector we can free the old data.

			Texture::ColorType ct;

			if (channelCount == 3)
			{
				ct = Texture::ColorType::RGB;
			}
			else if (channelCount == 4)
			{
				ct = Texture::ColorType::RGBA;
			}
			else
			{
				XENO_THROW_EXCEPTION("Unknown texture color format");
				return Texture(); // Return empty texture.
			}

			Texture texture(data, width, height, ct, true, Texture::TEXTURE_LINEAR, Texture::TEXTURE_LINEAR, Texture::REPEAT, Texture::REPEAT);

			return texture;
		}
	}
}