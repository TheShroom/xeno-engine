#pragma once

#include "EngineDefines.h"
#include "Input/Input.h"
#include "Graphics/Material.h"
#include "Graphics/Window.h"
#include "Graphics/Camera.h"
#include "Graphics/Renderer.h"

namespace Xeno {

	// The core of the Xeno game engine.
	class ENGINEAPI Engine
	{
	private:
		Engine();
		~Engine();

		// The singleton instance of the class.
		static Engine* m_instance;
		// The game window.
		Xeno::Graphics::Window m_window;
		// The main camera.
		Xeno::Graphics::Camera m_mainCamera;
		// The renderer.
		Xeno::Graphics::Renderer* m_renderer;

		int m_totalFrames;

		// The game loop of the engine, handles everything.
		void EngineLoop();

		// Calculates FPS.
		float CalculateFramesPerSecond();
		float CalculateAverageFramesPerSecond();
		// Calculates the amount of time it takes for 1 frame to complete in milliseconds.
		float CalculateFrameTime();
		float CalculateAverageFrameTime();
	protected:
	public:
		// Returns a singleton instance of this class.
		static Engine& GetInstance();

		// Initializes and starts the game engine.
		bool Initialize(int argc, char* argv[]);

		// Closes the engine.
		void Exit(int status);
	};

}