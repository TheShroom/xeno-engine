#pragma once
#include <Xeno/EngineDefines.h>
#include <Xeno/Entity Component System/Entity.h>
#include <map>

namespace Xeno
{
	class ENGINEAPI World
	{
	private:
		std::vector<Entity*> m_worldEntities;
	protected:
	public:
		World();

		void Load();
		void Unload();

		void AddEntity(Entity* entity);

		void UpdateEntities();
		void LateUpdateEntities();

		Entity* GetEntityWithTag(const std::string& tag);
		Entity* GetEntityWithName(const std::string& name);

		int GetEntityCount();
	};
}