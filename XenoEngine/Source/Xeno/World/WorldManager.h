#pragma once
#include <Xeno/EngineDefines.h>
#include "World.h"

namespace Xeno
{
	class ENGINEAPI WorldManager
	{
	private:
		World * m_activeWorld;
	protected:
	public:
		WorldManager();

		static WorldManager& GetInstance();

		void LoadWorld(World* world);
		void UnloadActiveWorld();

		static Entity* GetEntityWithTag(const std::string& tag);
		static Entity* GetEntityWithName(const std::string& name);

		void UpdateEntities();
		void LateUpdateEntities();

		World* GetActiveWorld();
	};
}