#include "WorldManager.h"
#include <Xeno/Editor/Editor.h>

namespace Xeno
{
	WorldManager::WorldManager() :
		m_activeWorld(nullptr)
	{

	}

	WorldManager& WorldManager::GetInstance()
	{
		static WorldManager instance;
		return instance;
	}

	void WorldManager::LoadWorld(World* world)
	{
		if (m_activeWorld != nullptr)
		{
			UnloadActiveWorld();
		}

		m_activeWorld = world;
		m_activeWorld->Load();
	}

	void WorldManager::UnloadActiveWorld()
	{
		m_activeWorld->Unload();

	}

	void WorldManager::UpdateEntities()
	{
		m_activeWorld->UpdateEntities();
	}

	void WorldManager::LateUpdateEntities()
	{
		m_activeWorld->LateUpdateEntities();
	}

	Entity* WorldManager::GetEntityWithTag(const std::string& tag)
	{
		return GetInstance().m_activeWorld->GetEntityWithTag(tag);
	}

	Entity* WorldManager::GetEntityWithName(const std::string& name)
	{
		return GetInstance().m_activeWorld->GetEntityWithName(name);
	}

	World* WorldManager::GetActiveWorld()
	{
		return m_activeWorld;
	}
}