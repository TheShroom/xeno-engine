#include "World.h"

namespace Xeno
{
	World::World()
	{
		
	}

	void World::Load()
	{
		m_worldEntities = std::vector<Entity*>();
	}
	
	void World::Unload()
	{
		std::vector<Entity*>::iterator it;
		for (it = m_worldEntities.begin(); it != m_worldEntities.end(); it++)
		{
			delete *it._Ptr;
			m_worldEntities.erase(it);
		}
	}

	void World::AddEntity(Entity* entity)
	{
		XENO_LOG("Adding entity '" + entity->GetName() + "' with tag '" + entity->GetTag() + "' to world entity list.");
		m_worldEntities.push_back(entity);
	}

	void World::UpdateEntities()
	{
		for (int i = 0; i < m_worldEntities.size(); i++)
		{
			Entity* entity = m_worldEntities[i];
			entity->Update();
		}
	}

	void World::LateUpdateEntities()
	{
		for (int i = 0; i < m_worldEntities.size(); i++)
		{
			Entity* entity = m_worldEntities[i];
			entity->LateUpdate();
		}
	}

	Entity* World::GetEntityWithTag(const std::string& tag)
	{
		for (int i = 0; i < m_worldEntities.size(); i++)
		{
			Entity* entity = m_worldEntities[i];
			if (entity->GetTag() == tag)
			{
				return entity;
			}
		}

		XENO_LOG_WARNING("Tried getting entity by tag, but no entity with the tag '" + tag + "' exists in the current world. Returning nullptr.");
	}

	Entity* World::GetEntityWithName(const std::string& name)
	{
		for (int i = 0; i < m_worldEntities.size(); i++)
		{
			Entity* entity = m_worldEntities[i];
			if (entity->GetName() == name)
			{
				return entity;
			}
		}

		XENO_LOG_WARNING("Tried getting entity by name, but no entity with the name '" + name + "' exists in the current world. Returning nullptr.");
	}

	int World::GetEntityCount()
	{
		return m_worldEntities.size();
	}
}